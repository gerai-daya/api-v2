<?php

use Illuminate\Support\Str;
use App\Models\GeneralModel;
use Carbon\Carbon;



function outputResponse($param)
{
    echo "haloo ini first_function";
}

function generateHasKey()
{
    return base64_encode(Str::random(50));
}

function showOutputResponse($funct, $status, $msg, $data, $code, $request = null)
{
    $dataResponse = array(
        'token_expired' => $funct == "tokenLoginExpired" ? true : false,
        'status' => $status,
        'message' => $msg,
        'data' => $data
    );
    if ($request != null) {
        GeneralModel::insertLogRequest($funct, $request['type'], $request['request'], $request['location']);
    } else {
        GeneralModel::insertLogRequest($funct, "OUT", json_encode($dataResponse), "-");
    }

    return response()->json($dataResponse, $code);
}

function showOutputResponseBCA($func, $type, $data, $location, $code, $isSuccess, $ina = "", $en = "")
{

    if (!$isSuccess) {
        $data = array('ErrorCode' => $code, 'ErrorMessage' => array('Indonesian' => $ina, 'English' => $en));
    }
    GeneralModel::insertLogRequest($func, $type, json_encode($data), $location);
    return response()->json($data, $code);
}

function showOutputResponseBCAVa($func, $type, $data, $location, $code, $isSuccess, $ina = "", $en = "", $params = "")
{
    $data = "";

    if ($func == "GetPaymentBillVaBCA") {

        $data = array(
            "CompanyCode" => (string)@$params->CompanyCode,
            "CustomerNumber" => (string)@$params->CustomerNumber,
            "RequestID" => @$params->RequestID,
            "PaymentFlagStatus" => "01",
            "PaymentFlagReason" => array(
                "Indonesian" => $ina,
                "English" => $en
            ),
            "CustomerName" => "",
            "CurrencyCode" => "IDR",
            "PaidAmount" => "0.00",
            "TotalAmount" => "0.00",
            "TransactionDate" =>  @$params->TransactionDate,
            "DetailBills" => array(),
            "FreeTexts" => array(),
            "AdditionalData" => ""
        );
    } else {
        $data = array(
            "CompanyCode" => (string)@$params->CompanyCode,
            "CustomerNumber" => (string)@$params->CustomerNumber,
            "InquiryStatus" => "01",
            "InquiryReason" => array(
                "Indonesian" => $ina,
                "English" => $en
            ),
            "RequestID" => @$params->RequestID,
            "CustomerName" => "",
            "CurrencyCode" => "IDR",
            "TotalAmount" => "0.00",
            "SubCompany" => "00000",
            "DetailBills" => array(),
            "FreeTexts" => array(),
            "AdditionalData" => ""
        );
    }

    GeneralModel::insertLogRequest($func, $type, json_encode($data), $location);
    return response()->json($data, $code);
}

function kirim_sms_viro($to, $text, $jenis = '')
{

    $pecah              = explode(",", $to);
    $jumlah             = count($pecah);

    $from               = "GERAI DAYA"; //Sender ID or SMS Masking Name, if leave blank, it will use default from telco
    $username           = $jenis == 'PREMIUM' ? "geraidayapremium" : "geraidaya";
    $password           = "Ec=ub-|9U0";

    $postUrl            = "https://api.smsviro.com/restapi/sms/1/text/advanced";
    for ($i = 0; $i < $jumlah; $i++) {

        if (substr($pecah[$i], 0, 2) == "62" || substr($pecah[$i], 0, 3) == "+62") {
            $pecah = $pecah;
        } elseif (substr($pecah[$i], 0, 1) == "0") {
            $pecah[$i][0] = "X";
            $pecah = str_replace("X", "62", $pecah);
        } else {
            //echo "Invalid mobile number format";
        }
        $destination = array("to" => $pecah[$i]);
        $message     = array(
            "from" => $from,
            "destinations" => $destination,
            "text" => $text,
            "smsCount" => 1
        );
        $postData           = array("messages" => array($message));
        $postDataJson       = json_encode($postData);

        $ch                 = curl_init();
        $header             = array("Content-Type:application/json", "Accept:application/json");

        curl_setopt($ch, CURLOPT_URL, $postUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataJson);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
    }
    return $response;
}



function sendSMS($hp, $pesan, $mode)
{
    if (config('app.IS_SMS_LIVE')) {
        kirim_sms_viro($hp, urldecode($pesan), $mode);
    }
}

function sendEMAIL($prm)
{

    $param = (object)$prm;
    if (config('app.IS_EMAIL_LIVE')) {
        $mailjetApiKey = config('app.MAILJET_API_KEY');
        $mailjetApiSecret = config('app.MAILJET_API_SECRET');
        $messageData = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => config('app.MAIL_SENDER'),
                        'Name' => config('app.MAIL_SENDER_NAME')
                    ],

                    'To' => $param->isbanyak ? $param->to : [
                        [
                            'Email' => $param->to,
                            'Name' => $param->name == "" ? "Guest!" : $param->name
                        ]
                    ],
                    'TemplateID' => 2090141,
                    'TemplateLanguage' => true,
                    'Mj-TemplateLanguage' => true,
                    'Mj-TemplateErrorReporting' => 'gitradevid@gmail.com',
                    'Mj-TemplateErrorDeliver' => 'deliver',
                    'Variables' => array(
                        "judul" => $param->subject,
                        "nama" => $param->name == "" ? "Guest!" : $param->name,
                        "pesan" => $param->message,
                        "kode_otp" => @$param->otp,
                    ),
                    'Subject' => $param->subject
                ]
            ]
        ];

        $jsonData = json_encode($messageData);
        $ch = curl_init(config('app.MAILJET_URL'));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_USERPWD, "{$mailjetApiKey}:{$mailjetApiSecret}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ]);
        $response = json_decode(curl_exec($ch));

        GeneralModel::insertLogEmail(json_encode($response));
    }
}

function kirimKwitansi($data)
{
    if (config('app.IS_EMAIL_LIVE')) {
        $dataTr = "";
        foreach ($data['booking_detail'] as $detail) {
            $dataTr .= '
                    <tr style="color:#000000">
                        <td align="left" width="15"> </td>
                        <td align="left" style="font-family:\'Helvetica\',\'Arial\',sans-serif;font-weight:normal">
                            <span style="font-size:11px;line-height:18px">' . $detail->qty . '</span>
                            <span style="font-size:11px;line-height:18px">@' . $detail->harga . '</span>
                            <span style="font-size:11px;font-weight:bold">' . $detail->item . '</span>
                        </td> <td align="right" style="font-family:\'Helvetica\',\'Arial\',sans-serif;font-weight:normal">
                            <span style="font-size:11px;line-height:18px">Rp. ' . $detail->jumlah . '</span>
                        </td><td align="right"width="15"></td>
                    </tr>
                    
                    <tr>
                        <td height="5px" align="left"></td>
                        <td height="5px" colspan="2" align="left" style="border-top:1px dashed #9e9e9e"> </td>
                        <td height="5px" align="left"> </td>
                    </tr>
                    ';
        }

        $dataBooking = $data['data_booking'];
        $variabel_values = array(
            "total" => $dataBooking->total,
            "tanggal" => $dataBooking->tanggal,
            "cara_bayar" => $dataBooking->cara_bayar,
            "kode_booking" => $dataBooking->kode_booking,
            "nama_petugas" => $dataBooking->nama_petugas,
            "nama_pelanggan" => $dataBooking->nama_pelanggan,
            "lokasi_pekerjaan" => $dataBooking->lokasi_pekerjaan,
            "subtotal" => $dataBooking->subtotal,
            "biaya_survey" => $dataBooking->biaya_survey,
            "grand_total" => $dataBooking->total,
            "diskon" => $dataBooking->diskon,
            "keluhan" => trim($dataBooking->keluhan),
            "data_detail" => $dataTr
        );
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => config('app.MAIL_SENDER'),
                        'Name' => config('app.MAIL_SENDER_NAME')
                    ],
                    'To' => [
                        [
                            'Email' => $dataBooking->email,
                            'Name' => $dataBooking->nama_pelanggan
                        ]
                    ],

                    'TemplateID' => 2084396,
                    'TemplateLanguage' => true,
                    'Mj-TemplateLanguage' => true,
                    'Mj-TemplateErrorReporting' => 'gitradevid@gmail.com',
                    'Mj-TemplateErrorDeliver' => 'deliver',
                    'Variables' => $variabel_values,
                    'Subject' => 'Nota Pembayaran Elektonik ' . $dataBooking->kode_booking
                ]
            ]
        ];

        $mailjetApiKey = config('app.MAILJET_API_KEY');
        $mailjetApiSecret = config('app.MAILJET_API_SECRET');

        $jsonData = json_encode($body);
        $ch = curl_init(config('app.MAILJET_URL'));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_USERPWD, "{$mailjetApiKey}:{$mailjetApiSecret}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ]);
        $response = curl_exec($ch);
        GeneralModel::insertLogEmail($response);
    }
}


function sendPushNotification($to, $param)
{
    $res = array();
    $res['data']['title'] = $param->title;
    $res['data']['is_background'] = true;
    $res['data']['message'] = $param->message;
    $res['data']['image'] = $param->image;
    $res['data']['payload'] = $param->data;
    $res['data']['timestamp'] = date('Y-m-d G:i:s');

    $fields = array(
        'to' => $to,
        'data' => $res,
    );


    $headers = array(
        'Authorization: key=' . config('app.FIREBASE_KEY'),
        'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, config('app.FIREBASE_URL'));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

function sendPushNotificationToMany($to, $param)
{
    $res = array();
    $res['data']['title'] = $param->title;
    $res['data']['is_background'] = true;
    $res['data']['message'] = $param->message;
    $res['data']['image'] = $param->image;
    $res['data']['payload'] = $param->data;
    $res['data']['timestamp'] = date('Y-m-d G:i:s');
    $res['data']['jsonData'] = @$param->jsonData;
    $res['data']['mode_api'] = @$param->mode == "" ? "NOTIF" : $param->mode;

    $fields = array(
        'registration_ids' => $to,
        'data' => $res,
    );


    $headers = array(
        'Authorization: key=' . config('app.FIREBASE_KEY'),
        'Content-Type: application/json'
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, config('app.FIREBASE_URL'));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

function generateNoRef()
{
    return 'GRDY_' . mt_rand(100000, 999999);
}
function getCurrentDatetime()
{
    date_default_timezone_set('Asia/Makassar');
    // return Carbon::now();
    return date('Y-m-d H:i:s');
}
function generateOtp()
{
    return mt_rand(100000, 999999);
}

function isExpiredOTP($dateExpired)
{
    $isExpired = false;
    $tgl_today = date('Y-m-d');
    $time_today = date('H:i:s');
    // echo $tgl_today;
    // echo $time_today;
    $tgl_expired = date("Y-m-d", strtotime($dateExpired));
    $time_expired = date("H:i:s", strtotime($dateExpired));

    if ($tgl_today > $tgl_expired) {
        $isExpired = true;
    } else {
        if (($tgl_today == $tgl_expired) && ($time_today > $time_expired)) {
            $isExpired = true;
        } else if (($tgl_today > $tgl_expired) && ($time_today > $time_expired)) {
            $isExpired = true;
        } else {
            $isExpired = false;
        }
    }
    return $isExpired;
}

function generateImageName($original_filename, $path)
{
    $original_filename_arr = explode('.', $original_filename);
    $file_ext = end($original_filename_arr);
    $destination_path = $path;
    $image = Carbon::now()->format('YmdHis') . rand(1000, 9999) . '.' . $file_ext;
    return $image;
}

function GetTime()
{
    date_default_timezone_set('Asia/Jakarta');
    $time = explode('+', date('c'));
    $time = $time[0] . '.' . date('B') . '+' . $time[1];
    return $time;
}

function removeSpace($string)
{
    return str_replace(' ', '', $string);
}

function ErrorToString($msgError)
{
    $text = "";
    $key = array_keys($msgError);
    for ($i = 0; $i < count($key); $i++) {
        foreach ($msgError[$key[$i]] as $var) {
            $text .= $var . " ";
        }
    }
    return $text;
}

function getKeyErrorToString($msgError)
{
    $text = "";
    $key = array_keys($msgError);
    $separator = count($key) == 1 ? " " : ", ";
    for ($i = 0; $i < count($key); $i++) {
        $text .= '[' . $key[$i] . '] ';
    }
    return $text;
}

function isTokenValid($isUat, $bearer)
{
    $plain = explode(':', base64_decode($bearer));
    if (@$plain[0] == checkCredential($isUat, 'ClientId') && @$plain[1] == checkCredential($isUat, 'ClientSecret')) {
        return true;
    } else {
        return false;
    }
}

function isExpired($dateParam, $interval)
{

    $today = date("Y-m-d");
    $dateExpired = date('Y-m-d', strtotime($dateParam . $interval));
    if ($today > $dateExpired) {
        return false;
    } else {
        return true;
    }
}

function isWordExists($sentence, $text)
{
    if (preg_match('/\b' . $text . '\b/', $sentence)) {
        return true;
    } else {
        return false;
    }
}

function checkCredential($isUat, $key)
{
    if ($isUat) {
        return config('app.' . $key . '-uat');
    } else {
        return config('app.' . $key);
    }
}

function isValidDatetimeFormat($datetime, $format)
{
    $date = explode(" ", $datetime);
    if (count($date) == 2) {
        if ($format == "yyyy-mm-dd") {
            return preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (0[1-9]|1[0-9]|2[0-4])\:([012345][0-9])\:([012345][0-9])$/", $datetime);
        } else if ($format == "dd/mm/yyyy") {
            return preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4} (0[1-9]|1[0-9]|2[0-4])\:([012345][0-9])\:([012345][0-9])$/", $datetime);
        }
    } else {
        if ($format == "yyyy-mm-dd") {
            return preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $datetime);
        } else {
            return false;
        }
    }
}

function isValidFlagAdvice($data)
{
    if ($data == "Y" || $data == "N") {
        return true;
    } else {
        return false;
    }
}
