<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\GeneralModel;
use App\Models\BcaModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class ApiBCA extends Controller
{

    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
        $this->params = '';
        $this->isUat = isWordExists($_SERVER['REQUEST_URI'], 'finance-uat');
    }




    public function index(Request $request)
    {
        $this->request = $request;
        if (!$this->request->status) {
            return  showOutputResponse("tokenLoginExpired", false, 'Token Anda Invalid, Silakan login ulang', array(), 401, array('type' => 'IN', 'request' => $this->request, 'location' => 'ApiBCA'));
            exit;
        }

        if ($request->isMethod('post')) {
            $this->request = $request->getContent();
            $this->params = json_decode($this->request);

            $validator = Validator::make(json_decode($this->request, true), [
                'Func' => 'required'
            ]);

            if ($validator->fails()) {
                return  showOutputResponse("paramaterFunctionNotFound", false, 'Parameter Not Found!', $validator->errors(), 404, array('type' => 'IN', 'request' => $this->request, 'location' => 'ApiBCA'));
            } else {
                if (method_exists($this, $this->params->Func)) {
                    GeneralModel::insertLogRequest($this->params->Func, 'IN', $this->request, 'ApiBCA');
                    return $this->{$this->params->Func}();
                } else {
                    return  showOutputResponse('FunctionNotFound', false, 'Function Not Found!', 'Function [' . @$this->params->Func . '] not found!', 404, array('type' => 'IN', 'request' => $this->request, 'location' => 'ApiBCA'));
                }
            }
        } else {
            return  showOutputResponse('methodNotAllowed', false, 'Not Found!', 'Not Found!', 405, array('type' => 'IN', 'request' => $this->request, 'location' => 'ApiBCA'));
        }
    }


    //checkCredential($this->isUat,'BCAApiSecret')
    //$hash = hash_hmac('sha256', $message, checkCredential($this->isUat,checkCredential($this->isUat,'BCAApiSecret'))); 
    public function GenerateSignature($method, $url, $token, $dataPost, $time)
    {
        $url = str_replace(',', '%2C', $url);
        if (strpos($url, 'StartDate') > 0) {
            $dateURL = explode('&', explode('?', $url)[1]);
            $url = explode('?', $url)[0] . '?' . $dateURL[1] . '&' . $dateURL[0];
        }
        $message = strtoupper($method) . ':' . $url . ':' . $token . ':' . strtolower((hash('sha256', $dataPost))) . ':' . $time;
        // echo $message." xx ";
        $hash = hash_hmac('sha256', $message, checkCredential($this->isUat, 'BCAApiSecret'));
        $signature = ($hash);
        // echo $signature." yy ";
        return  $signature;
    }



    public function  sendCurlBcaApi($func, $link, $TokenOrAPIKeySignature, $dataPost, $isToken = false, $method = 'POST', $isAddChannelID_CredentialID = false)
    {
        $time = removeSpace(GetTime());

        $process = curl_init(checkCredential($this->isUat, 'UrlApi') . $link);
        // GeneralModel::insertLogRequest($func,'REQUEST',removeSpace($dataPost),'ApiBCA');
        if ($isToken) { //Jika Token
            $header_request = array(
                'Content-Type:application/x-www-form-urlencoded',
                'Authorization: Basic ' . base64_encode(checkCredential($this->isUat, 'ClientId') . ':' . checkCredential($this->isUat, 'ClientSecret'))
            );
            curl_setopt($process, CURLOPT_HTTPHEADER, $header_request);
            GeneralModel::insertLogRequest($func, 'HEADER_REQUEST', json_encode($header_request), 'ApiBCA');
        } else {
            $header_request = array(
                'Content-Type:application/json',
                'Authorization:Bearer ' . $TokenOrAPIKeySignature,
                'Origin:',
                'X-BCA-Key:' . checkCredential($this->isUat, 'BCAApiKey'),
                'X-BCA-Timestamp:' . $time,
                'X-BCA-Signature:' . self::GenerateSignature($method, $link, $TokenOrAPIKeySignature, removeSpace($dataPost), $time)
            );

            if ($isAddChannelID_CredentialID) {
                $header_request[] = 'channel-id:95051';
                $header_request[] = "credential-id:" . checkCredential($this->isUat, 'CorporateId');
            }
            curl_setopt($process, CURLOPT_HTTPHEADER, $header_request);
            // GeneralModel::insertLogRequest($func,'HEADER_REQUEST',json_encode($header_request),'ApiBCA');
        }

        curl_setopt($process, CURLOPT_VERBOSE, 1);
        curl_setopt($process, CURLOPT_HEADER, 1);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        if ($method == 'POST') {
            curl_setopt($process, CURLOPT_POST, 1);
            curl_setopt($process, CURLOPT_POSTFIELDS, removeSpace($dataPost)); //'grant_type=client_credentials');
        } else {
            //curl_setopt($process, CURLOPT_POST, 0);
        }


        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($process, CURLOPT_SSL_VERIFYHOST, FALSE);
        $return = curl_exec($process);
        $header_size = curl_getinfo($process, CURLINFO_HEADER_SIZE);
        $header = substr($return, 0, $header_size);
        $return = substr($return, $header_size);

        // GeneralModel::insertLogRequest($func,'HEADER_RESPON',json_encode($header),'ApiBCA');
        // GeneralModel::insertLogRequest($func,'RESPONSE',$return,'ApiBCA');

        GeneralModel::insertLogRequest($func, 'REQUEST', 'REQUEST FIELD
            ' . removeSpace($dataPost) . '
            
            HEADER REQUEST
            ' . json_encode($header_request) . '
            
            HEADER RESPONSE
            ' . json_encode($header) . '
            
            RESPONSE RAW
            ' . $return, 'ApiBCA');

        curl_close($process);
        return json_decode($return);
    }


    public function GetToken()
    {
        $url    = '/api/oauth/token';
        $data   = "grant_type=client_credentials";
        $ch = self::sendCurlBcaApi("GetToken", $url, checkCredential($this->isUat, 'BCAApiKey'), $data, true);
        if (@($ch == 0)) {
            return 0;
        } else {
            return $ch->access_token;
        }
    }

    private function GetBalanceInquiry()
    { //Get Saldo
        $data = "";
        $url = '/banking/v3/corporates/' . checkCredential($this->isUat, 'CorporateId') . '/accounts/' . checkCredential($this->isUat, 'AccountNumber');
        $ch = self::sendCurlBcaApi($this->params->Func, $url, self::GetToken(), $data, false, 'GET');
        if (isset($ch->ErrorCode)) {
            return   showOutputResponse($url, false, $ch->ErrorMessage->Indonesian, array(), 200);
        } else {
            if (count(@$ch->AccountDetailDataSuccess) > 0) {
                return   showOutputResponse($url, true, 'Berhasil request GetBalanceInquiry', $ch->AccountDetailDataSuccess, 200);
            } else {
                return   showOutputResponse($url, false, $ch->AccountDetailDataFailed[0]->Indonesian, array("status" => 01), 200);
            }
        }
    }

    private function GetAccountStatement()
    { //Get Mutasi
        $validator = Validator::make(json_decode($this->request, true), [
            'StartDate' => 'required|min:10|max:10|date|date_format:Y-m-d|before_or_equal:today',
            'EndDate' => 'required|min:10|max:10|date|date_format:Y-m-d|after_or_equal:StartDate',
        ]);
        if ($validator->fails()) {
            return  showOutputResponse($this->params->Func, false, 'Parameter Tanggal Awal atau Tanggal Akhir Tidak Valid', $validator->errors(), 200);
        } else {
            $data = "";
            $url = '/banking/v3/corporates/' . checkCredential($this->isUat, 'CorporateId') . '/accounts/' . checkCredential($this->isUat, 'AccountNumber') . '/statements?StartDate=' . $this->params->StartDate . '&EndDate=' . $this->params->EndDate;
            $ch = self::sendCurlBcaApi($this->params->Func, $url, self::GetToken(), $data, false, 'GET');

            if (isset($ch->ErrorCode)) {
                return   showOutputResponse($url, false, $ch->ErrorMessage->Indonesian, array(), 200);
            } else {
                if (isset($ch->StartDate) > 0) {
                    return   showOutputResponse($url, true, 'Berhasil request GetAccountStatement', $ch, 200);
                } else if (isset($ch->AccountDetailDataSuccess) > 0) {
                    return   showOutputResponse($url, true, 'Berhasil request GetBalanceInquiry', $ch->AccountDetailDataSuccess, 200);
                } else {
                    return   showOutputResponse($url, false, $ch->AccountDetailDataFailed[0]->Indonesian, array("status" => 01), 200);
                }
            }
        }
    }

    //Transfer Antar Rekening BCA
    private function SendFundTransfer()
    {
        $validator = Validator::make(json_decode($this->request, true), [
            'destinationNumber' => 'required|max:16',
            'amount' => 'required|max:16'
        ]);
        if ($validator->fails()) {
            return  showOutputResponse($this->params->Func, false, 'Parameter Nomor Tujuan atau Nominal Tidak Valid', $validator->errors(), 200);
        } else {

            $url = '/banking/corporates/transfers';
            $data = json_encode(
                array(
                    "CorporateID" => checkCredential($this->isUat, 'CorporateId'),
                    "SourceAccountNumber" => checkCredential($this->isUat, 'AccountNumber'),
                    // "TransactionID"=>rand(10000001,99999999),
                    "TransactionID" => '83305115',
                    "TransactionDate" => Carbon::now()->format('Y-m-d'),
                    "ReferenceID" => rand(10001, 99999),
                    "CurrencyCode" => "IDR",
                    "Amount" => $this->params->amount,
                    "BeneficiaryAccountNumber" => $this->params->destinationNumber,
                    "Remark1" => removeSpace(@$this->params->desc1),
                    "Remark2" => removeSpace(@$this->params->desc2)
                )
            );
            $ch = self::sendCurlBcaApi($this->params->Func, $url, self::GetToken(), $data, false, 'POST');
            if (!isset($ch->ErrorCode)) {
                return   showOutputResponse($url, true, 'Transaksi berhasil', $ch, 200);
            } else {
                if (isset($ch->ErrorCode)) {
                    return   showOutputResponse($url, false, $ch->ErrorMessage->Indonesian, $ch, 200);
                } else {
                    return   showOutputResponse($url, false, @$ch->AccountDetailDataFailed[0]->Indonesian, array("status" => 01), 200);
                }
            }
        }
    }

    private function GetInquiryOverbooking()
    { //Get Data Nama Rekening antar rek BCA (Pemindah Bukuan)
        $url = '/apiservice/InquiryServices/AccountInfo/inq';
        $data     = json_encode(
            array(
                "AcctInqRq" =>
                array(
                    "MsgRqHdr" => array(
                        'RequestTimestamp' => date('c'),
                        'CustRefID' => $this->params->CustomerId
                    ),
                    "InqInfo" => array(
                        'AccountNumber' => $this->params->AccountNumber
                    )

                )
            )
        );
        $ch = self::sendCurlBcaApi($this->params->Func, $url, self::GetToken(), $data);
        // $ch=$ch->AcctInqRs;
        // $this->Model_global->InsertAddLogTransaksi('',$CustID,0,"Func :GetInquiryOverbooking",$ch->MsgRsHdr->StatusCode,'','','',$this->ChannelCode,'','GetInquiryOverbooking');
        GeneralModel::insertLogRequest("GetInquiryOverbooking", 'RESPONSE', json_encode($ch), 'ApiBCA');
        // echo json_encode($ch);

        if (!isset($ch->ErrorCode)) {
            return   showOutputResponse($url, true, 'Transaksi berhasil', $ch, 200);
        } else {
            if (isset($ch->ErrorCode)) {
                return   showOutputResponse($url, false, $ch->ErrorMessage->Indonesian, $ch, 200);
            } else {
                return   showOutputResponse($url, false, "Mohon maaf, coba beberapa saat lagi", array("status" => 01), 200);
            }
        }
    }

    //Transfer antar bank
    private function GetInquiryTransferOnline()
    {

        $validator = Validator::make(json_decode($this->request, true), [
            'ToAccount' => 'required|max:16',
            'BankId' => 'required'
        ]);
        if ($validator->fails()) {
            $errorString = getKeyErrorToString(json_decode($validator->errors(), true));
            return  showOutputResponse($this->params->Func, false, 'Parameter Nomor Tujuan atau Kode Bank', $errorString, 200);
        } else {
            $url = '/banking/corporates/transfers/v2/domestic/beneficiaries/banks/' . $this->params->BankId . '/accounts/' . $this->params->ToAccount;
            $ch = self::sendCurlBcaApi($this->params->Func, $url, self::GetToken(), "", false, 'GET', true);
            if (!isset($ch->ErrorCode)) {
                return   showOutputResponse($url, true, 'Transaksi berhasil', $ch, 200);
                /*
                    {
                        "beneficiary_bank_code" : "BNIAIDJA",
                        "beneficiary_account_number" : "1231314113331",
                        "beneficiary_account_name"" : "Trinawati Eka Putri"
                    }
                */
            } else {
                if (isset($ch->ErrorCode)) {
                    return   showOutputResponse($url, false, $ch->ErrorMessage->Indonesian, $ch, 200);
                } else {
                    return   showOutputResponse($url, false, "Mohon maaf, coba beberapa saat lagi", array("status" => 01), 200);
                }
            }
        }
    }

    private function SendTransferOnline()
    { //Transfer Antar Bank
        $validator = Validator::make(json_decode($this->request, true), [
            'destinationNumber' => 'required|max:16',
            'bankCode' => 'required',
            'destinationName' => 'required',
            'amount' => 'required',
            'transferType' => 'required',
        ]);
        if ($validator->fails()) {
            $errorString = getKeyErrorToString(json_decode($validator->errors(), true));
            return  showOutputResponse($this->params->Func, false, 'Parameter yang wajib diisi', $errorString, 200);
        } else {
            $url = '/banking/corporates/transfers/v2/domestic';

            $data = array(
                "transaction_id" => rand(10000001, 99999999),
                "transaction_date" => date('Y-d-m'),
                "source_account_number" => checkCredential($this->isUat, 'AccountNumber'),
                "beneficiary_account_number" => $this->params->destinationNumber,
                "beneficiary_bank_code" => $this->params->bankCode,
                "beneficiary_name" => $this->params->destinationName,
                "amount" => $this->params->amount,
                "transfer_type" => $this->params->transferType,
                "beneficiary_cust_type" => '2',
                "beneficiary_cust_residence" => '2',
                "currency_code" => 'IDR',
                "remark1" => @$this->params->desc1,
                "remark2" => @$this->params->desc2,
                "beneficiary_email" => @$this->params->email,
            );

            $ch = self::sendCurlBcaApi($this->params->Func, $url, self::GetToken(), json_encode($data), false, 'POST', true);
            if (!isset($ch->ErrorCode)) {
                return   showOutputResponse($url, true, 'Transaksi berhasil', $ch, 200);
            } else {
                if (isset($ch->ErrorCode)) {
                    return   showOutputResponse($url, false, $ch->ErrorMessage->Indonesian, $ch, 200);
                } else {
                    BcaModel::insertDataTransactionBank(array_merge($data, array('source' => 'GENERAL', 'status' => 'SUCCESS', 'type' => 'OUT')));
                    return   showOutputResponse($url, false, @$ch->AccountDetailDataFailed[0]->Indonesian, array("status" => 01), 200);
                }
            }
        }
    }

    private function GetStatusTransfer()
    { //Transfer Antar Bank

        $url = '/banking/corporates/transfers/v2/status';
        $data = json_encode(array(
            "transactionId" => $this->params->transactionId,
            "transactionDate" => $this->params->transactionDate,
            "transferType" => $this->params->transferType
        ));
        $ch = self::sendCurlBcaApi($this->params->Func, $url, self::GetToken(), $data, false, 'POST', true);
        if (!isset($ch->error_code)) {
            return   showOutputResponse($url, true, 'Transaksi berhasil', $ch, 200);
        } else {
            if (isset($ch->error_code)) {
                return   showOutputResponse($url, false, $ch->error_message->indonesian, $ch, 200);
            } else {
                return   showOutputResponse($url, false, @$ch->AccountDetailDataFailed[0]->indonesian, array("status" => 01), 200);
            }
        }
    }


    //virtual account

    public function GetTokenVABCA()
    { // ini belum solved ketika ada berbarengan request token
        GeneralModel::insertLogRequest("GetTokenVABCA", 'IN', $this->request, 'ApiBCA');
        if ($this->request->header('Authorization')) {
            $key = explode(' ', $this->request->header('Authorization'));
            if (@$key[0] != "Basic") {
                return showOutputResponseBCA('GetTokenVABCA', 'OUT', '', 'ApiBCA', 401, false, 'Autentikasi salah', 'You Unauthorized!');
            } else if (!isTokenValid($this->isUat, @$key[1])) {
                return showOutputResponseBCA('GetTokenVABCA', 'OUT', '', 'ApiBCA', 401, false, 'Autentikasi salah', 'You Unauthorized!');
            } else {
                $token = Hash::make(Carbon::now() . "#" . @$key[1]);
                GeneralModel::insertLogLogin("0", 'GetTokenVABCA', $token);
                $time = GetTime();
                $data = array(
                    "access_token" => $token,
                    "token_type" => "Bearer",
                    "expires_in" => 3600,
                    "scope" => "resource.WRITE resource.READ",
                    // "temp"=>array(
                    //     'bcakey'=>checkCredential($this->isUat,'BCAApiKey'),
                    //     'time'=>$time,
                    //     'signature'=>self::GenerateSignature('POST','/va/bills',$token,'',$time)
                    // )
                );
                return showOutputResponseBCA('GetTokenVABCA', 'OUT', $data, 'ApiBCA', 200, true);
            }
        } else {
            return showOutputResponseBCA('GetTokenVABCA', 'OUT', '', 'ApiBCA', 401, false, 'Perlu Autentikasi salah', 'You Unauthorized!');
        }
    }

    public function GetBillVaBCA()
    {
        GeneralModel::insertLogRequest('GetBillVaBCA', 'HEADER_REQUEST', $this->request, 'ApiBCA');

        $request = $this->request->getContent();
        $this->params = json_decode($request);

        if (!$this->request->header('Authorization')) {
            return showOutputResponseBCAVa('GetBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'Autentikasi tidak di temukan', 'Authorization not found', $this->params);
        } else if (!$this->request->header('X-Bca-Key')) {
            return showOutputResponseBCAVa('GetBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'BCA Key tidak di temukan', 'BCA Key not found', $this->params);
        } else if ($this->request->header('X-Bca-Key') != checkCredential($this->isUat, 'BCAApiKey')) {
            return showOutputResponseBCAVa('GetBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'BCA Key salah', 'BCA Key incorrect', $this->params);
        } else if (!$this->request->header('X-Bca-Signature')) {
            return showOutputResponseBCAVa('GetBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'BCA Signature tidak di temukan', 'BCA Signature not found', $this->params);
        } else if (!$this->request->header('X-Bca-Timestamp')) {
            return showOutputResponseBCAVa('GetBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'BCA Timestamp tidak di temukan', 'BCA Timestamp not found', $this->params);
        } else {
            $key = explode(' ', $this->request->header('Authorization'));
            if (@$key[0] != "Bearer") {
                return showOutputResponseBCAVa('GetBillVaBCA', 'OUT', '', 'ApiBCA', 401, false, 'Autentikasi salah', 'You Unauthorized!', $this->params);
            } else if (!GeneralModel::isValidTokenBca(@$key[1])) {
                return showOutputResponseBCAVa('GetBillVaBCA', 'OUT', '', 'ApiBCA', 401, false, 'Access token salah', 'Access token invalid', $this->params);
            } else {



                $validator = Validator::make(json_decode($request, true), [
                    'CompanyCode' => 'required|min:5|max:5',
                    'CustomerNumber' => 'required|min:6|max:18',
                    'RequestID' => 'required',
                    'ChannelType' => 'required|min:4|max:4',
                    'TransactionDate' => 'required'
                ]);

                if ($validator->fails()) {
                    $errorString = getKeyErrorToString(json_decode($validator->errors(), true));
                    return showOutputResponseBCAVa('GetBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, $errorString, $errorString, $this->params);
                } else if (!isValidDatetimeFormat($this->params->TransactionDate, 'dd/mm/yyyy')) {
                    return showOutputResponseBCAVa('GetBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'Format tanggal tidak valid. ex: dd/mm/yyyy hh:ii:ss', 'Date format not valid. ex: dd/mm/yyyy hh:ii:ss', $this->params);
                } else {
                    $dataCust = BcaModel::getDataTransactionVa($this->params, array('OPEN', 'REQUESTED'));
                    if ($dataCust->exists()) {
                        $dataCust = $dataCust->first();

                        if (isExpiredOTP($dataCust->date_expired)) {
                            return showOutputResponseBCAVa('GetBillVaBCA', 'OUT', '', 'ApiBCA', 200, false, "Nomor VA sudah kadaluarsa pada " . $dataCust->date_expired, "Your VA Number already expired in " . $dataCust->date_expired, $this->params);
                        } else {
                            $dataUpdate = array(
                                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                'request_id' => $this->params->RequestID,
                                'status' => 'REQUESTED',
                            );
                            BcaModel::updateDataTransactionVa($dataCust->id_va, $dataUpdate);
                            BcaModel::insertDataTransactionVaLog(array('id_va' => $dataCust->id_va, 'request_id' => $this->params->RequestID, 'created_at' => Carbon::now()));
                            GeneralModel::closeToken(@$key[1]);

                            $response = array(
                                "CompanyCode" => (string)$dataCust->company_code,
                                "CustomerNumber" => (string)$dataCust->cust_number,
                                "InquiryStatus" => "00",
                                "InquiryReason" => array(
                                    "Indonesian" => "Sukses",
                                    "English" => "Success"
                                ),
                                "RequestID" => $this->params->RequestID,
                                "CustomerName" => $dataCust->cust_name,
                                "CurrencyCode" => "IDR",
                                "TotalAmount" => $dataCust->amount . '.00',
                                "SubCompany" => "00000",
                                "DetailBills" => array(),
                                "FreeTexts" => array(),
                                "AdditionalData" => $dataCust->description
                            );

                            $resData = response()->json($response, 200);
                            GeneralModel::insertLogRequest('GetBillVaBCA', 'RESPONSE', $resData, 'ApiBCA');

                            return $resData;
                        }
                    } else {
                        return showOutputResponseBCAVa('GetBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, "Nomor Pelanggan Tidak Ditemukan", "Customer Number Not Found");
                    }
                }
            }
        }
    }
    public function GetPaymentBillVaBCA()
    {
        GeneralModel::insertLogRequest('GetPaymentBillVaBCA', 'HEADER_REQUEST', $this->request, 'ApiBCA');

        $request = $this->request->getContent();
        $this->params = json_decode($request);

        if (!$this->request->header('Authorization')) {
            return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'Autentikasi tidak di temukan', 'Authorization not found', $this->params);
        } else if (!$this->request->header('X-Bca-Key')) {
            return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'BCA Key tidak di temukan', 'BCA Key not found', $this->params);
        } else if ($this->request->header('X-Bca-Key') != checkCredential($this->isUat, 'BCAApiKey')) {
            return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'BCA Key salah', 'BCA Key incorrect', $this->params);
        } else if (!$this->request->header('X-Bca-Signature')) {
            return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'BCA Signature tidak di temukan', 'BCA Signature not found', $this->params);
        } else if (!$this->request->header('X-Bca-Timestamp')) {
            return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'BCA Timestamp tidak di temukan', 'BCA Timestamp not found', $this->params);
        } else {
            $key = explode(' ', $this->request->header('Authorization'));
            if (@$key[0] != "Bearer") {
                return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 401, false, 'Autentikasi salah', 'You Unauthorized!', $this->params);
            } else if (!GeneralModel::isValidTokenBca(@$key[1])) {
                return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 401, false, 'Access token salah', 'Access token invalid', $this->params);
            } else {

                $validator = Validator::make(json_decode($request, true), [
                    'CompanyCode' => 'required|min:5|max:5',
                    'CustomerNumber' => 'required|min:6|max:18',
                    'RequestID' => 'required',
                    'ChannelType' => 'required|min:4|max:4',
                    'TransactionDate' => 'required',
                    'CustomerName' => 'required',
                    'CurrencyCode' => 'required',
                    'PaidAmount' => 'required',
                    'TotalAmount' => 'required',
                    'SubCompany' => 'required',
                    'FlagAdvice' => 'required',
                    'Reference' => 'required',
                ]);

                if ($validator->fails()) {
                    $errorString = getKeyErrorToString(json_decode($validator->errors(), true));
                    return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, $errorString, $errorString, $this->params);
                } else if (!isValidDatetimeFormat($this->params->TransactionDate, 'dd/mm/yyyy')) {
                    return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, 'Format tanggal tidak valid. ex: dd/mm/yyyy hh:ii:ss', 'Date format not valid. ex: dd/mm/yyyy hh:ii:ss', $this->params);
                } else {
                    if (!isValidFlagAdvice($this->params->FlagAdvice)) { // INI HARUS Y DAN N
                        return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, "FlagAdvice harus Y atau N", "FlagAdvice must Y or N", $this->params);
                    } else {
                        $dataCust = BcaModel::getDataTransactionVa($this->params, array('REQUESTED'));
                        if ($dataCust->exists()) {
                            $dataCust = $dataCust->first();

                            if ($dataCust->amount != $this->params->TotalAmount) {
                                return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, "TotalAmount harus sama dengan hasil BillPresentment", "TotalAmount must be equal with BillPresentment", $this->params);
                            } else {

                                $dataUpdate = array(
                                    'date_payment' => Carbon::now()->format('Y-m-d H:i:s'),
                                    'status' => 'PAID',
                                );
                                BcaModel::updateDataTransactionVa($dataCust->id_va, $dataUpdate);
                                GeneralModel::closeToken(@$key[1]);

                                $response = array(
                                    "CompanyCode" => (string)$dataCust->company_code,
                                    "CustomerNumber" => (string)$dataCust->cust_number,
                                    "RequestID" => $this->params->RequestID,
                                    "PaymentFlagStatus" => "00",
                                    "PaymentFlagReason" => array(
                                        "Indonesian" => "Sukses",
                                        "English" => "Success"
                                    ),

                                    "CustomerName" => $dataCust->cust_name,
                                    "CurrencyCode" => "IDR",
                                    "PaidAmount" => $dataCust->amount . '.00',
                                    "TotalAmount" => $dataCust->amount . '.00',
                                    "TransactionDate" => $this->params->TransactionDate,
                                    "DetailBills" => array(),
                                    "FreeTexts" => array(),
                                    "AdditionalData" => $dataCust->description
                                );

                                $resData = response()->json($response, 200);
                                GeneralModel::insertLogRequest('GetPaymentBillVaBCA', 'RESPONSE', $resData, 'ApiBCA');

                                return $resData;
                            }
                        } else {
                            return showOutputResponseBCAVa('GetPaymentBillVaBCA', 'OUT', '', 'ApiBCA', 404, false, "Nomor Pelanggan Tidak Ditemukan", "Customer Number Not Found", $this->params);
                        }
                    }
                }
            }
        }
    }
    private function GetStatusPaymentVA()
    {
        $data = "";
        $url = '/va/payments?CompanyCode=' . checkCredential($this->isUat, 'CorporateId') . '&RequestID=' . $this->params->RequestID;
        $ch = self::sendCurlBcaApi($this->params->Func, $url, self::GetToken(), $data, false, 'GET');
        if (!isset($ch->ErrorCode)) {
            return   showOutputResponse($url, true, 'Transaksi berhasil', $ch, 200);
        } else {
            if (isset($ch->ErrorCode)) {
                return   showOutputResponse($url, false, $ch->ErrorMessage->Indonesian, $ch, 200);
            } else {
                return   showOutputResponse($url, false, @$ch->AccountDetailDataFailed[0]->Indonesian, array("status" => 01), 200);
            }
        }
    }

    public function GenerateNumberVA()
    {
        $validator = Validator::make(json_decode($this->request, true), [
            'company_code' => 'required|min:5|max:5',
            'cust_number' => 'required|min:6|max:18',
            'cust_name' => 'required',
            'channel_type' => 'required|min:4|max:4',
            'amount' => 'required'
        ]);

        if ($validator->fails()) {
            $errorString = getKeyErrorToString(json_decode($validator->errors(), true));
            return showOutputResponseBCA('GenerateNumberVA', 'OUT', '', 'ApiBCA', 404, false, $errorString, $errorString);
        } else {
            return BcaModel::insertDataTransactionVa($this->params);
        }
    }

    public function GetNotification()
    {

        $request = $this->request->getContent();
        $this->params = json_decode($request);
        $key = explode(' ', $this->request->header('Authorization'));

        // if (!$this->request->header('companycode')) {
        //     return showOutputResponseBCA('GetNotification', 'OUT', '', 'ApiBCA', 400, false, 'Kode Perusahaan Diperlukan', 'Company Code Needed!');
        // } else 

        if (!$this->request->header('Authorization')) {
            return showOutputResponseBCA('GetNotification', 'OUT', '', 'ApiBCA', 400, false, 'Autentikasi diperlukan', 'Authorization needed!');
        } else if (@$key[0] != "Bearer") {
            return showOutputResponseBCA('GetNotification', 'OUT', '', 'ApiBCA', 400, false, 'Autentikasi diperlukan', 'Authorization needed!');
        } else if (!GeneralModel::isValidTokenBca(@$key[1])) {
            return showOutputResponseBCA('GetNotification', 'OUT', '', 'ApiBCA', 400, false, 'Access token salah', 'Access token invalid');
        } else {
            $validator = Validator::make(json_decode($request, true), [
                'BankCode' => 'required',
                'AccountNumber' => 'required',
                'BranchCode' => 'required',
                'StatementType' => 'required',
                'Amount' => 'required',
                'SignAmount' => 'required',
                'Currency' => 'required',
                'ReferenceNumber' => 'required',
                'TransactionDate' => 'required',
                'TransactionTime' => 'required',
                'TransactionRemarks' => 'required',
                'IsReversal' => 'required'
            ]);

            if ($validator->fails()) {
                $errorString = getKeyErrorToString(json_decode($validator->errors(), true));
                return showOutputResponseBCA('GetNotification', 'OUT', '', 'ApiBCA', 400, false, 'Parameter ' . $errorString . ' diperlukan', 'Parameter: ' . $errorString . " Required");
            } else {

                // $param = array(
                //     'transaction_date' => $this->params->TransactionDate,
                //     'amount' => $this->params->Amount,
                //     'type' => 'IN',
                //     'status' => 'PENDING',
                // );
                // $listDataTransaction = BcaModel::getTransactionBank($param);
                // return $listDataTransaction;
                //exit;

                // echo $listDataTransaction;

                $response = array(
                    "RequestReferenceNumber" => $this->params->ReferenceNumber,
                    "ResponseCode" => "00",
                    "ResponseReason" => 'Sukses',
                );

                return  response()->json($response, 200);
            }


            // echo ;
            // $dataNotif= (object)array(
            //     'title'=>"Transaction Title",
            //     'message'=>"Transaction Message",
            //     'image'=>'-',
            //     'data'=>array('team'=>'Indonesia') );
            // sendPushNotification($FirebaseId,$dataNotif);

            // $dataMail = array(
            //     'to'=>$to,
            //     'subject'=>"Subject",
            //     'message'=>"Message",
            //     'name'=>'Admin Gerai Daya',
            //     'otp'=>"",
            //     'isbanyak'=>false
            // );
            // sendEMAIL($dataMail);


            // return   showOutputResponse('GetNotification', true, 'Notification saved', array(), 200);
        }
    }

    public function GetNotificationTolakan()
    {
        $request = $this->request->getContent();
        $this->params = json_decode($request);
        $key = explode(' ', $this->request->header('Authorization'));

        if (!$this->request->header('Authorization')) {
            return showOutputResponseBCA('GetNotificationTolakan', 'OUT', '', 'ApiBCA', 404, false, 'Autentikasi diperlukan', 'Authorization needed!');
        } else if (@$key[0] != "Bearer") {
            return showOutputResponseBCA('GetNotificationTolakan', 'OUT', '', 'ApiBCA', 404, false, 'Autentikasi diperlukan', 'Authorization needed!');
        } else if (!GeneralModel::isValidTokenBca(@$key[1])) {
            return showOutputResponseBCA('GetNotificationTolakan', 'OUT', '', 'ApiBCA', 404, false, 'Access token salah', 'Access token invalid');
        } else if (!$this->request->header('X-Pass-Signature')) {
            return showOutputResponseBCA('GetNotificationTolakan', 'OUT', '', 'ApiBCA', 404, false, 'Signature tidak cocok', 'Signature not match');
        } else {
            $validator = Validator::make(json_decode($request, true), [
                'TransactionID' => 'required|max:8',
                'TransactionDate' => 'required|max:10',
                'TransferType' => 'required|max:3',
                'PPUNumber' => 'required|max:5',
                'AccountNoFrom' => 'required|max:10',
                'AccountNoTo' => 'required|max:34',
                'ReceiverBankCode' => 'required|max:8',
                'ReceiverName' => 'required|max:30',
                'Amount' => 'required|max:16',
                'RejectStatusID' => 'required',
                'RejectStatusEN' => 'required',
            ]);

            if ($validator->fails()) {
                $errorString = getKeyErrorToString(json_decode($validator->errors(), true));
                return showOutputResponseBCA('GetNotificationTolakan', 'OUT', '', 'ApiBCA', 404, false, 'Parameter ' . $errorString . ' diperlukan', 'Parameter: ' . $errorString . " Required");
            } else if (!isValidDatetimeFormat($this->params->TransactionDate, 'yyyy-mm-dd')) {
                return showOutputResponseBCAVa('GetNotificationTolakan', 'OUT', '', 'ApiBCA', 404, false, 'Format tanggal tidak valid. ex: yyyy/mm/dd', 'Date format not valid. ex: yyyy/mm/dd', $this->params);
            } else {
                // $param = array(
                //     'transaction_id' => $this->params->TransactionID,
                //     'transaction_date' => $this->params->TransactionDate
                //     // 'amount' => $this->params->Amount,
                //     // 'source_account_number' => $this->params->AccountNoFrom,
                //     // 'beneficiary_account_number' => $this->params->AccountNoTo,
                //     // 'beneficiary_name' => $this->params->ReceiverName,
                //     // 'beneficiary_bank_code' => $this->params->ReceiverBankCode,
                //     // 'transfer_type' => $this->params->TransferType
                // );
                // $response = BcaModel::getTransactionBank($param);
                // if ($response) {
                //     BcaModel::getTransactionBank($response->id, array('status' => 'REFUND', 'updated_at' => Carbon::now()));
                // }



                $response = array(
                    "ResponseWS" => "1",
                );

                return  response()->json($response, 200);

                // return   showOutputResponse('GetNotificationTolakan', true, 'Notification saved', array(), 200);



                // $dataNotif= (object)array(
                //     'title'=>"Transaction Title",
                //     'message'=>"Transaction Message",
                //     'image'=>'-',
                //     'data'=>array('team'=>'Indonesia') );
                // sendPushNotification($FirebaseId,$dataNotif);

                // $dataMail = array(
                //     'to'=>$to,
                //     'subject'=>"Subject",
                //     'message'=>"Message",
                //     'name'=>'Admin Gerai Daya',
                //     'otp'=>"",
                //     'isbanyak'=>false
                // );
                // sendEMAIL($dataMail);

            }
        }
    }
}
