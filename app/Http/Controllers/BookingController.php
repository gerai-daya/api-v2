<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\GeneralModel;
use App\Models\Booking;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;


class BookingController extends Controller
{
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
        $this->params='';
        $this->requestContent='';
    }

    public function index(Request $request) {

        $this->request = $request;
        if(!$this->request->status){
          return  showOutputResponse("tokenLoginExpired",false,'Token Anda Invalid, Silakan login ulang',array(),401,array('type'=>'IN','request'=>$this->request,'location'=>'BookingController'));
          exit;
        }

        
        if ($request->isMethod('post')) {
            $this->requestContent = $request->getContent();
            $this->params = json_decode($this->requestContent);
            
            $validator = Validator::make(json_decode($this->requestContent,true),[
                'Func' =>'required'
            ]);
      
            if($validator->fails()){
              return  showOutputResponse("paramaterFunctionNotFound",false,'Parameter Not Found!',$validator->errors(),404,array('type'=>'IN','request'=>$this->requestContent,'location'=>'BookingController'));
            }else{
                if(method_exists($this,$this->params->Func)){
                  GeneralModel::insertLogRequest($this->params->Func,'IN',$this->requestContent,'BookingController');
                  return $this->{$this->params->Func}();
                }else{
                  return  showOutputResponse('FunctionNotFound',false,'Function Not Found!','Function ['.@$this->params->Func.'] not found!',404,array('type'=>'IN','request'=>$this->requestContent,'location'=>'BookingController'));
                }
            }
        }else{
          return  showOutputResponse('methodNotAllowed',false,'Not Found!','Not Found!',405,array('type'=>'IN','request'=>$this->requestContent,'location'=>'BookingController'));
        }
      }

    public function OrderPekerjaan(){
          $validator = Validator::make(json_decode($this->requestContent,true),[
              'id_petugas' =>'required',
              'id_pelanggan'=>'required',
              'jenis_layanan'=>'required',
              'alamat'=>'required',
              'petunjuk'=>'required',
              'keluhan'=>'required',
              'cara_bayar'=>'required',
              'is_command_center'=>'required'
          ]);

        if($validator->fails()){
          return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
        }else{
          $statusKerja="FREE";
          $dataPetugas="";
          if($this->params->is_command_center!="Y"){
            $dataPetugas = Petugas::getPetugasById($this->params->id_petugas);
            $statusKerja=$dataPetugas->status_kerja;
          }
          if($statusKerja=="FREE"){
            // echo "123";
            return Booking::OrderPekerjaan($this->params,$dataPetugas);
          }else{
            return  showOutputResponse($this->params->Func,false,'Mohon maaf, Petugas yang bersangkutan telah menghandle pekerjaan lain.',array(),200);
          }
        }
    }

    public function getHomePetugas(){
      $validator = Validator::make(json_decode($this->requestContent,true),[
          'id_petugas' =>'required'
      ]);

    if($validator->fails()){
      return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
    }else{

      $homePetugas = Booking::getHomePelanggan($this->params->id_petugas);
      $dataOnprogress = Booking::getOnprogressPetugas($this->params->id_petugas);
      $dataLastWork = Booking::getLastWorkPetugas($this->params->id_petugas);
          
      
      $dataLayananPetugas = array();
      $datadummy = array();
      foreach ($dataOnprogress as $i => $rows) {
          $param = (object)array('id_booking'=>$rows->id_booking);
          $dataBooking = Booking::getDataBooking($param);
          $dataBookingDetail = Booking::getDataBookingDetail($param);
          $dataBookingStatus = Booking::getDataBookingStatus($param);
          if(in_array($dataBooking[0]->status_order,array("SURVEY_SELESAI","DITERIMA","DIKERJAKAN") )==1){
            $dataLayananPetugas = Booking::getLayananPetugas($dataBooking[0]->id_petugas,$dataBooking[0]->kode_layanan);
          }

          $dataOnprogress[$i]->data = array('rows_status'=>$dataBookingStatus,'rows_utama'=>$dataBooking,'rows_detail'=>$dataBookingDetail,'rows_pekerjaan_petugas'=>$dataLayananPetugas);
      }

      $data = array(
          'row_home_petugas'=>$homePetugas,
          'rows_progress'=>$dataOnprogress,
          'rows_aktivitas'=>$dataLastWork,
      );
      return  showOutputResponse($this->params->Func,true,'Data ditemukan',$data,200);
    }
  }


  public function getHomePelanggan(){
    $validator = Validator::make(json_decode($this->requestContent,true),[
      'id_pelanggan' =>'required'
      ]);

    if($validator->fails()){
      return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
    }else{

      $dataOnprogress = Booking::getOnprogressPelanggan($this->params->id_pelanggan);
      $getSumarryPelanggan = Booking::getSummaryPelanggan($this->params->id_pelanggan);
      $datadummy = array();
      foreach ($dataOnprogress as $i => $rows) {
          $param = (object)array('id_booking'=>$rows->id_booking);
          $dataBooking = Booking::getDataBooking($param);
          $dataBookingDetail = Booking::getDataBookingDetail($param);
          $dataBookingStatus = Booking::getDataBookingStatus($param);
          $dataOnprogress[$i]->data = array('rows_utama'=>$dataBooking,'rows_status'=>$dataBookingStatus,'rows_detail'=>$dataBookingDetail);
      }

      $result = array("result_summary"=>$getSumarryPelanggan,"history"=>$dataOnprogress);
      
      return  showOutputResponse($this->params->Func,true,'Data ditemukan',$result,200);
    }
  }
  
    public function getOnprogressPetugas(){
        $validator = Validator::make(json_decode($this->requestContent,true),[
          'id_petugas' =>'required'
      ]);

      if($validator->fails()){
        return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
      }else{


        $dataOnprogress = Booking::getOnprogressPetugas($this->params->id_petugas);

        $dataLayananPetugas = array();
        $datadummy = array();
        foreach ($dataOnprogress as $i => $rows) {
            $param = (object)array('id_booking'=>$rows->id_booking);
            $dataBooking = Booking::getDataBooking($param);
            $dataBookingDetail = Booking::getDataBookingDetail($param);
            $dataBookingStatus = Booking::getDataBookingStatus($param);
            if(in_array($dataBooking[0]->status_order,array("SURVEY_SELESAI","DITERIMA","DIKERJAKAN") )==1){
              $dataLayananPetugas = Booking::getLayananPetugas($dataBooking[0]->id_petugas,$dataBooking[0]->kode_layanan);
            }
  
            $dataOnprogress[$i]->data = array('rows_status'=>$dataBookingStatus,'rows_utama'=>$dataBooking,'rows_detail'=>$dataBookingDetail,'rows_pekerjaan_petugas'=>$dataLayananPetugas);
        }

        
        return  showOutputResponse($this->params->Func,true,'Data ditemukan',$dataOnprogress,200);
      }
    }
    
    public function getLastWorkPetugas(){
        $validator = Validator::make(json_decode($this->requestContent,true),[
          'id_petugas' =>'required'
      ]);

      if($validator->fails()){
        return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
      }else{
        $data = Booking::getLastWorkPetugas($this->params->id_petugas);
        return  showOutputResponse($this->params->Func,true,'Data ditemukan',$data,200);
      }
    }

    public function getHistoriOrder(){
        $validator = Validator::make(json_decode($this->requestContent,true),[
          'id_pelanggan' =>'required',
          'id_petugas' =>'required',
          'tipe_user' =>'required',
          'bulan' =>'required',
          'tahun' =>'required',
      ]);

      if($validator->fails()){
        return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
      }else{
        $data = Booking::getHistoriOrder($this->params);
        
        return  showOutputResponse($this->params->Func,count($data)>0?true:false,count($data)>0?'Data ditemukan':'Data tidak di temukan',$data,200);
      }
    }
    
    public function getHistoriPendapatan(){
        $validator = Validator::make(json_decode($this->requestContent,true),[
          'id_petugas' =>'required',
          'bulan' =>'required',
          'tahun' =>'required',
      ]);

      if($validator->fails()){
        return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
      }else{
        $data = Booking::getHistoriPendapatan($this->params);
        return  showOutputResponse($this->params->Func,count($data)>0?true:false,count($data)>0?'Data ditemukan':'Data tidak di temukan',$data,200);
      }
    }


    public function getDetailOrderPekerjaan(){
      $validator = Validator::make(json_decode($this->requestContent,true),[
        'id_booking' =>'required',
        'tipe_user' =>'required'
    ]);

    if($validator->fails()){
      return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
    }else{
      $dataBooking = Booking::getDataBooking($this->params);
      $dataBookingDetail = Booking::getDataBookingDetail($this->params);
      $dataBookingStatus = Booking::getDataBookingStatus($this->params);
      $dataLayananPetugas = array();
      if($this->params->tipe_user=="PETUGAS"){
          if(in_array($dataBooking[0]->status_order,array("SURVEY_SELESAI","DITERIMA","DIKERJAKAN") )==1){
            $dataLayananPetugas = Booking::getLayananPetugas($dataBooking[0]->id_petugas,$dataBooking[0]->kode_layanan);
          }
      }
      $result=array(
            'rows_status'=>$dataBookingStatus,
            'rows_utama'=>$dataBooking,
            'rows_detail'=>$dataBookingDetail,
            'rows_pekerjaan_petugas'=>$dataLayananPetugas,
        );
      
      return  showOutputResponse($this->params->Func,true,'Data ditemukan',$result,200);
    }
  }
  
  public function getLayananPekerjaanByIdPetugas(){

      $validator = Validator::make(json_decode($this->requestContent,true),[
          'id_petugas' =>'required',
          'layanan' =>'required'
      ]);

    if($validator->fails()){
      return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
    }else{
      $result = Booking::getLayananPetugas($this->params->id_petugas,$this->params->layanan);
      return  showOutputResponse($this->params->Func,true,'Data ditemukan',$result,200);
    }
  }


  public function updatePekerjaanBooking(){
      $validator = Validator::make(json_decode($this->requestContent,true),[
        'id_booking' =>'required',
        'tipe_user' =>'required',
        'id_pelanggan' =>'required',
        'id_petugas' =>'required',
        'aksi_cancel' =>'required',
        // 'alasan_penolakan' =>'required',
        'lat_petugas' =>'required',
        'longi_petugas' =>'required',
        'diskon' =>'required',
        'request' =>'required',
    ]);

  if($validator->fails()){
    return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
  }else{
    return Booking::updatePekerjaanBooking($this->params);
  }
}

public function updateBookingDetail(){
      $validator = Validator::make(json_decode($this->requestContent,true),[
        'id_booking' =>'required',
        'data_json' =>'required',
        'note' =>'required'
    ]);

  if($validator->fails()){
    return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
  }else{
    return Booking::updateBookingDetail($this->params);
  }
}

public function managQtyBookingDetail(){
      $validator = Validator::make(json_decode($this->requestContent,true),[
        'id_booking_detail' =>'required',
        'id_booking' =>'required',
        'mode' =>'required',
    ]);

  if($validator->fails()){
    return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
  }else{
    return Booking::managQtyBookingDetail($this->params);
  }
}

public function updateDiskonBooking(){
      $validator = Validator::make(json_decode($this->requestContent,true),[
        'id_booking' =>'required',
    ]);

  if($validator->fails()){
    return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
  }else{
    return Booking::updateDiskonBooking($this->params);
  }
}


public function updateFotoBooking(){

    $image = @$this->request->input('image');
    $fileName = Carbon::now()->format('YmdHis').rand(1000,9999).".png";
    $this->prm = (object)array(
        'Func'=>"updateFotoBooking",
        'id_petugas'=>$this->request->input('id_petugas'),
        'id_booking'=>$this->request->input('id_booking'),
        'tipe_upload'=>$this->request->input('tipe_upload')
      );

      GeneralModel::insertLogRequest($this->prm->Func,'IN',json_encode($this->prm),'BookingController');
      if (!empty($image)) {
        try {
          $file  = file_put_contents(config('app.path_booking').$fileName, base64_decode($image));
          $dataBooking = Booking::getDataBookingById($this->request->input('id_booking'));

            if ($dataBooking->exists()){
              $dtaBokg = $dataBooking->first();

              if($this->prm->tipe_upload=="sebelum"){
                  if(strlen($dtaBokg->foto_sebelum)>0){
                    if(file_exists(config('app.path_booking').$dtaBokg->foto_sebelum)){
                      unlink(config('app.path_booking').$dtaBokg->foto_sebelum);
                    }
                  }
                Booking::updateFotoBooking(array('foto_sebelum'=>$fileName),$this->prm->id_booking); 
              }else{
                  if(strlen($dtaBokg->foto_sesudah)>0){
                    if(file_exists(config('app.path_booking').$dtaBokg->foto_sesudah)){
                      unlink(config('app.path_booking').$dtaBokg->foto_sesudah);
                    }
                  }
                Booking::updateFotoBooking(array('foto_sesudah'=>$fileName),$this->prm->id_booking);
              } 
            }
            return  showOutputResponse($this->prm->Func,true,"Proses update data berhasil",array('url_photo'=>config('app.url_booking').$fileName),200);

        } catch (\Throwable $th) {
          GeneralModel::insertLogRequest($this->prm->Func,'ERROR',$th,'BookingController');
          return  showOutputResponse($this->prm->Func,false,"Proses update data gagal, coba beberapa saat lagi",array(),200);
        }
          
      }else{
        return  showOutputResponse($this->prm->Func,false,"Proses update data gagal, File foto tidak ditemukan",array(),200);
      }
      
}



public function TambahReview(){
    $validator = Validator::make(json_decode($this->requestContent,true),[
        'id_booking' =>'required',
        'masukan' =>'required',
    ]);

    if($validator->fails()){
      return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
    }else{
      return Booking::TambahReview($this->params);
    }
}

public function updateLocation(){
  $validator = Validator::make(json_decode($this->requestContent,true),[
    'tipe_user' =>'required',
    'latitude' =>'required',
    'longitude' =>'required',
    'id_petugas' =>'required'
]);

if($validator->fails()){
  return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),404);
}else{
  return Booking::updateLocation($this->params);
}
}



public function getDataEmail(){
    $data = Booking::getDataKwitansiBooking($this->params->id_booking);

    kirimKwitansi($data);
    // $dataJson = json_decode($data,true);
    // var_dump($dataJson);
 
    // echo ($data['data_booking']->tanggal);
    //  echo json_encode($data);
}






}
