<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Pelanggan;
use App\Models\GeneralModel;
use App\Models\DepositModel;
use Illuminate\Support\Str;
use Auth;
use Carbon\Carbon;

class DepositController extends Controller
{
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
        $this->params='';
        $this->requestContent='';
    }

    public function index(Request $request) {

        $this->request = $request;
        if(!$this->request->status){
          return  showOutputResponse("tokenLoginExpired",false,'Token Anda Invalid, Silakan login ulang',array(),401,array('type'=>'IN','request'=>$this->request,'location'=>'DepositController'));
          exit;
        }

        if ($request->isMethod('post')) {
            $this->requestContent = $request->getContent();
            $this->params = json_decode($this->requestContent);
            
            $validator = Validator::make(json_decode($this->requestContent,true),[
                'Func' =>'required'
            ]);
      
            if($validator->fails()){
              return  showOutputResponse("paramaterFunctionNotFound",false,'Parameter Not Found!',$validator->errors(),404,array('type'=>'IN','request'=>$this->requestContent,'location'=>'DepositController'));
            }else{
                if(method_exists($this,$this->params->Func)){
                  GeneralModel::insertLogRequest($this->params->Func,'IN',$this->requestContent,'DepositController');
                  return $this->{$this->params->Func}();
                }else{
                  return  showOutputResponse('FunctionNotFound',false,'Function Not Found!','Function ['.@$this->params->Func.'] not found!',404,array('type'=>'IN','request'=>$this->requestContent,'location'=>'DepositController'));
                }
            }
        }else{
          return  showOutputResponse('methodNotAllowed',false,'Not Found!','Not Found!',405,array('type'=>'IN','request'=>$this->requestContent,'location'=>'DepositController'));
        }
    }

   
    public function getListHistoryDeposit(){
        $validator = Validator::make(json_decode($this->requestContent,true),[
            'id_petugas' =>'required',
            'bulan' =>'required',
            'tahun' =>'required',
        ]);
        if($validator->fails()){
          return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),200);
        }else{
          $response = DepositModel::getListHistoryDeposit($this->params);
          return   showOutputResponse($this->params->Func,true,'Data ditemukan',$response,200);
        }
    }
    
    public function manajemenDeposit(){

        $image = @$this->request->input('image');
        $fileName = Carbon::now()->format('YmdHis').rand(1000,9999).".png";
        $this->prm = (object)array(
            'id_petugas'=>$this->request->input('id_petugas'),
            'id_bank'=>$this->request->input('id_bank'),
            'jumlah'=>$this->request->input('jumlah'),
            'atas_nama'=>$this->request->input('atas_nama'),
            'bank'=>$this->request->input('bank'),
            'tgl_transfer'=>$this->request->input('tgl_transfer'),
            'id_deposit'=>$this->request->input('id_deposit'),
            'user'=>$this->request->input('user'),
            'Func'=>'manajemenDeposit',
            'bulan'=>Carbon::now()->format('m'),
            'tahun'=>Carbon::now()->format('Y'),
          );

          GeneralModel::insertLogRequest($this->prm->Func,'IN',json_encode($this->prm),'DepositController');

          if (!empty($image)) {
            try {
              $file  = file_put_contents(config('app.path_bukti_deposit').$fileName, base64_decode($image));
              $dataPermohonan = DepositModel::getPermohonanDepositById($this->request->input('id_deposit'));

              if(file_exists(config('app.path_bukti_deposit').$fileName)){

                  if ($dataPermohonan->exists()){
                    $dataPermohonan = $dataPermohonan->first();
                    if(file_exists(config('app.path_bukti_deposit').$dataPermohonan->url_foto)){
                      unlink(config('app.path_bukti_deposit').$dataPermohonan->url_foto);
                    }
                  }
                  return DepositModel::manajemenDeposit($this->prm,true,$fileName);
              }else{
                  return DepositModel::manajemenDeposit($this->prm,false,"");
              }

            } catch (\Throwable $th) {
              GeneralModel::insertLogRequest($this->prm->Func,'ERROR',$th,'DepositController');
              return  showOutputResponse($this->prm->Func,false,"Proses update data gagal, coba beberapa saat lagi",array(),200);
            }
              
          }else{
            return DepositModel::manajemenDeposit($this->prm,false,"");
          }

         
        
    }
}
