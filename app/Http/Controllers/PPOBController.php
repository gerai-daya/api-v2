<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\GeneralModel;
use App\Models\PPOBModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class PPOBController extends Controller
{
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
        $this->params='';
        $this->requestContent='';
    }

    public function index(Request $request) {

        $this->request = $request;
        if(!$this->request->status){
          return  showOutputResponse("tokenLoginExpired",false,'Token Anda Invalid, Silakan login ulang',array(),401,array('type'=>'IN','request'=>$this->request,'location'=>'UserController'));
          exit;
        }

        
        if ($request->isMethod('post')) {
            $this->requestContent = $request->getContent();
            $this->params = json_decode($this->requestContent);
            
            $validator = Validator::make(json_decode($this->requestContent,true),[
                'Func' =>'required'
            ]);
      
            if($validator->fails()){
              return  showOutputResponse("paramaterFunctionNotFound",false,'Parameter Not Found!',$validator->errors(),404,array('type'=>'IN','request'=>$this->requestContent,'location'=>'UserController'));
            }else{
                if(method_exists($this,$this->params->Func)){
                  GeneralModel::insertLogRequest($this->params->Func,'IN',$this->requestContent,'UserController');
                  return $this->{$this->params->Func}();
                }else{
                  return  showOutputResponse('FunctionNotFound',false,'Function Not Found!','Function ['.@$this->params->Func.'] not found!',404,array('type'=>'IN','request'=>$this->requestContent,'location'=>'UserController'));
                }
            }
        }else{
          return  showOutputResponse('methodNotAllowed',false,'Not Found!','Not Found!',405,array('type'=>'IN','request'=>$this->requestContent,'location'=>'UserController'));
        }
      }

    public function getAllDenomByCategory(){
       
        $denom = PPOBModel::getAllDenomByCategory($this->params);
        $provider = PPOBModel::getAllProvider();
        return showOutputResponse($this->params->Func,true,'Data ditemukan',array('denom'=>$denom,'provider'=>$provider),200); 
    } 

    public function getAllDenomByCategory1(){
       
        $denom = PPOBModel::getAllDenomByCategory($this->params);
        return showOutputResponse($this->params->Func,true,'Data ditemukan',$denom,200); 
    } 

    public function getNoFavorit(){
        $validator = Validator::make(json_decode($this->requestContent,true),[
            'id_user' =>'required',
            'tipe_user' =>'required'
        ]);
  
        if($validator->fails()){
          return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),401);
        }else{
            $nofav = PPOBModel::getNoFavorit($this->params);
            if($nofav->count()>0){
                return showOutputResponse($this->params->Func,true,'Data ditemukan',$nofav,200); 
            }else{
                return showOutputResponse($this->params->Func,false,'Data tidak ditemukan',$nofav,200); 
            }
        }
    }

    public function getInqueryPPOB(){
        $denom = PPOBModel::getInqueryPPOB($this->params);
        return showOutputResponse($this->params->Func,true,'Data ditemukan',$denom,200); 
    } 


    public function managNoFavorit(){
        $response = PPOBModel::managNoFavorit($this->params);
        return $response;
    } 

    public function addTransaksiPPOB(){
        $response = PPOBModel::addTransaksiPPOB($this->params);
        return $response;
    } 

    
    
}
