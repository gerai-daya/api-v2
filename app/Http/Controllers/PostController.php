<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller{

    public function index(){
        $post = Post::all();
        // $getPost = Post::OrderBy("id", "DESC")->paginate(10);
        
        return response()->json([
            'success'=>true,
            'message'=>'List Data',
            'data'   =>$post
        ],200);
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'title' =>'required',
            'content' =>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'success'=>false,
                'message'=>'Semua kolom wajib isi',
                'data' => $validator->errors()
            ],401);
        }else{
            $post = Post::create([
                'title'=>$request->input('title'),
                'content'=>$request->input('content'),
            ]);

            if($post){
                return response()->json([
                    'success'=>true,
                    'message'=>'Data berhasi di tambahkan',
                    'data' => $post
                ],201);
            }else{
                return response()->json([
                    'success'=>false,
                    'message'=>'Data gagal di simpan'
                ],400);
            }
        }
    }

    public function show($id){
        $post = Post::find($id);
        if($post){
            return response()->json([
                'success'=>true,
                'message'=>'Detail post',
                'data' => $post
            ],200);
        }else{
            return response()->json([
                'success'=>false,
                'message'=>'Data tidak di temukan!'
            ],400);
        }
    }

    // public function showdata($id){
    //     $data = Post::where('id',$id)->get();
    //     return response ($data);
    // }

    


    public function update(Request $request,$id){

        $validator = Validator::make($request->all(),[
            'title' =>'required',
            'content' =>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'success'=>false,
                'message'=>'Semua kolom wajib isi',
                'data' => $validator->errors()
            ],401);
        }else{
            $post = Post::whereId($id)->update([
                'title'=>$request->input('title'),
                'content'=>$request->input('content'),
            ]);

            if($post){
                return response()->json([
                    'success'=>true,
                    'message'=>'Data berhasi di update',
                    'data' => $post
                ],201);
            }else{
                return response()->json([
                    'success'=>false,
                    'message'=>'Data gagal di simpan'
                ],400);
            }
        }
    }

    public function destroy($id){
        $post = Post:: whereId($id)->first();
        $post ->delete();
        if($post){
            return response()->json([
                'success'=>true,
                'message'=>'Data Berhasil di simpan!'
            ],200);
        }
    }




}
?>