<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\GeneralModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
        $this->params='';
        $this->requestContent='';
    }

    public function index(Request $request) {

        $this->request = $request;
        if(!$this->request->status){
          return  showOutputResponse("tokenLoginExpired",false,'Token Anda Invalid, Silakan login ulang',array(),401,array('type'=>'IN','request'=>$this->request,'location'=>'UserController'));
          exit;
        }

        
        if ($request->isMethod('post')) {
            $this->requestContent = $request->getContent();
            $this->params = json_decode($this->requestContent);
            
            $validator = Validator::make(json_decode($this->requestContent,true),[
                'Func' =>'required'
            ]);
      
            if($validator->fails()){
              return  showOutputResponse("paramaterFunctionNotFound",false,'Parameter Not Found!',$validator->errors(),404,array('type'=>'IN','request'=>$this->requestContent,'location'=>'UserController'));
            }else{
                if(method_exists($this,$this->params->Func)){
                  GeneralModel::insertLogRequest($this->params->Func,'IN',$this->requestContent,'UserController');
                  return $this->{$this->params->Func}();
                }else{
                  return  showOutputResponse('FunctionNotFound',false,'Function Not Found!','Function ['.@$this->params->Func.'] not found!',404,array('type'=>'IN','request'=>$this->requestContent,'location'=>'UserController'));
                }
            }
        }else{
          return  showOutputResponse('methodNotAllowed',false,'Not Found!','Not Found!',405,array('type'=>'IN','request'=>$this->requestContent,'location'=>'UserController'));
        }
      }

      public function setOnlineOffline(){
        $validator = Validator::make(json_decode($this->requestContent,true),[
            'id' =>'required',
            'status' =>'required'
        ]);

        if($validator->fails()){
            return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),200);
        }else{
          return Pelanggan::setOnlineOffline($this->params);  
        }
    }

    
    public function insertApproveTos(){
        $validator = Validator::make(json_decode($this->requestContent,true),[
          'id_user' =>'required',
          'tipe_user' =>'required',
      ]);
      if($validator->fails()){
        return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),401);
      }else{
          GeneralModel::insertApproveTos($this->params);
        return  showOutputResponse($this->params->Func,true,'Persetujuan telah kami terima. Terimakasih',"",200);
      }
    }
    
    public function managDataPelanggan(){
        $validator = Validator::make(json_decode($this->requestContent,true),[
          'id_user' =>'required',
          'mode_update' =>'required'
      ]);

      if($validator->fails()){
        return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),401);
      }else{
        return Pelanggan::managDataPelanggan($this->params);
      }
    } 
    
    public function managDataPetugas(){
        $validator = Validator::make(json_decode($this->requestContent,true),[
          'id_user' =>'required',
          'mode_update' =>'required'
      ]);

      if($validator->fails()){
        return  showOutputResponse($this->params->Func,true,'Parameter required',$validator->errors(),401);
      }else{
        return Petugas::managDataPetugas($this->params);
      }
    }
    
    public function updateFotoProfile(){
        $image = @$this->request->input('image');
        $id_user = @$this->request->input('id_user');
        $tipe_user = @$this->request->input('tipe_user');

        $fileName = Carbon::now()->format('YmdHis').rand(1000,9999).".png";
        $file  = file_put_contents(config('app.path_foto_profile').$fileName, base64_decode($image));

        
        if($tipe_user=="PETUGAS"){
          $dataUserOld = Petugas::getPetugasById($id_user);
            $res = DB::table('tb_petugas')->where('id_petugas',$id_user)->update(['foto'=>$fileName,'updated_at'=>Carbon::now()]);
            $dataUser = Petugas::getPetugasById($id_user);
        }else{
            $dataUserOld = Pelanggan::getPelangganById($id_user);
            $res = DB::table('tb_pelanggan')->where('id_pelanggan',$id_user)->update(['foto'=>$fileName,'updated_at'=>Carbon::now()]);
            $dataUser = Pelanggan::getPelangganById($id_user);
        }
        try{
          if(file_exists(config('app.path_foto_profile').$fileName)){
              if(strlen($dataUserOld->foto_ori)>3){
                  if(file_exists(config('app.path_foto_profile').$dataUserOld->foto_ori)){
                    unlink(config('app.path_foto_profile').$dataUserOld->foto_ori);
                  }
              }
              return  showOutputResponse("updateFotoProfile",true,'Perubahan Foto Berhasil ',array('data_user'=>$dataUser),200);
          }else{
              return  showOutputResponse("updateFotoProfile",false,'Perubahan Foto Gagal',array(),200);
          }
        } catch (\Throwable $th) {
          return  showOutputResponse("updateFotoProfile",false,"Proses update data gagal, coba beberapa saat lagi",array(),200);
        }
        

    }
    
    public function getProfile(){
        $validator = Validator::make(json_decode($this->requestContent,true),[
          'id_user' =>'required',
          'tipe_user' =>'required'
      ]);

      if($validator->fails()){
        return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),401);
      }else{
        $user = $this->params->tipe_user=="PETUGAS"?Petugas::getPetugasById($this->params->id_user):Pelanggan::getPelangganById($this->params->id_user);
        return  showOutputResponse($this->params->Func,true,'Data ditemukan',array('data_user'=>$user),200);
      }
    }

    public function uploadFile(){
      
      if(!$this->request->status){
        return  showOutputResponse("tokenLoginExpired",false,'Token Anda Invalid, Silakan login ulang',array(),401,array('type'=>'IN','request'=>$this->request,'location'=>'UserController'));
        exit;
      }else{
        $response = null;
        $user = (object) ['image' => ""];
  
        if ($this->request->hasFile('image')) {
            $original_filename = $this->request->file('image')->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $file_ext = end($original_filename_arr);
            $destination_path = './upload/';
            $image = 'U-' . time() . '.' . $file_ext;
  
            if ($this->request->file('image')->move($destination_path, $image)) {
                $user->image = '/upload/' . $image;
  
                return  showOutputResponse("uploadFile",true,'Success upload file',$user,200);
            } else {
                return  showOutputResponse("uploadFile",false,'Cannot upload file',array(),404);
            }
        } else {
            return  showOutputResponse("uploadFile",false,'File Not found!',array(),404);
        }
      }
    } 


    

   


}
