<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Pelanggan;
use App\Models\GeneralModel;
use App\Models\DepositModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;


class UserPublicController extends Controller
{
    
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
        $this->params='';
    }

    public function index(Request $request) {

      
      if ($request->isMethod('post')) {
          $this->request = $request->getContent();
          $this->params = json_decode($this->request);
          
          $validator = Validator::make(json_decode($this->request,true),[
              'Func' =>'required'
          ]);
    
          if($validator->fails()){
            return  showOutputResponse("paramaterFunctionNotFound",false,'Parameter Not Found!',$validator->errors(),404,array('type'=>'IN','request'=>$this->request,'location'=>'UserPublicController'));
          }else{
              if(method_exists($this,$this->params->Func)){
                GeneralModel::insertLogRequest($this->params->Func,'IN',$this->request,'UserPublicController');
                return $this->{$this->params->Func}();
              }else{
                return  showOutputResponse('FunctionNotFound',false,'Function Not Found!','Function ['.@$this->params->Func.'] not found!',404,array('type'=>'IN','request'=>$this->request,'location'=>'UserPublicController'));
              }
          }
      }else{
        return  showOutputResponse('methodNotAllowed',false,'Not Found!','Not Found!',405,array('type'=>'IN','request'=>$this->request,'location'=>'UserPublicController'));
      }

      
    }



   public function loginAccount(){
      $validator = Validator::make(json_decode($this->request,true),[
          'username' =>'required',
          'password' =>'required',
          'latitude' =>'required',
          'longitude' =>'required',
          'tipe_user' =>'required',
          'gcm_code' =>'required',
          'imei' =>'required',
    ]);

    // $value = Crypt::encrypt('admin');
    // try {
    //     $decrypted = Crypt::decrypt($value);
    //     echo $decrypted;
    // } catch (DecryptException $e) {
    //     //
    // }
    
    if($validator->fails()){
        return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),200);
    }else{
        $user = Pelanggan::loginAccount($this->params);
      return $user;
    }
  }

  public function logoutAccount(){
    $validator = Validator::make(json_decode($this->request,true),[
        'id' =>'required',
        'tipe_user' =>'required'
  ]);
  
  if($validator->fails()){
      return  showOutputResponse($this->params->Func,false,'Parameter required123',$validator->errors(),200);
  }else{
      $user = Pelanggan::logoutAccount($this->params);
    return $user;
  }
}



  public function registerAccount(){
        $validator = Validator::make(json_decode($this->request,true),[
            'username' =>'required',
            'password' =>'required',
            'nama' =>'required',
            'alamat' =>'required',
            'hp' =>'required',
            'email' =>'required',
            'imei' =>'required',
            'status_live' =>'required',
      ]);

    if($validator->fails()){
        return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),200);
    }else{
      $user = Pelanggan::registerAccount($this->params,Carbon::now());
      return $user;
    }
  }
    
    public function AktivasiAkun(){
      $validator = Validator::make(json_decode($this->request,true),[
          'token' =>'required|digits:6',
          'noref' =>'required',
          'id_pelanggan' =>'required',
      ]);
      if($validator->fails()){
        return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),200);
      }else{
        $response = Pelanggan::AktivasiAkun($this->params);
        return $response;
      }
      
    }

    public function insertFirebaseID(){
      $validator = Validator::make(json_decode($this->request,true),[
          'imei' =>'required',
          'tipe_user' =>'required',
          'gcm_code' =>'required',
      ]);
      if($validator->fails()){
        return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),200);
      }else{
        GeneralModel::handleGCMCode($this->params,0);
        return showOutputResponse($this->params->Func,true,'Manag Berhasil','-',200); 
      }
    }

    public function saveFirebasePublic(){
         $this->prm = (object)array(
            'imei'=>$this->request->input('kode_imei'),
            'tipe_user'=>$this->request->input('tipe_user'),
            'gcm_code'=>$this->request->input('gcm_code'),
          );
          GeneralModel::insertLogRequest("saveFirebasePublic",'IN',json_encode($this->prm),'UserPublicController');

          GeneralModel::handleGCMCode($this->prm,0);
          return showOutputResponse("saveFirebasePublic",true,'Data GCM berhasil di simpan','-',200); 
    }


    public function updatePassword(){
      $validator = Validator::make(json_decode($this->request,true),[
          'token' =>'required|digits:6',
          'noref' =>'required',
          'password' =>'required',
          'tipe_user' =>'required',
          'id_user' =>'required',
      ]);
      if($validator->fails()){
        return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),200);
      }else{
        $response = Pelanggan::updatePassword($this->params);
        return $response;
      }
    }
    
    public function updatePasswordInSession(){
      $validator = Validator::make(json_decode($this->request,true),[
          'password' =>'required',
          'tipe_user' =>'required',
          'id' =>'required',
      ]);
      if($validator->fails()){
        return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),200);
      }else{
        $response = Pelanggan::updatePassword($this->params);
        return $response;
      }
    }

    public function requestResetPassword(){
      $validator = Validator::make(json_decode($this->request,true),[
          'username' =>'required',
          'tipe_user' =>'required',
      ]);
      if($validator->fails()){
        return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),200);
      }else{
        $response = Pelanggan::requestResetPassword($this->params);
        return $response;
      }
    }
    
    
    public function getDataSplashScreen(){
      $validator = Validator::make(json_decode($this->request,true),[
          'id_user' =>'required',
          'tipe_user' =>'required',
      ]);
      if($validator->fails()){
        return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),200);
      }else{
        $response = GeneralModel::getDataSplashScreen($this->params);
        return   showOutputResponse($this->params->Func,true,'Data ditemukan',$response,200);
      }
    }
    
    public function getNewToken(){
      $validator = Validator::make(json_decode($this->request,true),[
          'id_user' =>'required',
          'tipe_user' =>'required',
      ]);
      if($validator->fails()){
        return  showOutputResponse($this->params->Func,false,'Parameter required',$validator->errors(),200);
      }else{
        return Pelanggan::getNewToken($this->params);
      }
    }
    
    public function updateLocation(){
      GeneralModel::updateLocation($this->params);
      return   showOutputResponse($this->params->Func,true,'Berhasil update lokasi',array(),200);
    }

    public function sendNotification(){
      $dataNotif= (object)array(
        'title'=>$this->request->input('judul'),
        'message'=>$this->request->input('pesan'),
        'image'=>'-',
        'data'=>array('team'=>'Indonesia') );
      $res = sendPushNotification($this->request->input('gcm_code'),$dataNotif);
    }
 

    public function testData(){

      echo base64_encode('943a0bb1-1118-43be-842e-2e651526db9b:09ef4e14-52ee-45f7-84df-5778b7ee5127');


    }

    public function tesFile(){
      if ($this->request->hasFile('image')) {

        $image = $this->request->file('image');
        $data = base64_encode($image);

        file_put_contents(config('app.url_foto_profile').rand(1000,9999).'.png', base64_decode($image));

        // base64_decode($image);

        $original_filename = $image->getClientOriginalName();
        // echo $original_filename;
      }
    }


    

}
