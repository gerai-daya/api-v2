<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class BcaModel extends Model
{
    public static function getDataTransactionVa($param, $status)
    {
        $response = DB::table('tb_transaction_va')
            ->where('company_code', $param->CompanyCode)
            ->where('cust_number', $param->CustomerNumber)
            ->whereIn('status', $status);
        return  $response;
    }

    public static function insertDataTransactionVa($param)
    {
        $data = DB::table('tb_transaction_va')
            ->where('company_code', $param->company_code)
            ->where('cust_number', $param->cust_number)
            ->where('status', 'OPEN')
            ->whereDate('date_request', Carbon::now()->toDateString());
        if (!$data->exists()) {
            try {
                $data = DB::table('tb_transaction_va')->insert([
                    'company_code' => $param->company_code,
                    'cust_number' => $param->cust_number,
                    'cust_name' => $param->cust_name,
                    'request_id' => "",
                    'channel_type' => $param->channel_type,
                    'date_request' => Carbon::now()->format('Y-m-d H:i:s'),
                    'date_expired' => Carbon::now()->addHours(12),
                    'description' => $param->desc,
                    'amount' => $param->amount,
                    'status' => 'OPEN',
                    'created_at' => Carbon::now(),
                ]);
                return showOutputResponseBCA('GenerateNumberVA', 'OUT', '', 'ApiBCA', 200, false, 'Generate VA Number Success', 'Pembuatan Nomor VA Berhasil');
            } catch (\Exception $e) {
                return showOutputResponseBCA('GenerateNumberVA', 'OUT', '', 'ApiBCA', 404, false, 'Generate VA Number Failed', 'Pembuatan Nomor VA Gagal');
            }
        } else {
            return showOutputResponseBCA('GenerateNumberVA', 'OUT', '', 'ApiBCA', 404, false, 'VA Number is Already Exist', 'Nomor VA Sudah Terdaftar');
        }
    }

    public static function updateDataTransactionVa($id_va, $data)
    {
        try {
            $data = DB::table('tb_transaction_va')->where('id_va', $id_va)->update($data);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function insertDataTransactionVaLog($param)
    {

        $data = DB::table('tb_transaction_va_log')->insert($param);
        /*if(!DB::table('tb_transaction_va_log')->where('id_va',$param['id_va'])->exists()){
            
        } */
        return  true;
    }

    public static function insertDataTransactionBank($param)
    {
        try {
            $data = DB::table('tb_transaction_bank')->insert(array_merge($param, array('created_at' => Carbon::now())));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function updateDataTransactionBank($id, $param)
    {
        try {
            $data = DB::table('tb_transaction_bank')->where('id', $id)->update($param);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getTransactionBank($param)
    {
        // var_dump($param);
        // exit;
        $data = DB::table('tb_transaction_bank')->where($param);
        if ($data->exists()) {
            return  $data->first();
        } else {
            return false;
        }
    }
}
