<?php

namespace App\Models;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Support\Facades\DB;
use App\Models\GeneralModel;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\DepositModel;
use App\Models\TokenRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;


class Booking extends Model implements Authenticatable
{
    use AuthenticableTrait;



    public static function getOnprogressPetugas($idPetugas){

        $data = DB::select(" SELECT a.nama,
            case
                when status_order='DIKERJAKAN' then
                    IFNULL(DATE_FORMAT(`tgl_dikerjakan`,'%d-%m-%Y %T'),'-')
                    when status_order='SELESAI' then
                    IFNULL(DATE_FORMAT(`tgl_selesai`,'%d-%m-%Y %T'),'-')
            else
            DATE_FORMAT(`tgl_pesan`,'%d-%m-%Y %T')
            end as tgl_pengerjaan,
            DATE_FORMAT(`tgl_pesan`,'%d-%m-%Y %T') as tgl_pesan,
            status_order,
            CONCAT('Rp. ',FORMAT((IFNULL(tagihan,0)+IFNULL(biaya_survey,0) )-IFNULL(diskon,0) ,0,'de_DE')) as tagihan,
            concat( ifnull(jml_rating,0),'/5') as jml_rating,
            ifnull(masukan,'') as masukan,
            biaya_survey,
            b.alamat,
            b.kode_booking,
            id_booking,a.id_pelanggan,b.id_petugas,
            (select deskripsi from  tb_master_data xx where xx.value=b.status_order limit 1 ) as status_huruf
            FROM tb_pelanggan a,tb_booking b,tb_petugas c
            WHERE
            a.`id_pelanggan`=b.`id_pelanggan` and c.id_petugas=b.id_petugas
            AND b.id_petugas=".$idPetugas."
            AND status_order not IN ('SELESAI','CLOSE','CANCEL','DITOLAK','KONFIRMASI_SURVEY_DITOLAK')
        ");
        return $data;
    } 

    public static function getOnprogressPelanggan($idPelanggan){

        $data = DB::select(" SELECT
                        ifnull(c.nama,'[Belum Tersedia]') as nama,
                        case
                                when status_order='DIKERJAKAN' then
                                    IFNULL(DATE_FORMAT(`tgl_dikerjakan`,'%d-%m-%Y %T'),'-')
                                    when status_order='SELESAI' then
                                    IFNULL(DATE_FORMAT(`tgl_selesai`,'%d-%m-%Y %T'),'-')
                            else
                            DATE_FORMAT(`tgl_pesan`,'%d-%m-%Y %T')
                            end as tgl_pengerjaan,
                        status_order,
                        ifnull((select DATE_FORMAT(`create_at`,'%d-%m-%Y %H:%i') from tb_booking_logstatus where id_booking=b.id_booking and status='BERANGKAT_KE_LOKASI'), DATE_FORMAT(`tgl_pesan`,'%d-%m-%Y %H:%i') ) as tgl_pesan,
                        ifnull((select DATE_FORMAT(`create_at`,'%d-%m-%Y %H:%i') from tb_booking_logstatus where id_booking=b.id_booking and status='TIBA_DI_LOKASI'),DATE_FORMAT(`tgl_dikerjakan`,'%d-%m-%Y %H:%i')) as tgl_pengerjaan_,
                        IFNULL(DATE_FORMAT(`tgl_selesai`,'%d-%m-%Y %H:%i'),'-') as tgl_selesai,
                        CONCAT('Rp. ',FORMAT((IFNULL(tagihan,0)+IFNULL(biaya_survey,0)+IFNULL(biaya_pembatalan,0) )-IFNULL(diskon,0) ,0,'de_DE')) as tagihan,
                        biaya_survey,
                        b.alamat,
                        concat( ifnull(jml_rating,0),'/5') as jml_rating,
                        ifnull(masukan,'') as masukan,
                        b.kode_booking,
                        id_booking,a.id_pelanggan,b.id_petugas,
                        (select deskripsi from  tb_master_data xx where xx.value=b.status_order limit 1 ) as status_huruf
                        FROM tb_pelanggan a,tb_booking b LEFT JOIN tb_petugas c ON c.id_petugas=b.id_petugas
                        WHERE
                        a.`id_pelanggan`=b.`id_pelanggan` 
                        AND b.id_pelanggan=".$idPelanggan."
                        order by id_booking desc limit 5
        ");
        return $data;
    } 
    
    

    
    public static function getLastWorkPetugas($idPetugas){

        $data = DB::select("SELECT
        a.nama,
            case
            when status_order='DIKERJAKAN' then
                IFNULL(DATE_FORMAT(`tgl_dikerjakan`,'%d-%m-%Y %T'),'-')
                when status_order='SELESAI' then
                IFNULL(DATE_FORMAT(`tgl_selesai`,'%d-%m-%Y %T'),'-')
        else
        DATE_FORMAT(`tgl_pesan`,'%d-%m-%Y %T')
        end as tgl_pengerjaan1,
        ifnull(DATE_FORMAT(`tgl_dikerjakan`,'%d-%m-%Y %T'),'- -') as tgl_pengerjaan,
        ifnull(DATE_FORMAT(`tgl_pesan`,'%d-%m-%Y %T'),'- -') as tgl_pesan,
        ifnull(DATE_FORMAT(`tgl_selesai`,'%d-%m-%Y %T'),'- -') as tgl_selesai,
        status_order,
        ifnull(aksi_cancel,'-') as aksi_cancel,
        CONCAT('Rp. ',FORMAT((IFNULL(tagihan,0)+IFNULL(biaya_survey,0) )-IFNULL(diskon,0) ,0,'de_DE')) as tagihan,
        concat( ifnull(jml_rating,0),'/5') as jml_rating,
        ifnull(masukan,'') as masukan,
        biaya_survey,
        b.alamat,
        b.kode_booking,
        id_booking,a.id_pelanggan,b.id_petugas,
        (select deskripsi from  tb_master_data xx where xx.value=b.status_order limit 1 ) as status_huruf
        FROM tb_pelanggan a,tb_booking b,tb_petugas c
        WHERE
        a.`id_pelanggan`=b.`id_pelanggan` and c.id_petugas=b.id_petugas
        AND b.id_petugas=".$idPetugas."
        AND status_order  IN ('SELESAI','CLOSE','CANCEL','DITOLAK','KONFIRMASI_SURVEY_DITOLAK')
        order by id_booking desc limit 5
        ");
        return $data;
    }
    
    public static function getHomePelanggan($idPetugas){
        $dataPetugas = DB::select(" SELECT
                        (fnGetSaldoAkhir (a.id_perusahaan,'".date('Y-m-d')."','DEPOSIT')) saldo_deposit,
                        ifnull((select count(id) from tb_konfirmasi_deposit where id_perusahaan = a.id_perusahaan and status in ('pending','review')),0) as jml_deposit_review,
							FORMAT(ifnull((select count(id_booking) from tb_booking where id_petugas=a.id_petugas and status_order IN ('CLOSE','SELESAI','CANCEL')),0),0,'de_DE') as total_order,
							FORMAT(ifnull((select count(distinct(id_pelanggan)) from tb_booking where id_petugas=a.id_petugas and status_order IN ('CLOSE','SELESAI','CANCEL') ),0),0,'de_DE') as total_pelanggan,
							FORMAT(ifnull((select concat(round(avg(jml_rating),1),' (', count(id_booking),')') from tb_booking where id_petugas=a.id_petugas and status_order IN ('CLOSE','SELESAI','CANCEL') limit 1),0),0,'de_DE') as total_rating,
                            ifnull((   select 
                                    case when jenis_bentuk_usaha='PERUSAHAAN' then FORMAT(ifnull(fee_petugas,0),0,'de_DE')
                                    else FORMAT(ifnull((fee_petugas+fee_material),0),0,'de_DE') 
                                    end  as  total_income
                                from (
                                    SELECT 
                                        round((sum(fee_petugas)),2)  as fee_petugas,
                                        sum(fee_material) as fee_material,
                                        (select jenis_bentuk_usaha from tb_perusahaan z where z.id_perusahaan=b.id_perusahaan ) jenis_bentuk_usaha
                                    FROM tb_booking_fee aa, tb_booking b where  aa.id_booking=b.id_booking and id_petugas=".$idPetugas."
                                    group by id_perusahaan
                                )xx
                                ),0) AS total_income,

                             ifnull( (select value from jenis_usaha where id_usaha in (select id_usaha from  tb_perusahaan_detail b where b.id_perusahaan=a.id_perusahaan) ),'LISTRIK') as jenis_layanan
                    FROM  `tb_petugas` a  where id_petugas=".$idPetugas."
                ");
        return $dataPetugas;
    } 
    
    public static function getLayananPetugas($idPetugas,$kodeLayanan){

            $jenis_utama_ = explode('_',$kodeLayanan);
            $jenis_utama = $jenis_utama_[0];
            if(count($jenis_utama_)>1){
                $addquery=" and kode_layanan='".$kodeLayanan."' ";
            }else{
                if($jenis_utama_[0]=="AC"){
                    $addquery=" and SUBSTRING(kode_layanan,1,2)='".$jenis_utama."' ";
                }else if($jenis_utama_[0]=="LISTRIK"){
                    $addquery=" and SUBSTRING(kode_layanan,1,7)='".$jenis_utama."' ";
                }else{
                    $addquery=" and kode_layanan='".$jenis_utama."' ";
                }
                
            }
            
            $query=" SELECT xx.*,0 as fee_pln,0 as fee_vendor from (
                        SELECT
                            nama_jasa as value,
                            harga_jasa as harga_pelanggan,
                            id_harga_jasa as id_pekerjaan_detail,
                            a.id_jasa as id_master_data,
                            'JASA' as jenis,ifnull(ket,'') as ket
                            from
                            tb_master_harga_jasa a, tb_master_jasa b,tb_master_layanan c,tb_master_zona d
                                where a.id_jasa=b.id_jasa and b.id_layanan=c.id and a.id_zona=d.id
                                ".$addquery."
                                AND id_zona in (
                                    select id_zona from tb_perusahaan a,tb_petugas b
                                    where a.id_perusahaan=b.id_perusahaan
                                    and id_petugas=".$idPetugas."
                            )
                        UNION
                        SELECT
                            nama_material as value,
                            harga_material as harga_pelanggan,
                            id_harga_material as id_pekerjaan_detail,
                            a.id_material as id_master_data,
                            'MATERIAL' as jenis,ifnull(ket,'') as ket
                            from
                            tb_master_harga_material a, tb_master_material b,tb_master_zona c, jenis_usaha d
                            where a.id_material=b.id_material and a.id_zona=c.id and b.id_usaha=d.id_usaha and value='".$jenis_utama."'
                            AND id_zona in (
                                select id_zona from tb_perusahaan a,tb_petugas b
                                where a.id_perusahaan=b.id_perusahaan
                                and id_petugas=".$idPetugas."
                            )

                )xx order by jenis asc ";
                $dataLayananPetugas = DB::select($query);
        return $dataLayananPetugas;
    } 
    
    
    public static function getHistoriOrder($param){
        if($param->tipe_user=='PETUGAS'){
            $addQuery=" and b.id_petugas=".$param->id_petugas;
        }else{
            $addQuery=" and b.id_pelanggan=".$param->id_pelanggan;
        }
        $data = DB::select("SELECT
                a.nama,
                ifnull(DATE_FORMAT(`tgl_dikerjakan`,'%d-%m-%Y %T'),'- -') as tgl_dikerjakan,
                ifnull(DATE_FORMAT(`tgl_pesan`,'%d-%m-%Y %T'),'- -') as tgl_pesan,
                ifnull(DATE_FORMAT(`tgl_selesai`,'%d-%m-%Y %T'),'- -') as tgl_selesai,
                status_order,
                ifnull(aksi_cancel,'-') as aksi_cancel,
                CONCAT('Rp. ',FORMAT((IFNULL(tagihan,0)+IFNULL(biaya_survey,0) )-IFNULL(diskon,0) ,0,'de_DE')) as tagihan,
                concat( ifnull(jml_rating,0),'/5') as jml_rating,
                ifnull(masukan,'') as masukan,
                biaya_survey,
                b.alamat,
                b.kode_booking,
                id_booking,a.id_pelanggan,b.id_petugas,
                (select deskripsi from  tb_master_data xx where xx.value=b.status_order limit 1 ) as status_huruf
                FROM tb_pelanggan a,tb_booking b,tb_petugas c
                WHERE
                a.`id_pelanggan`=b.`id_pelanggan` and c.id_petugas=b.id_petugas
                ".$addQuery."
                AND status_order  IN ('SELESAI','CLOSE','CANCEL','DITOLAK','KONFIRMASI_SURVEY_DITOLAK')
                and year(tgl_selesai)=".$param->tahun." and month(tgl_selesai)=".$param->bulan."
                order by id_booking desc ");
        return $data;
    } 
    
    public static function getHistoriPendapatan($param){
        $data = DB::table('tb_booking_fee_petugas')
        ->select(DB::raw("keterangan, id_booking,FORMAT(nominal,'de_DE',2) as nominal,DATE_FORMAT(create_at,'%d %M %Y %h:%i') as tgl,FORMAT(saldo,'de_DE',2) as saldo,case when debet>0 then 'DB' else 'CR' end as tipe_transaksi"))
        ->whereYear('create_at',$param->tahun)
        ->whereMonth('create_at',$param->bulan)
        ->where('id_petugas',$param->id_petugas)
        ->get();
        return $data;
    } 
    
    public static function getDataBooking($param){
        $data = DB::select(" SELECT 
                a.nama,IFNULL( concat('+62', SUBSTR(a.hp,2,14)),'') AS hp,IFNULL( a. gcm_code,'') AS firebase_id_plg,a.username as username_plg,IFNULL( concat('+62', SUBSTR(c.hp,2,14)),'') AS hp_ptg,IFNULL( c. gcm_code,'') AS firebase_id_ptg,
                IFNULL(c.username,'') as username_ptg,IFNULL(c.nama,'') as nama_ptg,IFNULL(c.id_petugas,'0') AS id_petugas,IFNULL(d.nama,'') AS nama_perusahaan,IFNULL(d.alamat,'') AS alamat_perusahaan,IFNULL(d.biaya_pembatalan,'0') AS biaya_pembatalan,lat_pel,longi_pel,
                IFNULL(DATE_FORMAT(`tgl_pesan`,'%d-%m-%Y %H:%i'),'-') as tgl_pesan,
                IFNULL(DATE_FORMAT(`tgl_dikerjakan`,'%d-%m-%Y %H:%i'),'-') as tgl_dikerjakan,
                IFNULL(DATE_FORMAT(`tgl_selesai`,'%d-%m-%Y %H:%i'),'-') as tgl_selesai,
                IFNULL(DATE_FORMAT(`tgl_rencana_dikerjakan`,'%d-%m-%Y %H:%i'),'-') as tgl_rencana_dikerjakan,

                status_order,
                ifnull(aksi_cancel,'') as aksi_cancel,
                CONCAT('Rp. ',FORMAT(IFNULL(tagihan,0) ,0,'de_DE')) as tagihan,
                IFNULL(diskon,0) as diskon,
                b.biaya_survey,
                CONCAT('Rp. ',FORMAT(IFNULL( (tagihan+b.biaya_survey)-diskon,0) ,0,'de_DE')) as grandtotal,
                b.alamat,petunjuk,keluhan,ifnull(solusi,'') as solusi,ifnull(concat('".config('app.url_booking')."',foto_sebelum),'-') as foto_sebelum,ifnull(concat('".config('app.url_booking')."',foto_sesudah),'-') as foto_sesudah,ifnull(alasan_penolakan,'') as alasan_penolakan,
                b.kode_booking,metode_pembayaran as cara_bayar,
                id_booking,a.id_pelanggan,
                kategori_pekerjaan,kode_layanan,
                jml_rating,ifnull(masukan,'') as masukan,IFNULL(DATE_FORMAT(`tgl_rating`,'%d-%m-%Y %T'),'-') as tgl_rating,
                (select deskripsi from  tb_master_data xx where xx.value=b.status_order limit 1 ) as status_huruf,
                case when date_format(now(),'%Y-%m-%d')< date_format(date_add(tgl_selesai,INTERVAL 5 DAY),'%Y-%m-%d') then 'true' else 'false' end as is_masa_garansi
                FROM tb_pelanggan a,tb_booking b 
                LEFT JOIN tb_petugas c on c.id_petugas=b.id_petugas
                LEFT JOIN tb_perusahaan d on d.id_perusahaan=b.id_perusahaan
                WHERE
                a.`id_pelanggan`=b.`id_pelanggan` AND b.id_booking =".$param->id_booking);
        return $data;
    }
    
    public static function getSummaryPelanggan($id_pelanggan){
        $data = DB::table('tb_booking')
        ->select(DB::raw("
            ifnull(count(id_booking),0) as total,
            ifnull( (select count(id_booking) from tb_booking x where id_pelanggan=tb_booking.id_pelanggan and  status_order in ('SELESAI','DITOLAK','CLOSE','CANCEL') ),0) as selesai,
            ifnull( (select count(id_booking) from tb_booking x where id_pelanggan=tb_booking.id_pelanggan and  status_order not in ('SELESAI','DITOLAK','CLOSE','CANCEL') ),0) as progress
            "))
        ->where('id_pelanggan',$id_pelanggan)
        ->limit(1)
        ->get();
        if($data->count()<=0){
            $data = array(array("total"=>0,"selesai"=>0,"progress"=>0));
        }
        return $data;
    }

    public static function getDataBookingDetail($param){
        $data = DB::select("
            SELECT xx.* from (
                    SELECT
                    id_booking,a.id_booking_detail, a.id_pekerjaan_detail,a.harga as harga_pelanggan,nama_jasa as value,qty,'JASA' as jenis
                    FROM tb_booking_detail a, tb_master_harga_jasa b , tb_master_jasa c
                    where id_pekerjaan_detail = b.id_harga_jasa and b.id_jasa=c.id_jasa
                    and jenis='JASA'   and  id_booking='".$param->id_booking."'
                    UNION

                    SELECT
                    id_booking,a.id_booking_detail, a.id_pekerjaan_detail,a.harga as harga_pelanggan,nama_material as value,qty,'MATERIAL' as jenis
                    FROM tb_booking_detail a, tb_master_harga_material b , tb_master_material c
                    where id_pekerjaan_detail = b.id_harga_material and b.id_material=c.id_material
                    and jenis='MATERIAL' and  id_booking='".$param->id_booking."'
                    UNION
                    SELECT
                    id_booking,a.id_booking_detail, a.id_pekerjaan_detail,a.harga as harga_pelanggan,keterangan as value,qty,jenis
                    FROM tb_booking_detail a
                    WHERE  jenis NOT IN ('MATERIAL','JASA') and  id_booking='".$param->id_booking."'
            )xx  order by jenis asc
        ");
        return $data;
    }
    
    public static function getListBookingDetail($idBooking){
        $data = DB::select("SELECT xx.* from (
                    SELECT
                    id_booking,a.id_booking_detail, a.id_pekerjaan_detail,a.harga as harga_pelanggan,nama_jasa as value,qty,'JASA' as jenis
                    FROM tb_booking_detail a, tb_master_harga_jasa b , tb_master_jasa c
                    where id_pekerjaan_detail = b.id_harga_jasa and b.id_jasa=c.id_jasa
                    and jenis='JASA'   and  id_booking=".$idBooking."
                    UNION

                    SELECT
                    id_booking,a.id_booking_detail, a.id_pekerjaan_detail,a.harga as harga_pelanggan,nama_material as value,qty,'MATERIAL' as jenis
                    FROM tb_booking_detail a, tb_master_harga_material b , tb_master_material c
                    where id_pekerjaan_detail = b.id_harga_material and b.id_material=c.id_material
                    and jenis='MATERIAL' and  id_booking=".$idBooking."
                    UNION
                    SELECT
                    id_booking,a.id_booking_detail, a.id_pekerjaan_detail,a.harga as harga_pelanggan,keterangan as value,qty,jenis
                    FROM tb_booking_detail a
                    WHERE  jenis NOT IN ('MATERIAL','JASA') and  id_booking=".$idBooking."
                )xx  order by jenis asc");
        return $data;
    }

    public static function getDataBookingStatus($param){
        $data = DB::table('tb_booking_logstatus')
            ->where('id_booking',$param->id_booking)
            ->select(DB::raw("status,DATE_FORMAT(`create_at`,'%d-%m-%Y %H:%i:%s') as create_at"))
            ->get();
            return $data;
    } 

    public static function getDataDetailBooking($idBooking){
        $data = DB::select("SELECT 
                    *,
                    ifnull((select gcm_code from tb_petugas where id_petugas=a.id_petugas),'') as gcm_code_petugas,
                    ifnull((select gcm_code from tb_pelanggan where id_pelanggan=a.id_pelanggan),'') as gcm_code_pelanggan,
                    (b.geraidaya/100 )as persen_fee_gd,
                    (b.mitra/100) as persen_fee_mitra,
                    (b.petugas/100) as persen_fee_petugas,
                    ifnull( (select sum(kredit-debet) from tb_booking_fee_petugas where id_petugas= a.id_petugas),0) as saldo_petugas,
                    (select ifnull(sum(qty*harga),0) from tb_booking_detail where id_booking=a.id_booking and jenis in ('TAMBAHAN JASA','JASA')) AS nominal_jasa,
                    (select ifnull(sum(qty*harga),0)  from tb_booking_detail where id_booking=a.id_booking and jenis IN ('MATERIAL','TAMBAHAN MATERIAL')) AS nominal_material,
                    (select jenis_bentuk_usaha from tb_perusahaan where id_perusahaan=a.id_perusahaan ) jenis_bentuk_usaha,
                    (select biaya_pembatalan from jenis_usaha where value=a.kategori_pekerjaan ) biaya_pembatalan,
                    (select value from tb_master_data where kategori='STATUS_PKP') status_pkp_gd,
                    (select status_pkp from tb_perusahaan where id_perusahaan=a.id_perusahaan ) status_pkp_perusahaan
                    from tb_booking a, tb_master_persentase b where a.id_persentase = b.id and  id_booking =".$idBooking);
        return $data;
    } 
    
    public static function getDataBookingById($idBooking){
        $data = DB::table('tb_booking')->where('id_booking',$idBooking);
        return $data;
    } 
    
    
    
    public static function updatePekerjaanBooking($param){
        $data_update = array();
        $StatusPetugas = "FREE";
        $data_lokasi  = array('lat_petugas'=>$param->lat_petugas,'longi_petugas'=>$param->longi_petugas);
        switch($param->request){
            case 'DITOLAK':
            case 'CANCEL':
            case 'KOMPLAIN':
                if($param->request=='CANCEL' && strlen($param->aksi_cancel)>0 && $param->aksi_cancel!="0" ){
                    $data_update = array(
                            'tgl_selesai'=>Carbon::now(),
                            'aksi_cancel'=>$param->aksi_cancel);
                }else{
                    if($param->request=="CANCEL"){
                        $data_update = array(
                            'tgl_selesai'=>Carbon::now(),
                            'status_order'=>$param->request,
                            'tagihan'=>0,
                            'diskon'=>0,
                            'alasan_penolakan'=>$param->alasan_penolakan,
                            'biaya_pembatalan'=>$param->tipe_user=='PETUGAS'?0:$param->biaya_pembatalan);
                            DB::table("tb_booking_detail")->where("id_booking",$param->id_booking)->delete();
                    }else{
                        $data_update = array(
                            'tgl_selesai'=>Carbon::now(),
                            'status_order'=>$param->request,
                            'alasan_penolakan'=>$param->alasan_penolakan,
                            'biaya_pembatalan'=>$param->tipe_user=='PETUGAS'?0:$param->biaya_pembatalan);
                    }
                }
                break;
            case 'TIBA_DI_LOKASI':
                $data_update = array(
                    'masukan'=>'',
                    'status_order'=>'START_SURVEY');
                    $StatusPetugas = "WORK";
                break;
            case 'DITERIMA':
            case 'KONFIRMASI_SURVEY':
            case 'BERANGKAT_KE_LOKASI':
            case 'START_SURVEY':
            case 'SURVEY_SELESAI':
            case 'KOMPLAIN_DITERIMA':
                $data_update = array(
                    'masukan'=>'',
                    'solusi'=>'',
                    'status_order'=>$param->request
                );

                if($param->request=="KOMPLAIN_DITERIMA"){
                    $data_update = array(
                        'masukan'=>'',
                        'solusi'=>$param->alasan_penolakan,
                        'status_order'=>$param->request
                    );
                }else if($param->request=="SURVEY_SELESAI"){
                    $dtBook = self::getDataBookingById($param->id_booking)->first();
                    $data_update = array(
                        'keluhan'=>$dtBook->keluhan.' # Hasil Survey:'.$param->alasan_penolakan,
                        'status_order'=>$param->request
                    );
                }
                $StatusPetugas = "WORK";
                break;
            case 'DIKERJAKAN':
                $data_update = array(
                    'masukan'=>'',
                    'solusi'=>'',
                    'status_order'=>$param->request,
                    'diskon'=>$param->diskon,
                    'tgl_dikerjakan'=>Carbon::now()->format('Y-m-d'),
                );
                $StatusPetugas = "WORK";
                break;
            case 'INPUT_SOLUSI':
                $data_update = array('solusi'=>$param->solusi);
                break;
            case 'SELESAI':
                $data_update = array(
                    'masukan'=>'',
                    'solusi'=>'',
                    'status_order'=>$param->request,
                    'diskon'=>$param->diskon,
                    'tgl_selesai'=>Carbon::now()->format('Y-m-d'),
                );
                break;
        }

        // $data_update = array(
        //     'masukan'=>'',
        //     'solusi'=>'',
        //     'status_order'=>'KONFIRMASI_SURVEY_DITERIMA'
        // );
         
        
        try {

            $updateBooking = DB::table('tb_booking')
            ->where('id_booking',$param->id_booking)
            ->update(array_merge($data_update,$data_lokasi));
            $dataBooking = self::getDataDetailBooking($param->id_booking);
            
            
            $valueBooking = $dataBooking[0];
            $isSudahPotongDeposit = DB::table('tb_deposit')->where('id_booking',$param->id_booking)->doesntExist();
            if($isSudahPotongDeposit==1 && $param->request=='SELESAI'){
                self::prosesPotongDeposit($valueBooking);
            }
            
            self::prosesPotongDepositJikaBatal($valueBooking,$param);
            DB::table('tb_petugas')->where('id_petugas',$valueBooking->id_petugas)->update(['status_kerja'=>$StatusPetugas]);

            $gcm_code_pelanggan = $valueBooking->gcm_code_pelanggan;
            if(strlen($gcm_code_pelanggan)<10){
                $resGcmPelanggan=DB::table('tb_gcm_user')->where('id_relasi',$param->id_pelanggan)->where('tipe_user','PELANGGAN');
                if($resGcmPelanggan->exists()){
                    $gcm_code_pelanggan = $resGcmPelanggan->first()->gcm_code;
                }

            }

            $request=$param->request;

            $resMasterData=DB::table('tb_master_data')->where('subkategori',$param->request)->where('kategori','PESAN_STATUS_BOOKING_PELANGGAN')->first();
            if(strlen($gcm_code_pelanggan)>0){
                if( $param->request=="CANCEL" && strlen($param->aksi_cancel)>=10){
                    $request=$param->aksi_cancel;
                }
            }

            $pesan="Update informasi ".$valueBooking->kode_booking." [".str_replace('_',' ',$request)."]\n".$resMasterData->value.".\n\n".$resMasterData->deskripsi;
            $dataNotif= (object)array(
                'title'=>"INFO ORDER ".str_replace('_',' ',$request),
                'message'=>$pesan,
                'image'=>'-',
                'data'=>array('team'=>'Indonesia') );
            $res = sendPushNotification($gcm_code_pelanggan,$dataNotif);
                

            $gcm_code_petugas = $valueBooking->gcm_code_petugas;
            if(strlen($gcm_code_petugas)<10){
                $resGcmPetugas=DB::table('tb_gcm_user')->where('id_relasi',$param->id_petugas)->where('tipe_user','PETUGAS');
                if($resGcmPetugas->exists()){
                    $gcm_code_petugas = $resGcmPetugas->first()->gcm_code;
                }
            }

            if(strlen($gcm_code_petugas)>0){
                if(strlen($param->aksi_cancel)>=2){
                    $pesan="Update informasi ".$valueBooking->kode_booking." [".str_replace('_',' ',$request)."]\nStatus pekerjaan dikomplain oleh Pelanggan, Mohon maaf atas ketidaknyaman. Silakan Anda menanggapi komplain dari Pelanggan";
                    $dataNotif= (object)array(
                        'title'=>"INFO ORDER ".str_replace('_',' ',$request),
                        'message'=>$pesan,
                        'image'=>'-',
                        'data'=>array('team'=>'Indonesia') );
                    $res = sendPushNotification($gcm_code_petugas,$dataNotif);
                }else{
                    
                    if($request=="CANCEL" || $request=="KOMPLAIN"){
                        $resMasterData=DB::table('tb_master_data')->where('subkategori',$param->request)->where('kategori','PESAN_STATUS_BOOKING_PETUGAS')->first();
                        $pesan="Update informasi ".$valueBooking->kode_booking." [".str_replace('_',' ',$request)."]\n".$resMasterData->value.".\n\n".$resMasterData->deskripsi;

                        $dataNotif= (object)array(
                            'title'=>"INFO ORDER ".str_replace('_',' ',$request),
                            'message'=>$pesan,
                            'image'=>'-',
                            'data'=>array('team'=>'Indonesia') );
                        $res = sendPushNotification($gcm_code_petugas,$dataNotif);

                    }
                    
                }
                   
            }

            if($param->request!="KOMPLAIN_DITERIMA"){
                $status = $param->request=='TIBA_DI_LOKASI'?'START_SURVEY':$param->request;
                $deskripsi_ = $resMasterData->deskripsi;
                if($param->request=="CANCEL" && strlen($param->aksi_cancel)>2){
                    $status = $param->aksi_cancel;
                    $deskripsi_ = "Aksi dari perubahan status CANCEL ke ".$param->aksi_cancel;
                }
                $deskripsi_ = $param->request=='TIBA_DI_LOKASI'?'Saat ini petugas telah tiba di lokasi Anda':$deskripsi_;

                $isDataExist=DB::table('tb_booking_logstatus')->where('id_booking',$param->id_booking)->where('status',$status)->exists();
                if(!$isDataExist){
                    DB::table('tb_booking_logstatus')->insert([
                        'id_booking'=>$param->id_booking,
                        'status'=>$status,
                        'description'=>$deskripsi_,
                        'create_at'=>Carbon::now()
                    ]);
                }
                
            }

            $dataOnprogress = Booking::getOnprogressPetugas($param->id_petugas);
            $dataLastWork = Booking::getLastWorkPetugas($param->id_petugas);

            $dataLayananPetugas = array();
            $datadummy = array();
            foreach ($dataOnprogress as $i => $rows) {
                $prm = (object)array('id_booking'=>$rows->id_booking);
                $dataBooking = self::getDataBooking($prm);
                $dataBookingDetail = self::getDataBookingDetail($prm);
                $dataBookingStatus = self::getDataBookingStatus($prm);
                if(in_array($dataBooking[0]->status_order,array("SURVEY_SELESAI","DITERIMA","DIKERJAKAN") )==1){
                    $dataLayananPetugas = self::getLayananPetugas($dataBooking[0]->id_petugas,$dataBooking[0]->kode_layanan);
                }

                $dataOnprogress[$i]->data = array('rows_status'=>$dataBookingStatus,'rows_utama'=>$dataBooking,'rows_detail'=>$dataBookingDetail,'rows_pekerjaan_petugas'=>$dataLayananPetugas);
            }


            $result = showOutputResponse($param->Func,true,'Proses Perubahan Status Berhasil',array('rows_progress'=>$dataOnprogress,'rows_aktivitas'=>$dataLastWork),200); //,array('rows_progress'=>$dataOnprogress,'rows_aktivitas'=>$dataLastWork),200);

        } catch (\QueryException $th) {
            $result = showOutputResponse($param->Func,false,'Perubahan mengalami gangguan, coba beberapa saat lagi',array(),200);
        }
        
        return $result;
    }




    public static function updateBookingDetail($param){
            $obj = (object)json_decode($param->data_json,true);
            $id_pelanggan= $obj->IdPelanggan;
            
            foreach ($obj->DetailOrder as $value) {
                if($param->note=='pekerjaan_khusus'){
                    DB::table('tb_booking_detail')->insert([
                        'id_booking'=>$param->id_booking, 
                        'jenis'=>$value['Jenis'],
                        'id_pekerjaan_detail'=>0, 
                        'harga'=>$value['Harga'],
                        'qty'=>$value['Qty'],
                        'keterangan'=>$value['Keterangan']
                    ]);
                }else{
                    $data = DB::table('tb_booking_detail')
                    ->where('id_pekerjaan_detail',$value['idPekerjaanDetail'])
                    ->where('id_booking',$param->id_booking)
                    ->where('jenis',$value['Jenis']);
                    if($data->exists()){
                        $qty=0;
                        foreach ($data->get() as $val) {
                            $qty = $val->qty+$value['Qty'];
                        }
                        DB::table('tb_booking_detail')
                            ->update(['qty'=>$qty])
                            ->where('id_pekerjaan_detail',$value['idPekerjaanDetail'])
                            ->where('id_booking',$param->id_booking)
                            ->where('jenis',$value['Jenis)']);
                    }else{
                        DB::table('tb_booking_detail')->insert([
                            'id_booking'=>$param->id_booking, 
                            'jenis'=>$value['Jenis'],
                            'id_pekerjaan_detail'=>$value['idPekerjaanDetail'],
                            'harga'=>$value['Harga'],
                            'qty'=>$value['Qty']
                        ]);
                    }
                }
            }

            $totalTagihan = DB::table('tb_booking_detail')->where('id_booking',$param->id_booking)->sum(DB::raw('harga*qty'));
            DB::table('tb_booking')->where('id_booking',$param->id_booking)->update(['tagihan'=>$totalTagihan]);
            $dataListDetail = self::getListBookingDetail($param->id_booking);

            $dataUser = DB::table('tb_booking')
                        ->join('tb_pelanggan','tb_booking.id_pelanggan','=','tb_pelanggan.id_pelanggan')
                        ->select('kode_booking','gcm_code')
                        ->where('id_booking',$param->id_booking)
                        ->get();

            $message  = "Terjadi perubahan item pekerjaan pada order kode booking ".$dataUser[0]->kode_booking." oleh petugas, silakan melakukan pengecekan pada aplikasi";
            $dataNotif= (object)array(
                'title'=>"Informasi Booking",
                'message'=>$message,
                'image'=>'-',
                'data'=>array('team'=>'Indonesia') );
            $res = sendPushNotification($dataUser[0]->gcm_code,$dataNotif);


            return  showOutputResponse($param->Func,true,'Data Detail Booking Berhasil Ditambahkan!',array('rows_detail'=>$dataListDetail),200);
    }
    
    public static function managQtyBookingDetail($param){ 

            if($param->mode=="delete"){
                DB::table('tb_booking_detail')->where('id_booking_detail', '=', $param->id_booking_detail)->delete();
            }else{
                DB::table('tb_booking_detail')->where('id_booking_detail',$param->id_booking_detail)->update(['qty'=>$param->qty]);
            }

            $totalTagihan = DB::table('tb_booking_detail')->where('id_booking',$param->id_booking)->sum(DB::raw('harga*qty'));
            DB::table('tb_booking')->where('id_booking',$param->id_booking)->update(['tagihan'=>$totalTagihan]);
            $data = self::getListBookingDetail($param->id_booking);

            $dataUser = DB::table('tb_booking')
                        ->join('tb_pelanggan','tb_booking.id_pelanggan','=','tb_pelanggan.id_pelanggan')
                        ->select('kode_booking','gcm_code','id_petugas')
                        ->where('id_booking',$param->id_booking)
                        ->get();

            $dataOnprogress = Booking::getOnprogressPetugas($dataUser[0]->id_petugas);
            $dataLastWork = Booking::getLastWorkPetugas($dataUser[0]->id_petugas);

            $message  = "Terjadi perubahan item pekerjaan pada order kode booking ".$dataUser[0]->kode_booking." oleh petugas, silakan melakukan pengecekan pada aplikasi";
            $dataNotif= (object)array(
                'title'=>"Informasi Booking",
                'message'=>$message,
                'image'=>'-',
                'data'=>array('team'=>'Indonesia') );
            $res = sendPushNotification($dataUser[0]->gcm_code,$dataNotif);

            $dataLayananPetugas = array();
            $datadummy = array();
            foreach ($dataOnprogress as $i => $rows) {
                $prm = (object)array('id_booking'=>$rows->id_booking);
                $dataBooking = self::getDataBooking($prm);
                $dataBookingDetail = self::getDataBookingDetail($prm);
                $dataBookingStatus = self::getDataBookingStatus($prm);
                if(in_array($dataBooking[0]->status_order,array("SURVEY_SELESAI","DITERIMA","DIKERJAKAN") )==1){
                    $dataLayananPetugas = self::getLayananPetugas($dataBooking[0]->id_petugas,$dataBooking[0]->kode_layanan);
                }

                $dataOnprogress[$i]->data = array('rows_status'=>$dataBookingStatus,'rows_utama'=>$dataBooking,'rows_detail'=>$dataBookingDetail,'rows_pekerjaan_petugas'=>$dataLayananPetugas);
            }
            
            return  showOutputResponse($param->Func,true,'Data Detail Booking Berhasil Diperbaharui!',array('rows_detail'=>$data,'rows_progress'=>$dataOnprogress,'rows_aktivitas'=>$dataLastWork),200);
    }
    
    public static function updateDiskonBooking($param){ 

            $totalTagihan = DB::table('tb_booking_detail')->where('id_booking',$param->id_booking)->sum(DB::raw('harga*qty'));
            DB::table('tb_booking')->where('id_booking',$param->id_booking)->update(['tagihan'=>$totalTagihan,'diskon'=>$param->diskon]);

            $dataUser = DB::table('tb_booking')
                        ->join('tb_pelanggan','tb_booking.id_pelanggan','=','tb_pelanggan.id_pelanggan')
                        ->select('kode_booking','gcm_code')
                        ->where('id_booking',$param->id_booking)
                        ->get();

            $message  = "Diskon pada order kode booking ".$dataUser[0]->kode_booking." telah di ubah oleh petugas, silakan melakukan pengecekan pada aplikasi";
            $dataNotif= (object)array(
                'title'=>"Informasi Booking",
                'message'=>$message,
                'image'=>'-',
                'data'=>array('team'=>'Indonesia') );
            $res = sendPushNotification($dataUser[0]->gcm_code,$dataNotif);


            $dataOnprogress = Booking::getOnprogressPetugas($param->id_petugas);

            $dataLayananPetugas = array();
            $datadummy = array();
            foreach ($dataOnprogress as $i => $rows) {
                $prm = (object)array('id_booking'=>$rows->id_booking);
                $dataBooking = self::getDataBooking($prm);
                $dataBookingDetail = self::getDataBookingDetail($prm);
                $dataBookingStatus = self::getDataBookingStatus($prm);
                if(in_array($dataBooking[0]->status_order,array("SURVEY_SELESAI","DITERIMA","DIKERJAKAN") )==1){
                    $dataLayananPetugas = self::getLayananPetugas($dataBooking[0]->id_petugas,$dataBooking[0]->kode_layanan);
                }

                $dataOnprogress[$i]->data = array('rows_status'=>$dataBookingStatus,'rows_utama'=>$dataBooking,'rows_detail'=>$dataBookingDetail,'rows_pekerjaan_petugas'=>$dataLayananPetugas);
            }


            return  showOutputResponse($param->Func,true,'Data Detail Booking Berhasil Diperbaharui!',array('rows_progress'=>$dataOnprogress),200);
    }


    private static function prosesPotongDepositJikaBatal($dtBkg,$param){
        if(strlen($param->aksi_cancel)>2 && $param->request=="CANCEL" && $param->aksi_cancel=="CANCEL_LANJUT"){
            $biaya_survey = $dtBkg->kategori_pekerjaan=="LISTRIK"?$dtBkg->biaya_survey:$dtBkg->biaya_pembatalan; 
            DB::table('tb_booking')->where('id_booking',$dtBkg->id_booking)->update(['biaya_survey'=>$dtBkg->biaya_survey]);
            
            $fee_gd = $biaya_survey*$dtBkg->persen_fee_gd;
            $fee_mitra = ($biaya_survey-$fee_gd)*$dtBkg->persen_fee_mitra;
            $fee_petugas = ($biaya_survey-$fee_gd)*$dtBkg->persen_fee_petugas;
            $keterangan = " Fee Tagihan Biaya Survey ".$dtBkg->kode_booking." sebesar Rp.".number_format($fee_gd,2,',','.')."(".($dtBkg->persen_fee_gd*100)."%) dari Rp. ".number_format($biaya_survey,2,',','.');
            
            $saldoAkhir = DepositModel::getSaldoAkhir($dtBkg->id_perusahaan,$fee_gd,0);
            $result = DB::table('tb_deposit')->insert([
                'id_perusahaan'=>$dtBkg->id_perusahaan,
                'id_booking'=>$dtBkg->id_booking,
                'tgl_transaksi'=>Carbon::now(),
                'kredit'=>0,
                'debet'=>$fee_gd,
                'saldo'=>$saldoAkhir, 
                'kode_transaksi'=>'TARIKAN',
                'keterangan'=>'Potongan'.$keterangan,
                'jenis_deposit'=>'DEPOSIT',
                'status'=>'Y',
                'create_date'=>Carbon::now(),
                'created_at'=>Carbon::now() 
            ]);
            
            // DISTRIBUSI FEE KE PETUGAS
            $saldo_petugas = $dtBkg->saldo_petugas+$fee_petugas;
            $result = DB::table('tb_booking_fee_petugas')->insert([
                'id_petugas'=>$dtBkg->id_petugas,
                'id_booking'=>$dtBkg->id_booking,
                'nominal'=>$fee_petugas,
                'keterangan'=>$keterangan,
                'who'=>'sistem-auto',
                'debet'=>'0',
                'kredit'=>$fee_petugas,
                'saldo'=>$saldo_petugas
            ]); 

            // DISTRIBUSI FEE KE POST MASING
            $resFee=DB::table('tb_booking_fee')->where('id_booking',$dtBkg->id_booking);
            $dataValue = array(
                    'id_booking'=>$dtBkg->id_booking,
                    'total_tagihan'=>$tagihan_total,
                    'fee_geraidaya'=>$fee_gd,
                    'fee_mitra'=>$fee_mitra,
                    'fee_petugas'=>$fee_petugas,
                    'setoran'=>$fee_petugas,
                    'fee_material'=>0,
                    'pajak_geraidaya'=>0,
                    'pajak_mitra'=>0,
                    'pajak_petugas'=>0,
                    'total_pajak'=>0
            );
            if($resFee->exists()){
                $resFee = $resFee->first();
                DB::table('tb_booking_fee')
                    ->where('id',$resFee->id)
                    ->update(array_merge($dataValue,array('status'=>'1','update_at'=>Carbon::now())));
            }else{
                DB::table('tb_booking_fee')->insert($dataValue); 
            }

            $dataKwitansi = self::getDataKwitansiBooking($dtBkg->id_booking);
            kirimKwitansi($dataKwitansi);

        }
    }
    private static function prosesPotongDeposit($dtBkg){
        if($dtBkg->metode_pembayaran=='TUNAI'){
            
            $tagihan_total= ($dtBkg->tagihan+$dtBkg->biaya_survey)-$dtBkg->diskon;
            $fee_gd1 = $dtBkg->nominal_jasa*$dtBkg->persen_fee_gd;
            $fee_gd2 = $dtBkg->nominal_material*$dtBkg->persen_fee_gd;
            $fee_gd3 = $dtBkg->biaya_survey*$dtBkg->persen_fee_gd;
            $pot_diskon = $dtBkg->diskon*$dtBkg->persen_fee_gd;
            $fee_gd_bruto =  ($fee_gd1+$fee_gd2+$fee_gd3)-$pot_diskon;

            if($dtBkg->status_pkp_gd=="Ya"){
                $fee_gd = ceil(round((100/110)*$fee_gd_bruto,2));
                $pajak_gd =  ceil(round(($fee_gd_bruto - $fee_gd),2));
            }else{
                $fee_gd = $fee_gd_bruto;
                $pajak_gd =  "0";
            }

            $fee_mitra = ($dtBkg->nominal_jasa-$fee_gd1)*$dtBkg->persen_fee_mitra;
            $fee_mitra1 = ($dtBkg->nominal_material-$fee_gd2);
            $fee_mitra2 = ($dtBkg->biaya_survey-$fee_gd3)*$dtBkg->persen_fee_mitra;
            $pot_diskon1 = ($dtBkg->diskon-$pot_diskon)*$dtBkg->persen_fee_mitra;
            $fee_mitra_bruto =  $dtBkg->jenis_bentuk_usaha=="PERUSAHAAN"? ($fee_mitra+$fee_mitra1+$fee_mitra2)-$pot_diskon1:"0";

            if($dtBkg->status_pkp_perusahaan=="Ya"){
                $fee_mitra = ceil(round((100/110)*$fee_mitra_bruto,2));
                $pajak_mitra =  ceil(round(($fee_mitra_bruto - $fee_mitra),2));
            }else{
                $fee_mitra = $fee_mitra_bruto;
                $pajak_mitra =  "0";
            }

            $fee_petugas = ($dtBkg->nominal_jasa-$fee_gd1)*$dtBkg->persen_fee_petugas;
            $fee_petugas1 = 0;
            $fee_petugas2 = ($dtBkg->biaya_survey-$fee_gd3)*$dtBkg->persen_fee_petugas;
            $pot_diskon2 = $dtBkg->jenis_bentuk_usaha=="PERUSAHAAN"?($dtBkg->diskon-$pot_diskon)*$dtBkg->persen_fee_petugas:$dtBkg->diskon-$pot_diskon;

            $fee_petugas_perorangan = ($dtBkg->nominal_jasa-$fee_gd1)+($dtBkg->nominal_material-$fee_gd2)+($dtBkg->biaya_survey-$fee_gd3);
            $fee_petugas_bruto =  $dtBkg->jenis_bentuk_usaha=="PERUSAHAAN"?($fee_petugas+$fee_petugas1+$fee_petugas2)-$pot_diskon2:($fee_petugas_perorangan-$pot_diskon2);

            if($dtBkg->status_pkp_perusahaan=="Ya"){
                $fee_petugas = ceil(round((100/110)*$fee_petugas_bruto,2));
                $pajak_petugas =  ceil(round(($fee_petugas_bruto - $fee_petugas),2));
            }else{
                $fee_petugas = $fee_petugas_bruto;
                $pajak_petugas =  "0";
            }

            $total_pajak = ceil(round(($pajak_gd+$pajak_mitra+$pajak_petugas),2));
            $saldoAkhir = DepositModel::getSaldoAkhir($dtBkg->id_perusahaan,$fee_gd,0);
            $fee_material= "0" ;
            $persenGd =$dtBkg->persen_fee_gd*100;
            // PENGURANGAN DEPOSIT
            $result = DB::table('tb_deposit')->insert([
                'id_perusahaan'=>$dtBkg->id_perusahaan,
                'id_booking'=>$dtBkg->id_booking,
                'tgl_transaksi'=>Carbon::now(),
                'kredit'=>0,
                'debet'=>$fee_gd,
                'saldo'=>$saldoAkhir, 
                'kode_transaksi'=>'TARIKAN',
                'keterangan'=>"Potongan Fee Tagihan ".$dtBkg->kode_booking." sebesar Rp.".number_format($fee_gd,2,',','.')."(".$persenGd."%) dari Rp. ".number_format($tagihan_total,2,',','.'),
                'jenis_deposit'=>'DEPOSIT',
                'status'=>'Y',
                'create_date'=>Carbon::now(),
                'created_at'=>Carbon::now() 
            ]);
            
            // DISTRIBUSI FEE KE PETUGAS
            $saldo_petugas = $fee_petugas_bruto+$dtBkg->saldo_petugas;
            $result = DB::table('tb_booking_fee_petugas')->insert([
                'id_petugas'=>$dtBkg->id_petugas,
                'id_booking'=>$dtBkg->id_booking,
                'nominal'=>$fee_petugas_bruto,
                'keterangan'=>"Tambahan Fee dari ".$dtBkg->kode_booking." sebesar Rp.".number_format($fee_petugas_bruto,2,',','.'),
                'who'=>'sistem-auto',
                'debet'=>'0',
                'kredit'=>$fee_petugas_bruto,
                'saldo'=>$saldo_petugas
            ]); 

            // POTONGAN PAJAK KE PETUGAS
            if($pajak_petugas>0){
                $saldo_petugas = $saldo_petugas-$pajak_petugas;
                $result = DB::table('tb_booking_fee_petugas')->insert([
                    'id_petugas'=>$dtBkg->id_petugas,
                    'id_booking'=>$dtBkg->id_booking,
                    'nominal'=>$pajak_petugas,
                    'keterangan'=>"Potongan Pajak 10% dari Rp.".number_format($fee_petugas_bruto,2,',','.'),
                    'who'=>'sistem-auto',
                    'debet'=>'0',
                    'kredit'=>$pajak_petugas,
                    'saldo'=>$saldo_petugas
                ]);  
            }

            // DISTRIBUSI FEE KE POST MASING
            $resFee=DB::table('tb_booking_fee')->where('id_booking',$dtBkg->id_booking);
            $dataValue = array(
                    'id_booking'=>$dtBkg->id_booking,
                    'total_tagihan'=>$tagihan_total,
                    'fee_geraidaya'=>$fee_gd,
                    'fee_mitra'=>$fee_mitra,
                    'fee_petugas'=>$fee_petugas,
                    'fee_material'=>$fee_material,
                    'pajak_geraidaya'=>$pajak_gd,
                    'pajak_mitra'=>$pajak_mitra,
                    'pajak_petugas'=>$pajak_petugas,
                    'total_pajak'=>$total_pajak
            );
            if($resFee->exists()){
                $resFee = $resFee->first();
                DB::table('tb_booking_fee')
                    ->where('id',$resFee->id)
                    ->update(array_merge($dataValue,array('status'=>'1','update_at'=>Carbon::now())));
            }else{
                DB::table('tb_booking_fee')->insert($dataValue); 
            }

            $dataKwitansi = self::getDataKwitansiBooking($dtBkg->id_booking);
            kirimKwitansi($dataKwitansi);

            
        } 
    }


    public static function getDataKwitansiBooking($idBooking){

        $data_booking = DB::select(" SELECT 
                        DATE_FORMAT(tgl_selesai,'%d-%m-%Y %H:%i') AS tanggal,
                        c.nama as nama_petugas,
                        a.nama as nama_pelanggan,
                        a.email as email,
                        d.nama as nama_perusahaan,
                        metode_pembayaran as cara_bayar,
                        b.alamat as lokasi_pekerjaan,
                        keluhan,
                        FORMAT(b.biaya_survey,0,'de_DE') as biaya_survey,
                        FORMAT(tagihan,0,'de_DE') as subtotal,
                        FORMAT(ifnull(diskon,0),0,'de_DE') as diskon,
                        FORMAT((b.biaya_survey+tagihan)-diskon,0,'de_DE') as total,
                        kode_booking
                    FROM tb_pelanggan a,tb_booking b 
                    LEFT JOIN tb_petugas c on c.id_petugas=b.id_petugas
                    LEFT JOIN tb_perusahaan d on d.id_perusahaan=b.id_perusahaan
                    WHERE a.`id_pelanggan`=b.`id_pelanggan` AND b.id_booking =".$idBooking);
            

        $data_booking_detail = DB::select(" SELECT xx.* from (
								SELECT
									id_booking,
									concat(qty,'x ') as qty,
									nama_jasa as item,
									FORMAT((a.harga*qty),0,'de_DE') as jumlah,
									FORMAT(a.harga,0,'de_DE') as harga,
									'JASA' as jenis
								FROM tb_booking_detail a, tb_master_harga_jasa b , tb_master_jasa c
								where id_pekerjaan_detail = b.id_harga_jasa and b.id_jasa=c.id_jasa
                                and a.id_booking = ".$idBooking."
								and jenis='JASA'
								UNION
								SELECT id_booking,
									concat(qty,'x ') as qty,
									nama_material as item,
									FORMAT((a.harga*qty),0,'de_DE') as jumlah,
									FORMAT(a.harga,0,'de_DE') as harga,
									'MATERIAL' as jenis
								FROM tb_booking_detail a, tb_master_harga_material b , tb_master_material c
								where id_pekerjaan_detail = b.id_harga_material and b.id_material=c.id_material
								and jenis='MATERIAL'
                                and a.id_booking = ".$idBooking."

								UNION
								SELECT id_booking,
									concat(qty,'x ') as qty,
									keterangan as item,
									FORMAT((a.harga*qty),0,'de_DE') as jumlah,
									FORMAT(a.harga,0,'de_DE') as harga,
									jenis
								FROM tb_booking_detail a 
								WHERE  jenis not in ('JASA','MATERIAL')
                                and a.id_booking = ".$idBooking."
								
						)xx order by jenis
        ");

        // $data_utama=array( 'to'=>'email2yahoo.com'); 
        // $rows['jenis']='JASA';
		// $rowItem[] =$rows;

        // $data=array_merge($data_utama,array('data_detail'=>$rowItem));

        return array('data_booking'=>$data_booking[0],'booking_detail'=>$data_booking_detail);
        // return $data;
    }

    public static function updateFotoBooking($data,$id){
           $result= DB::table('tb_booking')
            ->where('id_booking',$id)
            ->update($data);
            return $result;
    }
   

    public static function OrderPekerjaan($param,$dataPetugas){
        $kode_booking = "BO/".date('Ymds')."/".rand(1000,9999);
        $status_order = $param->cara_bayar=="TUNAI"?"KONFIRMASI_SURVEY_DITERIMA":"MENUNGGU";
        $jenis_pekerjaan = explode('_',$param->jenis_layanan)[0];
        $biaya_survey_unique = $jenis_pekerjaan=="AC"?"0":$param->biaya_survey;
        $data_persen =DB::table('tb_master_persentase')->where("status","1")->first();

        

        try{
            $lastId = DB::table("tb_booking")->insertGetId([
                'kode_booking'=>$kode_booking,
                'id_pelanggan'=>$param->id_pelanggan,
                'id_petugas'=>$param->is_command_center=="Y"?"0":$param->id_petugas,
                'tgl_pesan'=>Carbon::now()->format('Y-m-d H:i:s'),
                'tgl_rencana_survey'=>Carbon::now()->format('Y-m-d H:i:s'),
                'tgl_rencana_dikerjakan'=>$param->tgl_rencana_pengerjaan,
                'alamat'=>$param->alamat,
                'petunjuk'=>$param->petunjuk,
                'keluhan'=>$param->keluhan,
                'status_order'=>$status_order,
                'solusi'=>"",
                'lat_pel'=>$param->latitudeUser,
                'longi_pel'=>$param->longitudeUser,
                'lat_petugas'=>$param->lat_petugas,
                'longi_petugas'=>$param->longi_petugas,
                'lat_progress_petugas'=>$param->lat_petugas,
                'longi_progress_petugas'=>$param->longi_petugas,
                'tagihan'=>@$param->tagihan,
                'biaya_survey'=>$biaya_survey_unique,
                'id_perusahaan'=>$param->is_command_center=="Y"?NULL:$dataPetugas->id_perusahaan,
                'metode_pembayaran'=>$param->cara_bayar,
                'kategori_pekerjaan'=>$jenis_pekerjaan,
                'kode_layanan'=>$param->jenis_layanan,
                'id_persentase'=>$data_persen->id,
                'alasan_penolakan'=>"",
                'foto_sebelum'=>"",
                'foto_sesudah'=>"",
                'diskon'=>"0",
                'biaya_pembatalan'=>"0",
                'created_at'=>Carbon::now()
            ]);
            $idBookingEnc = md5($lastId);

            DB::table('tb_booking')->where('id_booking',$lastId)->update(['id_booking_enc'=>$idBookingEnc]);
            // exit;
            GeneralModel::insertBookingStatus($lastId,$status_order);

            $message  = "Halo Mitra Yth GERAI DAYA, Anda Memiliki Order ".$kode_booking.", Silahkan buka aplikasi untuk info lebih lanjut. Selamat Bekerja";  
            if($param->is_command_center=="N"){
                $obj = (object)json_decode($param->data_json_detail_order,true);
                foreach ($obj->DetailOrder as $value) {

                    DB::table('tb_booking_detail')->insert([
                        'id_pekerjaan_detail'=>$value['idPekerjaanDetail'],
                        'id_booking'=>$lastId, 
                        'jenis'=>$value['Jenis'],
                        'harga'=>$value['Harga'],
                        'qty'=>$value['Qty']
                    ]);
                       
                } 
                sendSMS($dataPetugas->hp,$message,"REGULER");

                DB::table('tb_petugas')->where('id_petugas',$param->id_petugas)->update(['status_kerja'=>'WORK','latitude'=>$param->lat_petugas,'longitude'=>$param->longi_petugas]);

                $dataNotif= (object)array(
                    'title'=>"Informasi Booking Baru",
                    'message'=>$message,
                    'image'=>'-',
                    'data'=>array('team'=>'Indonesia') );
                $res = sendPushNotification($dataPetugas->gcm_code,$dataNotif);
            }

            $dataPelanggan = Pelanggan::getPelangganById($param->id_pelanggan);
            $messagePelanggan = "Nomor Booking Anda : ".$kode_booking.", cek progress secara berkala melalui aplikasi GERAI DAYA";

            $dataNotif= (object)array(
                'title'=>"Booking Baru ".$kode_booking,
                'message'=>$messagePelanggan,
                'image'=>'-',
                'data'=>array('team'=>'Indonesia') );
            $res = sendPushNotification($dataPelanggan->gcm_code,$dataNotif);
            
            $dataEmail =DB::table('tb_master_data')
                            ->select(DB::raw('value as Email,deskripsi as Name,gcm_code as gcm_id'))
                            ->join('tb_pelanggan', 'tb_pelanggan.username', '=', 'tb_master_data.value')
                            ->where("kategori","EMAIL_NOTIF_ADMIN")
                            ->get();

            $url_booking=config('app.url_detail_publik').$idBookingEnc;

            $arrayGCM = array();
            $data = json_encode($dataEmail);
            $result = json_decode($data, true);
                for ($i = 0; $i < count($dataEmail); $i++) {
                    array_push($arrayGCM, $result[$i]['gcm_id']);
                }

            $notifAdmin= (object)array(
                'title'=>"Order Baru! Buka Link Segera",
                'message'=>'Cek progress pekerjaan dengan menekan tombol [Buka Link]',
                'image'=>'-',
                'mode'=>'NOTIF_LINK_BOOKING',
                'data'=>array('team'=>'Indonesia','url_booking'=>$url_booking) );
            $res = sendPushNotificationToMany($arrayGCM,$notifAdmin);
             
            $dataMail = array(
                'to'=>$dataEmail,
                'subject'=>"Informasi Order BARU: ".$kode_booking,
                'message'=>"Cek progress pekerjaan melalui link di bawah ini\n".$url_booking,
                'name'=>'Admin Gerai Daya',
                'otp'=>"",
                'isbanyak'=>true
            );
            sendEMAIL($dataMail);

            return  showOutputResponse($param->Func,true,'Permintaan Sudah Diproses, Silahkan Menunggu Konfirmasi dari Petugas Gerai Daya',array('id_booking'=>$lastId),200);
            
        } catch (\Exception $e) {
            dd($e);
            return  showOutputResponse($param->Func,false,'Booking gagal tersimpan, Mohon coba beberapa saat lagi','-',200);

        }
    }


    public static function TambahReview($param){

         DB::table('tb_booking')
         ->where('id_booking',$param->id_booking)
         ->update([
             "tgl_selesai"=>Carbon::now(),
             "status_order"=>"CLOSE",
             "jml_rating"=>$param->jml_rating,
             "masukan"=>$param->masukan,
             "tgl_rating"=>Carbon::now()]);
         GeneralModel::insertBookingStatus($param->id_booking,"CLOSE");

        $dataBooking = self::getDataBooking($param)[0];
        // echo $dataBooking->id_booking." ".json_encode($dataBooking);
        // exit;

        $dataPelanggan = Pelanggan::getPelangganById($dataBooking->id_pelanggan);
        $dataPetugas = Petugas::getPetugasById($dataBooking->id_petugas);

        $message = "Orderan dengan kode booking <b>".$dataBooking->kode_booking."</b> telah diberi ulasan. Terimakasih";
        $dataNotif= (object)array(
            'title'=>"Informasi Rating Pekerjaan",
            'message'=>$message,
            'image'=>'-',
            'data'=>array('team'=>'Indonesia') );
        $res = sendPushNotification($dataPelanggan->gcm_code,$dataNotif);
        $res = sendPushNotification($dataPetugas->gcm_code,$dataNotif);
        

         return  showOutputResponse($param->Func,true,'Terimakasih Atas Kritik dan Sarannya',array(),200);
    }  
    
    public static function updateLocation($param){
            DB::table('tb_petugas')
            ->where('id_petugas',$param->id_petugas)
            ->update(["latitude"=>$param->latitude,"longitude"=>$param->longitude]);

            $dataLog = DB::table('tb_log_lokasi')
            ->where('id_petugas',$param->id_petugas)
            ->whereDate('date',Carbon::now()->format('Y-m-d'));

            if($dataLog->doesntExist()){
                DB::table('tb_log_lokasi')->insert([
                    'id_petugas'=>$param->id_petugas,
                    'latitude'=>$param->latitude,
                    'longitude'=>$param->longitude,
                    'date'=>Carbon::now(),
                    'created_at'=>Carbon::now(),
                ]);
            }else{
                $data = $dataLog->first();
                DB::table('tb_log_lokasi')
                ->where('id',$data->id)
                ->update(["latitude"=>$param->latitude,"longitude"=>$param->longitude,'created_at'=>Carbon::now()]);
            }
         return  showOutputResponse($param->Func,true,'Berhasil update lokasi',array(),200);
    } 


    


}