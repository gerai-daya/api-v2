<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Support\Facades\DB;
use App\Models\GeneralModel;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\Booking;
use Carbon\Carbon;

class DepositModel extends Model
{
    protected $table='tb_deposit';
    protected $fillable = [
        'id_perusahaan','id_booking','tgl_transaksi','debet','kredit','saldo','kode_transaksi','keterangan','status','jenis_deposit','create_date','update_date','create_by','update_by','created_at','updated_at'
    ];


    private static function getIdPerusahaan($isPerusahaan,$id){
        $id_perusahaan = $id;
        if(!$isPerusahaan){
            $data  =Petugas::where('id_petugas',$id);
            $id_perusahaan="0";
            if($data->exists()){
                $id_perusahaan = Petugas::where('id_petugas',$id)->first()->id_perusahaan;
            }
            
        }
        return $id_perusahaan;
    } 
    
    public static function getPermohonanDepositById($id){
        $res = DB::table("tb_konfirmasi_deposit")->where('id',$id);
        return $res;
    }

    private static function getSaldoDeposit($isPerusahaan,$id){
        $id_perusahaan = $id;
        if(!$isPerusahaan){
            $id_perusahaan = self::getIdPerusahaan($isPerusahaan,$id);
        }
        $saldo = DB::select("SELECT FORMAT(IFNULL(fnGetSaldoAkhir ( ".$id_perusahaan.",'".Carbon::now()->format('Y-m-d')."','DEPOSIT') ,0) ,0,'de_DE') saldo_deposit");
        return $saldo[0]->saldo_deposit;
    }

    public static function getSaldoAkhir($id_perusahaan,$debet,$kredit){
        $saldo = DB::select("SELECT IFNULL(fnGetSaldo(".$id_perusahaan.",'".Carbon::now()->format('Y-m-d')."',$debet,$kredit,'DEPOSIT') ,0) saldo");
        return $saldo[0]->saldo;
    }


    private static function getListPengajuanDeposit($param,$id_perusahaan){
        $result = DB::table('tb_konfirmasi_deposit')->where([
                    ['id_perusahaan','=',$id_perusahaan],
                    [DB::raw('year(create_at)'),'=',$param->tahun],
                    [DB::raw('month(create_at)'),'=',$param->bulan]])
                    ->select(DB::raw("id,id_bank,ifnull(id_deposit,0) as id_deposit,jml_deposit,date_format(create_at,'%d-%m-%Y %H:%i') as tgl_transaksi, status, concat('".config('app.url_bukti_deposit')."',url_foto) as url_foto 
                    "))
                    ->orderBy('created_at', 'desc')->get();
        return $result;
    }

    
    public static function getListHistoryDeposit($param){
        $id_perusahaan = self::getIdPerusahaan(false,$param->id_petugas);
        $saldo_deposit = self::getSaldoDeposit(true,$id_perusahaan);
        $data_deposit = DepositModel::where([
                    ['id_perusahaan','=',$id_perusahaan],
                    [DB::raw('year(tgl_transaksi)'),'=',$param->tahun],
                    [DB::raw('month(tgl_transaksi)'),'=',$param->bulan]
                    ]
                    )
                    ->select(DB::raw("id_deposit,id_perusahaan,date_format(tgl_transaksi,'%d-%m-%Y %H:%i') as tgl_transaksi,debet,kredit,saldo,kode_transaksi,keterangan"))
                    ->orderBy('tgl_transaksi', 'desc')->get();

        $pengajuan =self::getListPengajuanDeposit($param,$id_perusahaan);
        $data =array('saldo_deposit'=>$saldo_deposit,"rows_history"=>$data_deposit,"rows_pengajuan"=>$pengajuan);
        return $data;
    }
    
    public static function manajemenDeposit($param,$isUpload,$imageName){
        $id_perusahaan = self::getIdPerusahaan(false,$param->id_petugas);
        if($param->id_deposit==0){
            $result = DB::table('tb_konfirmasi_deposit')->insert([
                    'id_perusahaan'=>$id_perusahaan,
                    'id_bank'=>$param->id_bank,
                    'jml_deposit'=>$param->jumlah,
                    'jml_transfer'=>$param->jumlah,
                    'url_foto'=>$isUpload?$imageName:'',
                    'atas_nama_pembayar'=>'-',
                    'bank_pembayar'=>'-',
                    'tgl_transfer'=>Carbon::now()->format('Y-m-d'),
                    'status'=>'review',
                    'create_by'=>$param->user,
                    'created_at'=>Carbon::now()
            ]);
            $urlImage = $isUpload?config('app.url_bukti_deposit').$imageName:"-";
            $pengajuan =self::getListPengajuanDeposit($param,$id_perusahaan);
            return  showOutputResponse($param->Func,$result?true:false,$result?'Deposit Anda Berhasil Ditambahkan, Mohon menunggu konfirmasi dari admin kami.':'Data Deposit Gagal Disimpan, Mohon coba beberapa saat lagi',array('url_photo'=>$urlImage,"rows_pengajuan"=>$pengajuan),200);
        }else{
            $data = DB::table('tb_konfirmasi_deposit')->where('id',$param->id_deposit)->first();
            if($isUpload){
                if(strlen($data->url_foto)>0){
                    if (file_exists(config('app.path_bukti_deposit').$data->url_foto) ){
                        unlink($path.$data->url_foto);
                    }
                }
            }
           
            $result = DB::table('tb_konfirmasi_deposit')->where('id',$param->id_deposit)->update([
                'id_bank'=>$param->id_bank,
                'jml_deposit'=>$param->jumlah,
                'jml_transfer'=>$param->jumlah,
                'status'=>'review',
                'url_foto'=>$isUpload?$imageName:$data->url_foto,
                'updated_at'=>Carbon::now(),
                'update_by'=>$param->user
            ]);
            $urlImage = $isUpload?config('app.url_bukti_deposit').$imageName:config('app.url_bukti_deposit').$data->url_foto;
            $pengajuan =self::getListPengajuanDeposit($param,$id_perusahaan);
            return  showOutputResponse($param->Func,$result?true:false,'Deposit Anda Berhasil diperbaharui',array('url_photo'=>$urlImage,"rows_pengajuan"=>$pengajuan),200);
        }
       

    }
    
   

}
