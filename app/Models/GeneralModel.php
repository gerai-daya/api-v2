<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\TokenRequest;
use Carbon\Carbon;

class GeneralModel extends Model
{
    public static function insertLogTos($idPelanggan,$tipeUser){
            
            $idTos = DB::table('tb_master_tos')->where([['status','=','1'],['tipe_user','=',$tipeUser]])->first()->id;
            $doesntExist = DB::table('tb_log_tos')->where([['id_user','=',$idPelanggan],['tipe_user','=',$tipeUser]])->doesntExist();
            if($doesntExist){
                $respose = DB::table('tb_log_tos')->insert([
                    'tipe_user'=>'0',
                    'id_user'=>$idPelanggan,
                    'id_tos'=>$idTos,
                    'created_at'=>Carbon::now(),
                ]);
            }
            

        return @$respose;
    }


    public static function insertLogRequest($name,$type,$param,$location){
        $respose = DB::table('tb_log_request')->insert([
            'name'=>$name,
            'type'=>$type,
            'param'=>$param,
            'location'=>$location,
            'created_at'=>Carbon::now(),
        ]);
        return $respose;
    }
    public static function insertLogLogin($id_user,$tipe_user,$token){
        if($id_user!='0'){
            DB::table('tb_log_login')->where([['id_user','=', $id_user],['status','=', 'Y']])->update(['status' => 'N','updated_at' => Carbon::now()]);
        }
        $respose = DB::table('tb_log_login')->insert([
            'id_user'=>$id_user,
            'tipe_user'=>$tipe_user,
            'token_login'=>$token,
            'status'=>'Y',
            'created_at'=>Carbon::now(),
        ]);
        return $respose;
    } 

    public static function closeToken($token){
        DB::table('tb_log_login')->where('status','Y')->where('token_login',$token)->update(['status' => 'N','updated_at' => Carbon::now()]);
    }

    public static function closeOTPId($id){
        $affected = TokenRequest::whereIn('id_pelanggan',[$id])->where('status', 'OPEN')->update(['status' => 'CLOSE','updated_at' => Carbon::now()]);
        return $affected;
    }

    public static function usedOTP($token,$no_ref){
        $response = TokenRequest::where('kode_token',$token)->where('no_ref',$no_ref)->update(['status' => 'CLOSE','used_date' => Carbon::now(),'updated_at' => Carbon::now()]);
        return $response;
    }

    public static function insertOTP($param){
        $response = TokenRequest::insertGetId([
            'kode_token'=>$param->kode_otp,
            'no_ref'=>$param->no_ref,
            'status'=>'OPEN',
            'id_pelanggan'=>$param->id_pelanggan,
            'request_date'=>$param->request_date,
            'expired_date'=>$param->expired_date,
            'created_at'=>Carbon::now(),
        ]);
        return $response;
    }
    
    public static function getDataOtp($id){
        return TokenRequest::where('id_token',$id)->first();
    }
    public static function getDataOtpIsExist($param){
        return TokenRequest::where([['status','=','OPEN'],['kode_token','=',$param->token],['no_ref','=',$param->noref],['id_pelanggan','=',$param->id_pelanggan]])->first();
    }

    public static function getDataOtpByOtpNoref($param){
        return TokenRequest::where([['status','=','OPEN'],['kode_token','=',$param->token],['no_ref','=',$param->noref]])->first();
    }
 
    public static function handleGCMCode($param,$id){
        $data = DB::table('tb_gcm_user')->where('id_relasi',$id)->where('tipe_user',$param->tipe_user);
        if($data->exists()){
            $data = $data->first();
            DB::table('tb_gcm_user')->where('id_relasi',$id)->update(['kode_imei'=>$param->imei,'gcm_code'=>$param->gcm_code,'updated_at'=>Carbon::now()]);
        }else{
            $data = DB::table('tb_gcm_user')->where('kode_imei',$param->imei)->where('tipe_user',$param->tipe_user);
            if($data->exists()){
                $data = $data->first();
                DB::table('tb_gcm_user')->where('kode_imei',$param->imei)->where('tipe_user',$param->tipe_user)->update(['gcm_code'=>$param->gcm_code,'updated_at'=>Carbon::now()]);
            }else{
                DB::table('tb_gcm_user')->insert(['id_relasi'=>$id!=0?$id:NULL,'kode_imei'=>$param->imei,'tipe_user'=>$param->tipe_user,'created_at'=>Carbon::now(),'gcm_code'=>$param->gcm_code]);
            }
        }
        return true;
    }
    
    public static function insertApproveTos($param){
        $data= DB::table('tb_log_tos')
            ->leftJoin('tb_master_tos', 'tb_log_tos.id_tos', '=', 'tb_master_tos.id')
            ->where([['tb_log_tos.id_user','=',$param->id_user],['tb_log_tos.tipe_user','=',$param->tipe_user],['tb_master_tos.status','=','1'],['tb_master_tos.tipe_user','=',$param->tipe_user]])
            ->exists();
        if($data<=0){
            $data_tos = DB::table('tb_master_tos')->where('tipe_user',$param->tipe_user)->where('status','1')->first();
            DB::table('tb_log_tos')->insert(['tipe_user'=>$param->tipe_user,'id_user'=>$param->id_user,'created_at'=>Carbon::now(),'id_tos'=>$data_tos->id]);
        } 
        return $data;
    }
    
    public static function insertLogEmail($response){
        $data = json_decode($response, true);
        DB::table('tb_log_email')->insert([
                'status'=>@$data['Messages'][0]['Status'],
                'raw_data'=>$response,
                'created_at'=>Carbon::now() 
                ]);
        return $data;
    }
    
    public static function updateLocation($param){
            $result = DB::table('tb_petugas')->where('id_petugas',$param->id_petugas)->update(['latitude'=>$param->latitude,'longitude'=>$param->longitude]);
            $res = DB::table('tb_log_lokasi')->where('id_petugas',$param->id_petugas)->whereDate('date',Carbon::now()->toDateString())->first();
            if($res){
                $result = DB::table('tb_log_lokasi')
                    ->where('id_petugas',$param->id_petugas)
                    ->whereDate('date',Carbon::now()->toDateString())
                    ->update(['latitude'=>$param->latitude,'longitude'=>$param->longitude,'date'=>Carbon::now(),'updated_at'=>Carbon::now()]);
            }else{
                DB::table('tb_log_lokasi')->insert([
                    'latitude'=>$param->latitude,
                    'longitude'=>$param->longitude,
                    'id_petugas'=>$param->id_petugas,
                    'date'=>Carbon::now(),
                    'created_at'=>Carbon::now()
                    ]);
            }

            $result = DB::table('tb_booking')
                    ->where('id_petugas',$param->id_petugas)
                    ->whereNotIn('status_order', ['SELESAI', 'DITOLAK','CANCEL'])
                    ->whereDate('tgl_pesan',Carbon::now()->toDateString())
                    ->update(['lat_progress_petugas'=>$param->latitude,'longi_progress_petugas'=>$param->longitude,'time_progress'=>Carbon::now()]);
        return true;
    }
    
    public static function getDataSplashScreen($param){

                    $rows_about=array(array('nama_perusahaan'=>'PT. Gerai Daya Indonesia','alamat'=>'Jl. Tukad Badung N0. 234, Denpasar Bali - Indonesia','web'=>'https://geraidaya.id','email'=>'info@geraidaya.id','telp'=>'081138117711'));
                    $rows_slide = DB::table('tb_master_slide')
                                ->select(DB::raw(" ifnull(concat('".config('app.url_dashboard')."file_upload/slide_android/',url_image) ,'".config('app.url_nofoto')."') as url_image, title,`desc`"))
                                ->get();
                                
                    $masterData0 = DB::table('tb_master_data')
                                ->select(DB::raw('kategori,subkategori,value'))
                                ->whereIn('kategori',['STATUS_LAYANAN_AC','LINK_SYARAT_KETENTUAN','LINK_KEBIJAKAN_PRIVACY','TAG_KELUHAN','KORDINAT_BATAS','IS_COMMAND_CENTER','BIAYA_SURVEY','BIAYA_PEMBATALAN']);
                                
                    $master_data = DB::table('tb_bank')
                                ->select(DB::raw("'BANK' as kategori,id_bank as subkategori,concat(nama_bank,' - ',no_rekening,' - ',atas_nama) as value"))
                                ->union($masterData0)
                                ->get();

                    $rows_update = DB::table('tb_log_updateapp')
                                ->select(DB::raw("version_code,version_name,`desc`,date_format(create_at,'%d-%m-%Y') as create_at "))
                                ->where('status','1')
                                ->where('tipe_user',$param->tipe_user)
                                ->get();

                    $rows_wilayah = DB::table('tb_master_zona_wilayah')
                                ->join('provinsi', 'tb_master_zona_wilayah.id_prov', '=', 'provinsi.id_prov')
                                ->join('kabupaten', 'kabupaten.id_kab', '=', 'kabupaten.id_kab')
                                ->where('tb_master_zona_wilayah.status','1')
                                ->select("provinsi.nama as nama_prov, REPLACE(REPLACE(kabupaten.nama, 'KAB. ', ''),'KOTA ','')   as nama_kab")
                                ->get();
                    
                    $rows_wilayah = DB::select(" SELECT  
                            b.nama as nama_prov, REPLACE (REPLACE(c.nama, 'KAB. ', ''),'KOTA ','')   as nama_kab  
                            from tb_master_zona_wilayah a, provinsi b, kabupaten c 
                            where a.status='1' and a.id_prov=b.id_prov and c.id_kab=a.id_kab
                    ");
                    $rows_tos=array();
                    if($param->id_user!="0"){
                        $rows_tos = DB::select(" SELECT  *,
                        ifnull((select distinct(id_user) as id_user from tb_log_tos x where x.id_tos=a.id and x.tipe_user=a.tipe_user and id_user='".$param->id_user."'),0) as id_user
                        from tb_master_tos a where  status='1' and tipe_user='".$param->tipe_user."' ");
                    }
        return array('master_data'=>$master_data,"rows_wilayah"=>$rows_wilayah,"rows_tos"=>$rows_tos,"rows_about"=>$rows_about,"cek_update"=>$rows_update,"row_slide"=>$rows_slide);
    }

    public static function insertBookingStatus($idBooking,$status){
        $result = DB::table("tb_master_data")->where('kategori','PESAN_STATUS_BOOKING_PELANGGAN')->where('subkategori',$status)->first();
        $respose = DB::table('tb_booking_logstatus')->insert([
            'id_booking'=>$idBooking,
            'status'=>$status,
            'description'=>@$result->value,
            'create_at'=>Carbon::now(),
        ]);
        return $respose;
    } 

    public static function isValidTokenBca($access_token){
        $data = DB::table('tb_log_login')->where([['tipe_user','=','GetTokenVABCA'],['status','=','Y'],['token_login','=',$access_token]]);
        if($data->exists()){
            return  true;
        }else{
            return false;
        }
    } 




    


}
