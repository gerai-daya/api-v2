<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainKonfigModel extends Model
{
    protected $table='main_konfig';
    protected $fillable = [
        'id_konfig',
        'nama',
        'value'
    ];
}
