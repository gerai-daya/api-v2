<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\TokenRequest;
use Carbon\Carbon;

class PPOBModel extends Model
{

    public static function getAllDenom(){
        $response = DB::table('tb_master_denom')
        ->where('status',"Y")
        ->where([ ['main_price',">","0"]])
        ->get();
        return  $response;
    }
    
    public static function getAllDenomByCategory($param){

        if($param->kategori=="PULSA"||$param->kategori=="DATA"){
            $value = array('PULSA','DATA');
        }else{
            $value = array($param->kategori);
        }
        $response = DB::table('tb_master_denom')
        ->where('status',"Y")
        ->where('main_price',">","0")
        ->whereIn('category',$value)
        ->orderBy('id_denom','ASC')
        ->get();

       
        
        return  $response;
    }

    public static function getAllProvider(){
        $response = DB::table('tb_master_data')
        ->where('kategori','PROVIDER')
        ->where('status','Y')
        ->get();
        return  $response;
    }
    
    public static function getNoFavorit($param){
        $response = DB::table('tb_master_nofav')
        ->where('id_user',$param->id_user)
        ->where('tipe_user',$param->tipe_user)
        ->where('jenis',$param->jenis)
        ->get();
        return  $response;
    }
    
    public static function getInqueryPPOB($param){
        $response = DB::table('tb_master_nofav')
        ->where('id_user',$param->id_user)
        ->where('tipe_user',$param->tipe_user)
        ->get();
        return  $response;
    } 
    
    public static function managNoFavorit($param){
        try {
            if($param->action=="delete"){
                DB::table("tb_master_nofav")->where('id',$param->id)->delete();
                $dataFav = self::getNoFavorit($param);
                return showOutputResponse($param->Func,true,'Proses hapus data berhasil',$dataFav,200); 
            }else{
                $response = DB::table('tb_master_nofav')
                ->where('id_user',$param->id_user)
                ->where('tipe_user',$param->tipe_user)
                ->where('jenis',$param->jenis)
                ->where('nomor',$param->number);

                if($response->doesntExist()){
                    DB::table('tb_master_nofav')->insert([
                        'id_user'=>$param->id_user,
                        'tipe_user'=>$param->tipe_user,
                        'jenis'=>$param->jenis,
                        'nomor'=>$param->number,
                        'nama_nomor'=>$param->name,
                        'created_at'=>Carbon::now(),
                    ]);
                    return showOutputResponse($param->Func,true,'Proses penambahan data berhasil',array(),200); 
                }else{
                    $dataUser = $response->first();
                    DB::table('tb_master_nofav')->where('id',$dataUser->id)->update(['updated_at'=>Carbon::now(),'nomor'=>$param->number,'nama_nomor'=>$param->name]);
                    return showOutputResponse($param->Func,true,'Proses perubahan data berhasil',array(),200); 
                }

            }
        } catch (\Throwable $th) {
            return showOutputResponse($param->Func,false,'Mohon maaf, proses update mengalami gangguan. Coba beberapa saat lagi',array(),200); 
        }
    }
    

    // params.put("Func", "addTransaksiPPOB");
    // params.put("id_user", mdDenom.getSell_price());
    // params.put("price", mdUser.getIdMysql());
    // params.put("number", tvPhone.getText().toString());
    // params.put("denom", mdDenom.getValue());
    // params.put("jenis_ppob", mdUser.getIdMysql());

    public static function addPPOBLog($param){
        try {
            // id_user	order_id	price	message	status_request status_request	
            // status_final status akhir	raw_response	phone_number
            // phone_number_extend	denom_code	type created_by	created_at	changed_at	changed_by	unique_id
            DB::table('tb_master_nofav')->insert([
                'id_user'=>$param->id_user,
                'order_id'=>$param->order_id,
                'price'=>$param->price,
                'phone_number'=>$param->nomor,
                'phone_number_extend'=>$param->nomor,
                'denom_code'=>$param->nomor,
                'type'=>$param->nomor,
                'status_request'=>$param->number,
                'status_final'=>"",
                'raw_response'=>$param->name,
                'unique_id'=>$param->unique_id,
                'created_by'=>$param->who,
                'created_at'=>Carbon::now(),
            ]);

        } catch (\Throwable $th) {
            return showOutputResponse($param->Func,false,'Mohon maaf, proses update mengalami gangguan. Coba beberapa saat lagi',array(),200); 
        }
    }

}
