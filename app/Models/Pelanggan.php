<?php

namespace App\Models;

// use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable;

use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Support\Facades\DB;
use App\Models\GeneralModel;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\TokenRequest;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;


class Pelanggan extends Model implements Authenticatable
{
    use AuthenticableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='tb_pelanggan';
    protected $fillable = [
        'id_pelanggan','username','password','nama','alamat','hp','email','latitude','longitude','imei','status','foto','gcm_code'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public static function getPelangganById($id){
        $user = DB::table((new static)->getTable())
        ->select(DB::raw("id_pelanggan,username,password,nama,alamat,hp,email,latitude,longitude,imei,status,gcm_code, ifnull(concat('".config('app.url_foto_profile')."',foto),'".config('app.url_nofoto')."') as foto, ifnull(foto,'') as foto_ori "))
        ->where('id_pelanggan',$id)->first();
        return $user;
    } 
    
    public static function getPelangganByUsername($username){
        $user = DB::table((new static)->getTable())->where('email',$username)->first();
        return $user;
    } 
     

    public static function loginAccount($param){
        if($param->tipe_user=="PELANGGAN"){
            $resUser = Pelanggan::where([['email','=',$param->username],['status','!=','D']])
            ->select(DB::raw("id_pelanggan,username,password,nama,alamat,hp,email,latitude,longitude,imei,status,gcm_code, ifnull(concat('".config('app.url_foto_profile')."',foto),'".config('app.url_nofoto')."') as foto"))
            ->first();
        }else{
            $resUser = Petugas::where([ ['username','=',$param->username],['status_akun','!=','D'] ])
            ->select(DB::raw("id_petugas,id_perusahaan,username,password,nama,no_ktp,alamat,hp,email,latitude,longitude,imei,tipe_user,status_kerja  status_akun, ifnull(concat('".config('app.url_foto_profile')."',foto),'".config('app.url_nofoto')."') as foto,berkas_sertifikasi,status_online,gcm_code"))
            ->first();
        }
        
        if($resUser){
            $dataPetugas = array();
            $token_login = Crypt::encrypt(Carbon::now()."#".rand(100000,999999)."#".$resUser->username);

            if($param->tipe_user=="PELANGGAN"){
                if( (md5($param->password)==$resUser->password) || Hash::check($param->password, $resUser->password) ){
                    GeneralModel::insertLogLogin($resUser->id_pelanggan,$param->tipe_user,$token_login);
                    GeneralModel::handleGCMCode($param,$resUser->id_pelanggan);
                    Pelanggan::where('id_pelanggan',$resUser->id_pelanggan)->update(['gcm_code'=>$param->gcm_code]);

                    $data = array(
                            'data_user'=>$resUser,
                            'konfig'=>array(
                                'token_login'=>$token_login,
                                'TREAD_GPS_PEL'=>10,
                                'TREAD_GPS_PTG'=>10,
                            ),
                        );
                    return  showOutputResponse($param->Func,true,'Berhasil Login!',$data,200);
                }else{
                    return  showOutputResponse($param->Func,false,'Password yang Anda masukkan salah, mohon periksa kembali','-',200);
                }

            }else{

                if( (md5($param->password)==$resUser->password) || Hash::check($param->password, $resUser->password) ){
                    GeneralModel::insertLogLogin($resUser->id_petugas,$param->tipe_user,$token_login);
                    Petugas::where('id_petugas',$resUser->id_petugas)->update(['status_online'=>'ONLINE','gcm_code'=>$param->gcm_code]);
    
                    $homePetugas = Booking::getHomePelanggan($resUser->id_petugas);
                    $dataOnprogress = Booking::getOnprogressPetugas($resUser->id_petugas);
                    $dataLastWork = Booking::getLastWorkPetugas($resUser->id_petugas);
                    $resUser = Petugas::getPetugasById($resUser->id_petugas);
                    GeneralModel::handleGCMCode($param,$resUser->id_petugas);

                    $dataLayananPetugas = array();
                    $datadummy = array();
                    foreach ($dataOnprogress as $i => $rows) {
                        $prm = (object)array('id_booking'=>$rows->id_booking);
                        $dataBooking = Booking::getDataBooking($prm);
                        $dataBookingDetail = Booking::getDataBookingDetail($prm);
                        $dataBookingStatus = Booking::getDataBookingStatus($prm);
                        if(in_array($dataBooking[0]->status_order,array("SURVEY_SELESAI","DITERIMA","DIKERJAKAN") )==1){
                            $dataLayananPetugas = Booking::getLayananPetugas($dataBooking[0]->id_petugas,$dataBooking[0]->kode_layanan);
                        }
                        $dataOnprogress[$i]->data = array('rows_status'=>$dataBookingStatus,'rows_utama'=>$dataBooking,'rows_detail'=>$dataBookingDetail,'rows_pekerjaan_petugas'=>$dataLayananPetugas);
                    }

                    $data = array(
                        'data_user'=>$resUser,
                        'row_home_petugas'=>$homePetugas,
                        'rows_progress'=>$dataOnprogress,
                        'rows_aktivitas'=>$dataLastWork,
                        'konfig'=>array(
                                    'token_login'=>$token_login,
                                    'TREAD_GPS_PEL'=>10,
                                    'TREAD_GPS_PTG'=>10,
                                ),
                    );
                        
                    return  showOutputResponse($param->Func,true,'Berhasil Login!',$data,200);
                }else{
                    return  showOutputResponse($param->Func,false,'Password yang Anda masukkan salah, mohon periksa kembali','-',200); 
                }
            } 
        }else{
            return  showOutputResponse($param->Func,false,'Username / email yang Anda berikan salah, mohon periksa kembali','',200);
        }
    }
    
    public static function logoutAccount($param){
        if($param->tipe_user=="PETUGAS"){
            DB::table('tb_petugas')->where('id_petugas',$param->id)->update(['status_online'=>'OFFLINE']);
        }
        DB::table('tb_log_login')->where([['id_user','=', $param->id],['status','=', 'Y']])->update(['status' => 'N','updated_at' => Carbon::now()]);

        return  showOutputResponse($param->Func,true,'Logout Berhasil',"",200);
    }
    
    public static function setOnlineOffline($param){
        DB::table('tb_petugas')->where('id_petugas',$param->id)->update(['status_online'=>$param->status]);
        $resUser = Petugas::where('id_petugas',$param->id)->first();
        return  showOutputResponse($param->Func,true,'Perubahan status berhasil', array('data_user'=>$resUser),200);
    }

    public static function registerAccount($param,$date){
        
        $userIsExist = Pelanggan::where('email', $param->email)->doesntExist();
        if($userIsExist){
            try {
                $lastIdPelanggan = Pelanggan::create([
                    'username'=>$param->email,
                    'password'=>Hash::make($param->password),
                    'nama'=>$param->nama,
                    'alamat'=>$param->alamat,
                    'hp'=>$param->hp,
                    'email'=>$param->email,
                    'imei'=>$param->imei,
                    'created_at'=>Carbon::now()
                ]);

                GeneralModel::insertLogTos($lastIdPelanggan->id,'0');
                GeneralModel::closeOTPId($lastIdPelanggan->id);
    
                if($lastIdPelanggan->id>0){
                    $kodeOTP =generateOtp();
                    $noRef   =generateNoRef();
                    $dataOtp = array(
                        'kode_otp'=>$kodeOTP,
                        'no_ref'=>$noRef,
                        'id_pelanggan'=>$lastIdPelanggan->id,
                        'expired_date'=>Carbon::now()->addHour(),
                        'request_date'=>Carbon::now()
                    );
                    $responOtp = GeneralModel::insertOTP((object)$dataOtp);
                    
                    $pesan="TOKEN GERAI DAYA\nNo. Referensi: ".$noRef."\nOTP:".$kodeOTP."\nRAHASIA! jangan diberikan kpd pihak lain";
                    sendSMS($param->hp,$pesan,"PREMIUM");// global function
    
                    $dataEmail = array(
                        'to'=>$param->email,
                        'subject'=>"OTP Registrasi Akun Baru",
                        'message'=>"Berikut informasi Token dengan Nomor Referensi Adalah <b>".$noRef."</b> dan 6 Digit kode OTP Anda ada dibawah ini",
                        'name'=>strtoupper($param->nama),
                        'otp'=>$kodeOTP,
                        'isbanyak'=>false
                    );
                    sendEMAIL($dataEmail);

                    $responOtp = GeneralModel::getDataOtp($responOtp);
                    return  showOutputResponse($param->Func,true,'Selamat, Pendaftaran Berhasil. Kode OTP sudah dikirim via SMS dan EMAIL utuk aktivasi akun Anda!',$responOtp,200);

                }else{
                    return  showOutputResponse($param->Func,false,'Pendaftaran Gagal, Mohon coba lagi','-',200);
                }
                    
            } catch (\Exception $e) {
                return  showOutputResponse($param->Func,false,'Pendaftaran Gagal, Terdapat gangguan pada server. Mohon coba beberapa saat lagi','-',200);

            }
            
        }else{
            return  showOutputResponse($param->Func,false,'Username / Email Yang Anda Ajukan Sudah Terdaftar. Coba Menggunakan email lain',$userIsExist,200);
        }
    }

    public static function AktivasiAkun($param){
        $responseOtp = GeneralModel::getDataOtpIsExist($param);
        if($responseOtp){
            if(!isExpiredOTP($responseOtp->expired_date)){
                DB::table((new static)->getTable())->where('id_pelanggan',$param->id_pelanggan)->update(['status'=>'Y']);
                GeneralModel::usedOTP($param->token,$param->noref);
                $user = self::getPelangganById($param->id_pelanggan);
    
                $dataEmail = array(
                    'to'=>$user->email,
                    'subject'=>"OTP Registrasi Akun Baru",
                    'message'=>"<b>SELAMAT</b>! Akun Anda berhasil aktivasi. Kami mengucapkan terimakasih kepada Anda karena atas ketersediaan Anda bisa bergabung dengan <b>Gerai Daya!</b>",
                    'name'=>strtoupper($user->nama),
                    'otp'=>"",
                    'isbanyak'=>false
                );
                sendEMAIL($dataEmail);
                return  showOutputResponse($param->Func,true,'Proses Aktivasi Berhasil, Silahkan Login','-',200); 
            }else{
                return  showOutputResponse($param->Func,false,'Maaf, Kode OTP Anda sudah kadaluarsa pada '.date("d-m-Y H:i", strtotime($responseOtp->expired_date)),'-',200); 
            }
        }else{
            return  showOutputResponse($param->Func,false,'Kode TOKEN yang anda input tidak COCOK!','-',200); 
            
        }
    }

    public static function managDataPelanggan($param){

        try{
            if($param->mode_update=='password'){
                $res = DB::table((new static)->getTable())->where('id_pelanggan',$param->id_pelanggan)->update(['password'=>Hash::make($param->password),'updated_at'=>Carbon::now()]);
                DB::table('tb_log_login')->where([['id_user','=', $param->id_pelanggan],['status','=', 'Y']])->update(['status' => 'N','updated_at' => Carbon::now()]);

            }else if($param->mode_update=='email'){
                $res = DB::table((new static)->getTable())->where('id_pelanggan',$param->id_pelanggan)->update(['email'=>$param->email,'updated_at'=>Carbon::now()]);
            }else{
                $res = DB::table((new static)->getTable())->where('id_pelanggan',$param->id_pelanggan)->update(['hp'=>$param->hp,'updated_at'=>Carbon::now()]);
            }

            $user = self::getPelangganById($param->id_pelanggan);
            $dataEmail = array(
                'to'=>$user->email,
                'subject'=>"Pembaharuan ".$param->mode_update,
                'message'=>"<b>SELAMAT</b>! ".$param->mode_update." Anda sudah di perbaharui.",
                'name'=>strtoupper($user->nama),
                'otp'=>"",
                'isbanyak'=>false
            );
            sendEMAIL($dataEmail);
             return  showOutputResponse($param->Func,true,'Proses pembaharuan data '.$param->mode_update.' berhasil','-',200);
        } catch (\Exception $e) {
            return  showOutputResponse($param->Func,false,'Proses pembaharuan gagal, mohon coba beberapa saat lagi','-',200);   

        }
    }

    public static function updatePassword($param){
        $responseOtp = GeneralModel::getDataOtpByOtpNoref($param);
        if($responseOtp){
            if(!isExpiredOTP($responseOtp->expired_date)){
                if($param->tipe_user=="PELANGGAN"){
                    $res = DB::table((new static)->getTable())->where('id_pelanggan',$param->id_user)->update(['password'=>Hash::make($param->password),'updated_at'=>Carbon::now()]);
                    $user = self::getPelangganById($param->id_user);
                }else{
                    $res = Petugas::where('id_petugas',$param->id_user)->update(['password'=>Hash::make($param->password),'updated_at'=>Carbon::now()]);
                    $user = Petugas::where('id_petugas',$param->id_user)->first();
                }
                
                GeneralModel::usedOTP($param->token,$param->noref);
                
    
                $dataEmail = array(
                    'to'=>$user->email,
                    'subject'=>"Pembaharuan Password",
                    'message'=>"<b>SELAMAT</b>! password Anda sudah di perbaharui.",
                    'name'=>strtoupper($user->nama),
                    'otp'=>"",
                    'isbanyak'=>false
                );
                sendEMAIL($dataEmail);
                return  showOutputResponse($param->Func,true,'Proses perubahan password berhasil, silakan login ulang',array(),200); 
            }else{
                return  showOutputResponse($param->Func,false,'Maaf, Kode OTP Anda sudah kadaluarsa pada '.date("d-m-Y H:i", strtotime($responseOtp->expired_date)),'-',200); 
            }
        }else{
            return  showOutputResponse($param->Func,false,'Kode TOKEN yang anda input tidak COCOK!',array(),200); 
            
        }
    }
    
    
    public static function requestResetPassword($param){
        if($param->tipe_user=='PELANGGAN'){
            $user = self::getPelangganByUsername($param->username);
            $id_user = @$user->id_pelanggan;
        }else{
            $user = Petugas::where('email',$param->username)->first();
            $id_user = @$user->id_petugas;
        }

        if($user){
            GeneralModel::closeOTPId($id_user);
            $kodeOTP =generateOtp();
            $noRef   =generateNoRef();
            $dataOtp = array(
                'kode_otp'=>$kodeOTP,
                'no_ref'=>$noRef,
                'id_pelanggan'=>$id_user,
                'expired_date'=>Carbon::now()->addHour(),
                'request_date'=>Carbon::now()
            );
            $responOtp = GeneralModel::insertOTP((object)$dataOtp);
            
            
            $pesan="OTP Ganti Password GERAI DAYA\nNo. Referensi: ".$noRef."\nOTP:".$kodeOTP."\nRAHASIA! jangan diberikan kpd pihak lain";
            sendSMS($user->hp,$pesan,"PREMIUM");// global function

            $dataEmail = array(
                'to'=>$user->email,
                'subject'=>"OTP Ganti Password",
                'message'=>"Berikut informasi Token dengan Nomor Referensi Adalah <b>".$noRef."</b> dan 6 Digit kode OTP Anda ada dibawah ini",
                'name'=>strtoupper($user->nama),
                'otp'=>$kodeOTP,
                'isbanyak'=>false
            );
            sendEMAIL($dataEmail);

            $responOtp = GeneralModel::getDataOtp($responOtp);
            // $dataUser = array('hp'=>$user->hp,'id_user'=>$id_user);
            return  showOutputResponse($param->Func,true,'Permintaan Kode OTP sudah dikirim via SMS dan EMAIL. Silakan cek!',array('data_otp'=>$responOtp,'hp'=>$user->hp),200);
        }else{
            return  showOutputResponse($param->Func,false,'Data akun Anda tidak ditemukan, mohon cek kembali email yang Anda inpu',array(),200);
        }
    }
    
    public static function getNewToken($param){

        if($param->tipe_user=='PELANGGAN'){
            $user = self::getPelangganById($param->id_user);
            $id_user = @$user->id_pelanggan;
        }else{
            $user = Petugas::where('id_petugas',$param->id_user)->first();
            $id_user = @$user->id_petugas;
        }

        if($user){
            GeneralModel::closeOTPId($id_user);
            $kodeOTP =generateOtp();
            $noRef   =generateNoRef();
            $dataOtp = array(
                'kode_otp'=>$kodeOTP,
                'no_ref'=>$noRef,
                'id_pelanggan'=>$id_user,
                'expired_date'=>Carbon::now()->addHour(),
                'request_date'=>Carbon::now()
            );
            $responOtp = GeneralModel::insertOTP((object)$dataOtp);
            
            
            $pesan="OTP GERAI DAYA\nNo. Referensi: ".$noRef."\nOTP:".$kodeOTP."\nRAHASIA! jangan diberikan kpd pihak lain";
            sendSMS($user->hp,$pesan,"PREMIUM");// global function

            $dataEmail = array(
                'to'=>$user->email,
                'subject'=>"OTP Ganti Password",
                'message'=>"Berikut informasi Token dengan Nomor Referensi Adalah <b>".$noRef."</b> dan 6 Digit kode OTP Anda ada dibawah ini",
                'name'=>strtoupper($user->nama),
                'otp'=>$kodeOTP,
                'isbanyak'=>false
            );
            sendEMAIL($dataEmail);

            $responOtp = GeneralModel::getDataOtp($responOtp);
            return  showOutputResponse($param->Func,true,'Permintaan Kode OTP sudah dikirim via SMS dan EMAIL. Silakan cek!',$responOtp,200);
        }else{
            return  showOutputResponse($param->Func,false,'Data akun Anda tidak ditemukan, mohon cek kembali email yang Anda inpu',array(),200);
        }
    }
    
    

    
    
}
