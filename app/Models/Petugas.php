<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class Petugas extends Model
{
    protected $table='tb_petugas';
    protected $fillable = [
        'id_petugas','id_perusahaan','username','password','nama','no_ktp','alamat','hp','email','latitude','longitude','imei','tipe_user','status_kerja','status_akun','foto','berkas_sertifikasi','status_online','gcm_code','created_at','updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];


    public static function getPetugasById($id){
        $user = DB::table((new static)->getTable())
        ->select(DB::raw("id_petugas,id_perusahaan,username,password,nama,no_ktp,alamat,hp,email,latitude,longitude,imei,tipe_user,status_kerja,status_akun, ifnull(concat('".config('app.url_foto_profile')."',foto),'".config('app.url_nofoto')."') as foto,ifnull(foto,'') as foto_ori, berkas_sertifikasi,status_online,gcm_code"))
        ->where('id_petugas',$id)->first();
        return $user;
    } 

    public static function getPetugasByUsername($username){
        $user = DB::table((new static)->getTable())->where('username',$username)->first();
        return $user;
    }

    public static function managDataPetugas($param){
        try{
            $status=true;
            if($param->mode_update=='password'){
                $res = DB::table((new static)->getTable())->where('id_petugas',$param->id_user)->update(['password'=>Hash::make($param->password),'updated_at'=>Carbon::now()]);
                DB::table('tb_log_login')->where([['id_user','=', $param->id_user],['status','=', 'Y']])->update(['status' => 'N','updated_at' => Carbon::now()]);
            }else if($param->mode_update=='email'){
                $res = DB::table((new static)->getTable())->where('id_petugas',$param->id_user)->update(['email'=>$param->email,'updated_at'=>Carbon::now()]);
            }else if($param->mode_update=='hp'){
                $res = DB::table((new static)->getTable())->where('id_petugas',$param->id_user)->update(['hp'=>$param->hp,'updated_at'=>Carbon::now()]);
            } 

            $data = self::getPetugasById($param->id_user);
            if($param->mode_update!='foto'){
                $dataEmail = (object)array(
                    'to'=>$data->email,
                    'subject'=>"Pembaharuan ".$param->mode_update,
                    'message'=>"<b>SELAMAT</b>! ".$param->mode_update." Anda sudah di perbaharui.",
                    'name'=>strtoupper($data->nama),
                    'otp'=>"",
                    'isbanyak'=>false
                );
                sendEMAIL($dataEmail);
            }
            return  showOutputResponse($param->Func,true,'Proses pembaharuan data '.$param->mode_update.' Berhasil',array('data_user'=>$data),200);
        } catch (\Exception $e) {
            GeneralModel::insertLogRequest($param->Func."-ERROR",'OUT',$e,'PetugasModel');
            return  showOutputResponse($param->Func,false,'Proses pembaharuan gagal, mohon coba beberapa saat lagi','-',200);   

        }
        
}

    
}
