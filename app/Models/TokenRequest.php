<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokenRequest extends Model
{
    protected $table='tb_token_request';
    protected $fillable = [
        'kode_token',
        'no_ref',
        'request_date',
        'expired_date',
        'used_date',
        'status',
        'id_pelanggan',
        'created_at',
        'updated_at',
    ];

}
