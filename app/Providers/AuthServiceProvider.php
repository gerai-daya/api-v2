<?php

namespace App\Providers;

use App\Models\Pelanggan;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

            // $this->app['auth']->viaRequest('api', function ($request) {
            //     // Return User or null...
            // });

            $this->app['auth']->viaRequest('api', function ($request) {
                if ($request->header('Authorization')) {
                    $this->request = $request->getContent();
                    $this->params = json_decode($this->request);
                    
                        $key = explode(' ',$request->header('Authorization'));
                        $data = DB::table('tb_log_login')->where([['status','=','Y'],['token_login','=',@$key[1]]]);
                        if(!$data->exists()){
                            return  showOutputResponse(@$this->params->Func,false,'You Unauthorized!','',401);
                        }
                        if($data->exists()){
                            $data_user = $data->first();
                            $today = date("Y-m-d") ;
                            $dateExpired = date('Y-m-d', strtotime($data_user->created_at . ' +7 day'));
                            if($today>$dateExpired){
                                $request->request->add(['status'=>false]);
                                $data = DB::table('tb_log_login')->where('id',$data_user->id)->update(['status'=>'N','updated_at'=>Carbon::now()]);
                            }else{
                                DB::table('tb_log_login')->where('id',$data_user->id)->update(['updated_at'=>Carbon::now()]);
                                $request->request->add(['status'=>true,'data_user' => $data->first()]);
                            }
                        }else{
                            $request->request->add(['status'=>false]);
                        }
                    return $request;
                }
            });

    }
}
