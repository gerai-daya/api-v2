<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Lumen'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services the application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => (bool) env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),
    'url_dashboard' => env('APP_URL_DASHBOARD', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'Asia/Makassar',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => env('APP_LOCALE', 'en'),

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => env('APP_FALLBACK_LOCALE', 'en'),

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',
    'path_bukti_deposit' => '../../../dashboard.geraidaya.id/file_upload/bukti_transfer/',
    'path_foto_profile' => '../../../dashboard.geraidaya.id/file_upload/dp/',
    'path_booking' => '../../../dashboard.geraidaya.id/file_upload/booking/',
    
    'url_bukti_deposit' => env('APP_URL_DASHBOARD', 'http://localhost').'file_upload/bukti_transfer/',
    'url_booking' => env('APP_URL_DASHBOARD', 'http://localhost').'file_upload/booking/',
    'url_foto_profile' => env('APP_URL_DASHBOARD', 'http://localhost').'file_upload/dp/',
    'url_nofoto' => env('APP_URL_DASHBOARD', 'http://localhost').'file_upload/nophoto.png',
    'url_detail_publik' => env('APP_URL_DASHBOARD', 'http://localhost').'detail_transaksi_publik/',

    'FIREBASE_KEY' => 'AAAAeMjTFY4:APA91bFU74yhLfClEIDxOGJAGj2yqPOoKgSJfOtLAt_mnM9GQBFaD6U_BZWJyJ1lNVUCeZrQ93SywCcFuKsBIPfLmRKsKXvhgU4XBmvK8jUVpXigtyN-2yYaOj_BkoZWWz7nvh7XPxuJ',
    'FIREBASE_URL' => 'https://fcm.googleapis.com/fcm/send',

    'MAILJET_API_KEY' => '86c813c35fdce720e6d4bd0ba51ddb55',
    'MAILJET_API_SECRET' => '44f2f305a4df3ce2e3435a208fd8d61f',
    'MAILJET_URL' => 'https://api.mailjet.com/v3.1/send',
    'MAIL_SENDER' => 'info@geraidaya.id',
    'MAIL_SENDER_NAME' => 'Gerai Daya',

    'IS_EMAIL_LIVE' => true,
    'IS_SMS_LIVE' => true,
    // BCA CONFIG

    'CorporateId' 			=> "KBBGERAIDA",
    'BCAApiKey' 			=> "1b993bee-3743-4e09-86e5-39f6c8a335a3",
    'BCAApiSecret' 		    => "d6f94f56-8332-4d80-86fd-9dee234949d7",
    'ClientId' 		        => "943a0bb1-1118-43be-842e-2e651526db9b",
    'ClientSecret' 		    => "09ef4e14-52ee-45f7-84df-5778b7ee5127",
    'UrlApi' 			    => "https://api.klikbca.com:443",
    'AccountNumber' 		=> "7725534108",
    'AccountName' 	        => "PT. Gerai Daya Indonesia",
    'BCACompanyCode' 	    => "13784",
    'ProductName'    	    => "GERAI DAYA",


    'CorporateId-uat' 			=> "UATCORP001",
    'BCAApiKey-uat' 			=> "a16c5bb4-49d1-4a12-9194-db3df367d893",
    'BCAApiSecret-uat' 		    => "2ad77de8-7f0e-4379-bce5-71d70529a611",
    'ClientId-uat' 		        => "e305a76a-78d3-4f92-b734-c23ae58c97d8",
    'ClientSecret-uat' 		    => "04031743-9645-4bc7-84e5-c8943618c2c8",
    'UrlApi-uat' 			    => "https://devapi.klikbca.com:9443",
    'AccountNumber-uat' 		=> "0613332607",
    'AccountName-uat' 	        => "PERUSAHAAN 2",
    'BCACompanyCode-uat' 	    => "13784",
    'ProductName-uat'    	    => "GERAI DAYA",



];
