<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_user', function (Blueprint $table) {
            $table->bigIncrements('id_user');
            $table->string('username');
            $table->string('password');
            $table->string('status_akun');
            $table->string('token')->nullable();
            $table->string('firebase_id')->nullable();
            $table->string('imei');
            $table->string('foto')->nullable();
            $table->string('type_user');
            $table->string('password_change_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_user');
    }
}
