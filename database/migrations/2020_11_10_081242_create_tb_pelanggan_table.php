<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbPelangganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pelanggan', function (Blueprint $table) {
            $table->bigIncrements('id_pelanggan');
            $table->string ('username',50)->nullable();
            $table->string ('password',255)->nullable();
            $table->string ('nama',30)->nullable();
            $table->string ('alamat',255)->nullable();
            $table->string ('hp',30)->nullable();
            $table->string ('email',30)->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->string ('imei',20)->default('0');
            $table->string ('status',2)->default('N');
            $table->string ('foto',50)->default('nophoto.png');
            $table->string ('gcm_code',500)->default('0');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pelanggan');
    }
}
