<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbPetugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_petugas', function (Blueprint $table) {
            $table->bigIncrements('id_petugas');
            $table->integer('id_perusahaan')->nullable();
            $table->string ('username',100)->nullable();
            $table->string ('password',50)->nullable();
            $table->string ('nama',30)->nullable();
            $table->string ('no_ktp',30)->nullable();
            $table->string ('alamat')->nullable();
            $table->string ('hp',30)->nullable();
            $table->string ('email',30)->nullable();
            $table->double('latitude')->default('0');
            $table->double('longitude')->default('0');
            $table->string ('imei',20)->nullable();
            $table->string ('tipe_user',30)->default('PETUGAS');
            $table->string ('status_kerja',50)->default('FREE')->comment('FREE,WORK');
            $table->string ('status_akun',2)->default('N');
            $table->string ('foto',50)->default('nophoto.png');
            $table->string ('berkas_sertifikasi',50)->nullable();
            $table->string ('status_online',20)->default('OFFLINE')->comment('OFFLINE,ONLINE');
            $table->longText ('gcm_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_petugas');
    }
}
