<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_menu', function (Blueprint $table) {
            $table->bigIncrements('id_menu');
            $table->string('nama',30);
            $table->enum('level',['1','2'])->nullable();
            $table->integer('id_main')->default(0);
            $table->integer('hak_akses')->default(0);
            $table->string('icon',25)->nullable();
            $table->string('link',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_menu');
    }
}
