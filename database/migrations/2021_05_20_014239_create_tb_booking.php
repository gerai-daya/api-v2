<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_booking', function (Blueprint $table) {
            $table->bigIncrements('id_booking');
            $table->string('kode_booking',30)->nullable();
            $table->integer('id_pelanggan')->nullable();
            $table->integer('id_petugas')->nullable();
            $table->integer('id_perusahaan')->nullable();
            $table->dateTime('tgl_pesan')->nullable();
            $table->dateTime('tgl_rencana_survey')->nullable();
            $table->dateTime('tgl_rencana_dikerjakan')->nullable();
            $table->dateTime('tgl_dikerjakan')->nullable();
            $table->dateTime('tgl_selesai')->nullable();
            $table->string('alamat',255)->nullable();
            $table->string('petunjuk',500)->nullable();
            $table->string('keluhan',500)->nullable();
            $table->string('status_order',50)->nullable();
            $table->enum('aksi_cancel',['CANCEL_LANJUT','CANCEL_TIDAK_LANJUT','CLOSE'])->nullable();
            $table->string('solusi',500)->nullable();
            $table->string('alasan_penolakan')->nullable();
            $table->double('lat_pel')->nullable();
            $table->double('longi_pel')->nullable();
            $table->double('lat_petugas')->nullable();
            $table->double('longi_petugas')->nullable();
            $table->double('lat_progress_petugas')->nullable();
            $table->double('longi_progress_petugas')->nullable();
            $table->dateTime('time_progress')->nullable();
            $table->string('kategori_pekerjaan',100)->default('LISTRIK');
            $table->string('kode_layanan',50)->default('LISTRIK_01');
            $table->double('tagihan')->default('0');
            $table->double('diskon')->default('0');
            $table->double('biaya_survey')->default('0');
            $table->double('biaya_pembatalan')->default('0');
            $table->double('jml_rating')->default('0');
            $table->date('tgl_rating')->nullable();
            $table->string('masukan')->nullable();
            $table->enum('metode_pembayaran',['TUNAI', 'TRANSFER'])->default('TUNAI');
            $table->integer('id_persentase')->nullable();
            $table->string('id_booking_enc',50)->default('-');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_booking');
    }
}
