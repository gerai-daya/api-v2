<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbBookingDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_booking_detail', function (Blueprint $table) {
            $table->bigIncrements('id_booking_detail');
            $table->integer('id_booking')->nullable();
            $table->integer('id_pekerjaan_detail')->nullable();
            $table->double('harga')->nullable();
            $table->integer('qty')->nullable();
            $table->string('jenis',50)->nullable();
            $table->string('keterangan',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_booking_detail');
    }
}
