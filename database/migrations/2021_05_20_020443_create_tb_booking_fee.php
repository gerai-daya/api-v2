<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbBookingFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_booking_fee', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_booking');
            $table->double('total_tagihan');
            $table->double('fee_geraidaya')->comment('12% bruto');
            $table->double('fee_mitra')->comment('25% netto jasa');
            $table->double('fee_petugas')->comment('75% netto jasa');
            $table->double('fee_material')->default('0')->comment('full material mitra');
            $table->double('setoran')->nullable();
            $table->double('tarikan_fee_petugas')->nullable();
            $table->double('pajak_geraidaya')->default('0');
            $table->double('pajak_mitra')->default('0');
            $table->double('pajak_petugas')->default('0');
            $table->double('total_pajak')->default('0');
            $table->tinyInteger('status')->nullable();
            $table->string ('keterangan',255)->nullable();
            $table->string ('keterangan_tarikan',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_booking_fee');
    }
}
