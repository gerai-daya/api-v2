<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_deposit', function (Blueprint $table) {
            $table->bigIncrements('id_deposit');
            $table->integer('id_perusahaan')->nullable();
            $table->date('tgl_transaksi')->nullable();
            $table->double('debet')->nullable();
            $table->double('kredit')->nullable();
            $table->double('saldo')->nullable();
            $table->string ('kode_transaksi',50)->nullable()->comment('SETORAN,TARIKAN,DEPOSIT');
            $table->string ('keterangan',500)->nullable();
            $table->string ('status',50)->nullable()->comment('Y,N');
            $table->string ('jenis_deposit',50)->default('DEPOSIT')->comment('DEPOSIT,SURVEY');
            $table->string ('create_by',50)->nullable();
            $table->string ('update_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_deposit');
    }
}
