<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbGcmUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_gcm_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('gcm_code')->nullable();
            $table->string ('tipe_user',20)->nullable();
            $table->integer('id_relasi')->nullable();
            $table->string ('kode_imei',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_gcm_user');
    }
}
