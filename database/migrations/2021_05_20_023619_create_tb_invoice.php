<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_invoice', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_perusahaan');
            $table->string ('no_invoice',20);
            $table->double('jumlah');
            $table->date('tgl_invoice')->nullable();
            $table->date('tgl_jatuh_tempo')->nullable();
            $table->date('tgl_bayar')->nullable();
            $table->integer('status')->nullable()->comment('0:Belum Bayar, 1: Sudah Bayar');
            $table->integer('bulan')->nullable();
            $table->integer('tahun')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_invoice');
    }
}
