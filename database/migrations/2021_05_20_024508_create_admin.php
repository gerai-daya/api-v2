<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->bigIncrements('id_admin');
            $table->integer('id_perusahaan');
            $table->integer('id_provinsi')->nullable();
            $table->string('username',100)->nullable();
            $table->string('password',32)->nullable();
            $table->string('poto',32)->nullable();
            $table->string('nama_perusahaan')->nullable();
            $table->string('owner')->nullable();
            $table->integer('level')->nullable();
            $table->string('telp',50)->nullable();
            $table->string('alamat',100)->nullable();
            $table->string('email',50)->nullable();
            $table->enum('difficulty', ['easy', 'hard']);
            $table->enum('jenis_perusahaan', ['perusahaan','perorangan'])->comment('perusahaan,perorangan');
            $table->integer('id_parent')->nullable();
            $table->double('price')->default(20000);
            $table->double('saldo')->default(0);
            $table->dateTime('tgl')->nullable();
            $table->string('ip',20)->nullable();
            $table->integer('mode')->nullable();
            $table->enum('status', ['Y','N'])->comment('perusahaan,perorangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
