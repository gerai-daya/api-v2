<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbJenisPekerjaanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_jenis_pekerjaan_detail', function (Blueprint $table) {
            $table->bigIncrements('id_pekerjaan_detail');
            $table->integer('id_petugas')->default('0');
            $table->integer('id_master_data')->default('0');
            $table->double('harga_pelanggan')->default('0');
            $table->double('fee_pln')->default('0');
            $table->double('fee_vendor')->default('0');
            $table->string('jenis',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_jenis_pekerjaan_detail');
    }
}
