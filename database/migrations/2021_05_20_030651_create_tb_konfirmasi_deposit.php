<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbKonfirmasiDeposit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_konfirmasi_deposit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_deposit')->nullable();
            $table->integer('id_bank')->nullable();
            $table->integer('id_perusahaan')->nullable();
            $table->double('jml_deposit')->nullable();
            $table->string ('atas_nama_pembayar',50)->nullable();
            $table->string ('bank_pembayar',50)->nullable()->comment('bri,bca,bni,dll');
            $table->double('jml_transfer')->nullable();
            $table->date('tgl_transfer')->nullable();
            $table->dateTime ('tgl_upload')->nullable();
            $table->string ('url_foto',50)->default('nophoto.png');
            $table->enum('status',['approve','rejected','pending','review'])->nullable()->comment('menunggu,diterima,ditolak');
            $table->string ('create_by',50)->nullable();
            $table->string ('update_by',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_konfirmasi_deposit');
    }
}
