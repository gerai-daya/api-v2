<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbKonfirmasiPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_konfirmasi_pembayaran', function (Blueprint $table) {
            $table->bigIncrements('id_konfirmasi');
            $table->integer('id_bank')->nullable();
            $table->integer('id_pembayaran')->nullable();
            $table->integer('id_pelanggan')->nullable();
            $table->integer('id_booking')->nullable();
            $table->string ('atas_nama_pembayar',50)->nullable();
            $table->string ('bank_pembayar',50)->nullable()->comment('bri,bca,bni,dll');
            $table->double('jml_transfer')->nullable();
            $table->date('tgl_transfer')->nullable();
            $table->dateTime ('tgl_upload')->nullable();
            $table->string ('url_foto',50)->default('nophoto.png');
            $table->enum('status',['menungg','diterima','ditolak'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_konfirmasi_pembayaran');
    }
}
