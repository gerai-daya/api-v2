<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbLogLokasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_log_lokasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_petugas')->nullable(); 
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->dateTime ('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_log_lokasi');
    }
}
