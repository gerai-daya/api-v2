<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbLogRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_log_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string ('name',50)->nullable();
            $table->string ('type',3)->nullable()->default('IN');
            $table->longText('param');
            $table->string ('location',50)->default('-');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_log_request');
    }
}
