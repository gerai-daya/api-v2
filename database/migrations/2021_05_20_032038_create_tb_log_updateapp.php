<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbLogUpdateapp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_log_updateapp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('version_code')->nullable();
            $table->string ('version_name',255)->nullable();
            $table->string ('desc',255)->nullable();
            $table->enum('status',['0','1'])->default('1');
            $table->enum('tipe_user',['0','1'])->default('0')->comment('0:Pelanggan, 1: Petugas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_log_updateapp');
    }
}
