<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMasterData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_master_data', function (Blueprint $table) {
            $table->bigIncrements('id_master_data');
            $table->string ('kategori',40)->default('-');
            $table->string ('subkategori',40)->default('-');
            $table->string ('value',255)->default('-');
            $table->string ('deskripsi',255)->default('-');
            $table->string ('status',5)->default('Y')->comment('Y,N');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_master_data');
    }
}
