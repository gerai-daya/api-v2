<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMasterHargaMaterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_master_harga_material', function (Blueprint $table) {
            $table->bigIncrements('id_harga_material');
            $table->integer('id_material') ;
            $table->integer('id_zona')->default('0');
            $table->double('harga_material')->default('0');
            $table->tinyInteger('status')->default('1')->comment('0:Tidak Aktif, 1: Aktif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_master_harga_material');
    }
}
