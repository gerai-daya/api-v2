<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMasterJasa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_master_jasa', function (Blueprint $table) {
            $table->bigIncrements('id_jasa');
            $table->string('nama_jasa',100);
            $table->string('ket');
            $table->integer('id_usaha');
            $table->integer('id_layanan') ;
            $table->enum('status',['0','1'])->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_master_jasa');
    }
}
