<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMasterKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_master_kategori', function (Blueprint $table) {
            $table->bigIncrements('id_kategori');
            $table->string ('nama_kategori',100);
            $table->string ('ket',100);
            $table->tinyInteger('tipe')->default('0')->comment('0: Material, 1: Jasa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_master_kategori');
    }
}
