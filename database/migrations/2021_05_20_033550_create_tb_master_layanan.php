<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMasterLayanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_master_layanan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_usaha'); 
            $table->string ('nama_layanan',50);
            $table->tinyInteger('status')->default('0')->comment('0: Aktif, 1: Tidak Aktif');
            $table->string ('kode_layanan',100);
            $table->string ('subnama',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_master_layanan');
    }
}
