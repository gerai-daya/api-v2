<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMasterMaterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_master_material', function (Blueprint $table) {
            $table->bigIncrements('id_material');
            $table->string ('nama_material',100); 
            $table->integer('id_kategori'); 
            $table->integer('id_brand'); 
            $table->integer('id_jenis_material'); 
            $table->integer('id_usaha'); 
            $table->string ('satuan',100)->nullable();
            $table->string ('ket',100)->nullable();
            $table->enum('status',['0','1'])->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_master_material');
    }
}
