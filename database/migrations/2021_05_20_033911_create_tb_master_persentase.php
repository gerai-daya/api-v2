<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMasterPersentase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_master_persentase', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('geraidaya')->nullable();
            $table->float('mitra')->nullable();
            $table->float('petugas')->nullable();
            $table->enum('status',['0','1'])->nullable();
            $table->string ('tipe')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_master_persentase');
    }
}
