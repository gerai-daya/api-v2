<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMasterZona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_master_zona', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('zona')->nullable();
            $table->longText('cakupan_wilayah');
            $table->tinyInteger('status')->nullable()->comment('0: Tidak Aktif, 1: Aktif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_master_zona');
    }
}
