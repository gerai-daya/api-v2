<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pembayaran', function (Blueprint $table) {
            $table->bigIncrements('id_pembayaran');
            $table->integer('id_booking')->nullable();
            $table->date('tgl_bayar')->nullable();
            $table->double('jumlah_bayar')->nullable();
            $table->double('diskon')->nullable();
            $table->double('total')->nullable();
            $table->string ('status',50)->nullable()->comment('Y,N');
            $table->string ('jenis_pembayaran',50)->default('SURVEY')->comment('biaya survey dan biaya booking');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pembayaran');
    }
}
