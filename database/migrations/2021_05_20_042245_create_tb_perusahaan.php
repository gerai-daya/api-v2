<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbPerusahaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_perusahaan', function (Blueprint $table) {
            $table->bigIncrements('id_perusahaan');
            $table->integer('id_zona')->nullable();
            $table->string ('jenis_bentuk_usaha',50)->nullable();
            $table->integer('asosiasi')->nullable();
            $table->string ('nama',100)->nullable();
            $table->string ('alamat',100)->nullable();
            $table->integer('kabupaten')->nullable();
            $table->string('provinsi',10)->nullable();
            $table->string ('telp_kantor',50); 
            $table->string ('email',50);
            $table->string ('direktur',50)->nullable();
            $table->string ('no_hp',30)->nullable();
            $table->string ('username',100)->nullable();
            $table->string ('password',100);
            $table->dateTime ('last_login')->nullable();
            $table->dateTime ('approved_at')->nullable();
            $table->string ('berkas_ktp',100)->nullable();
            $table->string ('berkas_kk',100)->nullable();
            $table->string ('berkas_npwp',100)->nullable();
            $table->string ('berkas_siup',100)->nullable();
            $table->string ('berkas_sbu',100)->nullable();
            $table->string ('folder_tb',7)->nullable();
            $table->enum('status_approve',['Pending','Disetujui','Ditolak','Dihapus'])->default('Pending');
            $table->string ('keterangan_penolakan')->default('-');
            $table->tinyInteger('status_cloud_ktp')->default('0');
            $table->tinyInteger('status_cloud_kk')->default('0');
            $table->tinyInteger('status_cloud_npwp')->default('0');
            $table->tinyInteger('status_cloud_siup')->default('0');
            $table->tinyInteger('status_cloud_sbu')->default('0');
            $table->double('biaya_survey')->default('50000');
            $table->double('biaya_pembatalan')->default('20000');
            $table->string ('logo')->default('nophoto.png');
            $table->string ('berkas_skd')->nullable();
            $table->tinyInteger('status_cloud_skd')->nullable();
            $table->enum('status_pkp',['Ya','Tidak'])->nullable();
            $table->enum('group',['0','1'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_perusahaan');
    }
}
