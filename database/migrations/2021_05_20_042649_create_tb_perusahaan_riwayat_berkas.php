<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbPerusahaanRiwayatBerkas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_perusahaan_riwayat_berkas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_perusahaan')->nullable();
            $table->string ('jenis_dokumen',20)->nullable();
            $table->string ('file_name',100);
            $table->enum('status',['Review','Approve','Reject']);
            $table->string ('created_by',100); 
            $table->string ('updated_by',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_perusahaan_riwayat_berkas');
    }
}
