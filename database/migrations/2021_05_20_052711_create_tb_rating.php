<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbRating extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_rating', function (Blueprint $table) {
            $table->bigIncrements('id_rating');
            $table->integer('id_booking')->nullable();
            $table->integer('id_pelanggan')->nullable();
            $table->integer('id_petugas')->nullable();
            $table->double('jml_rating')->nullable();
            $table->string ('masukan')->nullable();
            $table->dateTime ('tgl_rating')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_rating');
    }
}
