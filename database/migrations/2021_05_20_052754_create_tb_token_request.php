<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbTokenRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_token_request', function (Blueprint $table) {
            $table->bigIncrements('id_token');
            $table->string ('kode_token',20)->nullable();
            $table->string ('no_ref',20)->nullable();
            $table->timestamp('request_date')->nullable();
            $table->timestamp('expired_date');
            $table->timestamp('used_date')->nullable();
            $table->string ('status',10)->nullable();
            $table->integer ('id_pelanggan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_token_request');
    }
}
