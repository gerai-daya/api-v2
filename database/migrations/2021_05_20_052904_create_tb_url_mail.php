<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbUrlMail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_url_mail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string ('url')->nullable();
            $table->integer('id_booking')->nullable();
            $table->string ('id_booking_enc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_url_mail');
    }
}
