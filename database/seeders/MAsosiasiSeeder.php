<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class MAsosiasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //use Illuminate\Support\Facades\DB;
         DB::table('m_asosiasi')->insert([
            ['id_asosiasi'=>1,'nama_asosiasi'=>'AKLI', 'status'=>1,'created_at'=>date('Y-m-d H:i:s')],
            ['id_asosiasi'=>2,'nama_asosiasi'=>'PAKLINA', 'status'=>0,'created_at'=>date('Y-m-d H:i:s')]
        ]);
    }
}
