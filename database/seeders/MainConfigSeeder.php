<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MainConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('main_config')->insert([
            ['id_konfig'=>1,'nama'=>'Logo','value'=>'logo.png','created_at'=>date('Y-m-d H:i:s')],
            ['id_konfig'=>2,'nama'=>'nama aplikasi','value'=>'Gerai Daya','created_at'=>date('Y-m-d H:i:s')],
            ['id_konfig'=>3,'nama'=>'Tanggal Project','value'=>'11/11/2018','created_at'=>date('Y-m-d H:i:s')],
            ['id_konfig'=>4,'nama'=>'Klien','value'=>'GeraiDaya','created_at'=>date('Y-m-d H:i:s')],
            ['id_konfig'=>5,'nama'=>'Product By','value'=>'GeraiDaya','created_at'=>date('Y-m-d H:i:s')],
            ['id_konfig'=>6,'nama'=>'Jenis Login','value'=>'1','created_at'=>date('Y-m-d H:i:s')],
            ['id_konfig'=>8,'nama'=>'footer','value'=>'Copyright © 2021 PT Gerai Daya Indonesia. All rights reserved.','created_at'=>date('Y-m-d H:i:s')],
            ['id_konfig'=>7,'nama'=>'record log','value'=>'1000','created_at'=>date('Y-m-d H:i:s')]
        ]);
    }
}
