<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MainLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //use Illuminate\Support\Facades\DB;
        DB::table('main_level')->insert([
            ['id_level'=>1, 'nama'=>'admin', 'direct'=>'dashboard', 'ket'=>'Untuk Admin','created_at'=>date('Y-m-d H:i:s')],
            ['id_level'=>44,'nama'=> 'mitra','direct'=> 'dashboard','ket'=> 'untuk mitra','created_at'=>date('Y-m-d H:i:s')]
        ]);
    }
}
