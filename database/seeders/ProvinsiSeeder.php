<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //use Illuminate\Support\Facades\DB;
         DB::table('provinsi')->insert([
            ['id_prov'=>1,'nama'=>'BALI','created_at'=>date('Y-m-d H:i:s')]
        ]);
    }
}
