<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TbBankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //use Illuminate\Support\Facades\DB;
        DB::table('tb_bank')->insert([
            ['id_bank'=>1,'nama_bank'=>'MANDIRI', 'atas_nama'=>'geraidaya','no_rekening'=>'1690001028981','created_at'=>date('Y-m-d H:i:s')],
            ['id_bank'=>2,'nama_bank'=>'MUAMALAT', 'atas_nama'=>'geraidaya','no_rekening'=>'3710044900','created_at'=>date('Y-m-d H:i:s')],
            ['id_bank'=>3,'nama_bank'=>'BNI', 'atas_nama'=>'geraidaya','no_rekening'=>'722594163 ','created_at'=>date('Y-m-d H:i:s')],
            ['id_bank'=>4,'nama_bank'=>'BRI', 'atas_nama'=>'geraidaya','no_rekening'=>'006301002112304 ','created_at'=>date('Y-m-d H:i:s')]
        ]);
    }
}
