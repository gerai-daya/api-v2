<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TbJenisUsahaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            //use Illuminate\Support\Facades\DB;
            DB::table('jenis_usaha')->insert([
                ['id_usaha'=>1,'nama_usaha'=>'JASA PERBAIKAN & INSTALASI LISTRIK', 'value'=>'LISTRIK', 'biaya_pembatalan'=>50000],
                ['id_usaha'=>2,'nama_usaha'=>'JASA SERVICE AC', 'value'=>'AC', 'biaya_pembatalan'=>50000]
            ]);
    }
}
