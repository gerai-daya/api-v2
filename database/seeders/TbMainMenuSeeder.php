<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TbMainMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //use Illuminate\Support\Facades\DB;
         DB::table('main_menu')->insert([
            ['id_menu'=>1,'nama'=> 'Dashboard','level'=> '1','id_main'=> 0,'icon'=> 'fa fa-home','hak_akses'=> 1,'link'=> 'dashboard','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>17,'nama'=> 'Transaksi','level'=> '1','id_main'=> 1,'icon'=> 'fa fa-folder','hak_akses'=> 1,'link'=> 'transaksi','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>245,'nama'=> 'Konfigurasi','level'=> '2','id_main'=> 22,'icon'=> '','hak_akses'=> 1,'link'=> 'pengaturan/konfig','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>246,'nama'=> 'User Group','level'=> '2','id_main'=> 22,'icon'=> '','hak_akses'=> 1,'link'=> 'pengaturan/user_group','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>20,'nama'=> 'Data User','level'=> '2','id_main'=> 0,'icon'=> '','hak_akses'=> 1,'link'=> 'manage_user/data_user','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>184,'nama'=> 'Biling','level'=> '2','id_main'=> 17,'icon'=> '','hak_akses'=> 1,'link'=> 'transaksi/biling','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>165,'nama'=> 'Master Data','level'=> '1','id_main'=> 1,'icon'=> 'fa fa-database','hak_akses'=>  44,'link'=> 'data_master','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>158,'nama'=> 'Data Petugas','level'=> '2','id_main'=> 165,'icon'=> '', 'hak_akses'=> 44,'link'=> 'data_master/petugas','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>23,'nama'=> 'Mitra','level'=> '2','id_main'=> 21,'icon'=> '','hak_akses'=> 1,'link'=> 'data_master/perusahaan','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>22,'nama'=> 'Pengaturan','level'=> '1','id_main'=> 1,'icon'=> 'fa fa-gears','hak_akses'=> 1,'link'=> 'pengaturan','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>162,'nama'=> 'Booking','level'=> '2','id_main'=> 17,'icon'=> '','hak_akses'=> 1,'link'=> 'transaksi/booking','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>170,'nama'=> 'Transaksi','level'=> '1','id_main'=> 1,'icon'=> 'fa fa-book', 'hak_akses'=> 44,'link'=> 'transaksi','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>157,'nama'=> 'Dashboard','level'=> '1','id_main'=> 0,'icon'=> 'fa fa-home','hak_akses'=>  44,'link'=> 'dashboard','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>166,'nama'=> 'Booking','level'=> '2','id_main'=> 170,'icon'=> '', 'hak_akses'=> 44,'link'=> 'transaksi/booking','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>25,'nama'=> 'Petugas','level'=> '2','id_main'=> 21,'icon'=> '','hak_akses'=> 1,'link'=> 'data_master/petugas','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>21,'nama'=> 'Master Data','level'=> '1','id_main'=> 1,'icon'=> 'fa fa-database','hak_akses'=> 1,'link'=> 'data_master','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>183,'nama'=> 'Deposit','level'=> '2','id_main'=> 17,'icon'=> '','hak_akses'=> 1,'link'=> 'transaksi/deposit','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>180,'nama'=> 'Jasa','level'=> '2','id_main'=> 165,'icon'=> '', 'hak_akses'=> 44,'link'=> 'data_master/master_pekerjaan_jasa','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>181,'nama'=> 'Material','level'=> '2','id_main'=> 165,'icon'=> '', 'hak_akses'=> 44,'link'=> 'data_master/master_material','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>26,'nama'=> 'Pelanggan','level'=> '2','id_main'=> 21,'icon'=> '','hak_akses'=> 1,'link'=> 'data_master/pelanggan','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>27,'nama'=> 'Material','level'=> '2','id_main'=> 21,'icon'=> '','hak_akses'=> 1,'link'=> 'data_master/master_material','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>28,'nama'=> 'Jasa','level'=> '2','id_main'=> 21,'icon'=> '','hak_akses'=> 1,'link'=> 'data_master/master_pekerjaan_jasa','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>247,'nama'=> 'Deposit','level'=> '2','id_main'=> 170,'icon'=> '','hak_akses'=>  44,'link'=> 'transaksi/deposit','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>248,'nama'=> 'Biling','level'=> '2','id_main'=> 170,'icon'=> '', 'hak_akses'=> 44,'link'=> 'transaksi/biling','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>250,'nama'=> 'Master Lainnya','level'=> '2','id_main'=> 21,'icon'=> '','hak_akses'=> 1,'link'=> 'data_master/menu_data_lainnya','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>24,'nama'=> 'Perubahan Berkas','level'=> '2','id_main'=> 21,'icon'=> '','hak_akses'=> 1,'link'=> 'data_master/perubahan_berkas','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>18,'nama'=> 'Laporan','level'=> '1','id_main'=> 0,'icon'=> 'fa fa-file','hak_akses'=> 1,'link'=> 'laporan','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>171,'nama'=> 'Laporan','level'=> '1','id_main'=> 0,'icon'=> 'fa fa-file', 'hak_akses'=> 44,'link'=> 'laporan','created_at'=>date('Y-m-d H:i:s')],
            ['id_menu'=>251,'nama'=> 'Transaksi','level'=> '2','id_main'=> 17,'icon'=> '','hak_akses'=> 1,'link'=> 'transaksi/histori','created_at'=>date('Y-m-d H:i:s')]
        ]);
    }
}
