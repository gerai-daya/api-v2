<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TbMasterBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tb_master_brand')->insert([
            ['id_brand'=>'3','nama_brand'=>'Kabelindo','ket'=>'','created_at'=>date('Y-m-d H:i:s')],
            ['id_brand'=>'4','nama_brand'=>'Supreme','ket'=>'','created_at'=>date('Y-m-d H:i:s')],
            ['id_brand'=>'5','nama_brand'=>'Philips ','ket'=>'','created_at'=>date('Y-m-d H:i:s')]
        ]);
    }
}
