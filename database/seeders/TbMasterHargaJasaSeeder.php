<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TbMasterHargaJasaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tb_master_harga_jasa')->insert([
            ['id_harga_jasa'=>'1','id_jasa'=>'1','id_zona'=>'1','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'2','id_jasa'=>'2','id_zona'=>'1','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'3','id_jasa'=>'3','id_zona'=>'1','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'4','id_jasa'=>'1','id_zona'=>'2','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'5','id_jasa'=>'2','id_zona'=>'2','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'6','id_jasa'=>'3','id_zona'=>'2','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'7','id_jasa'=>'1','id_zona'=>'3','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'8','id_jasa'=>'2','id_zona'=>'3','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'9','id_jasa'=>'3','id_zona'=>'3','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'11','id_jasa'=>'5','id_zona'=>'1','harga_jasa'=>'180000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'12','id_jasa'=>'6','id_zona'=>'1','harga_jasa'=>'150000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'13','id_jasa'=>'4','id_zona'=>'2','harga_jasa'=>'195000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'14','id_jasa'=>'5','id_zona'=>'2','harga_jasa'=>'180000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'15','id_jasa'=>'6','id_zona'=>'2','harga_jasa'=>'150000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'16','id_jasa'=>'4','id_zona'=>'3','harga_jasa'=>'195000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'17','id_jasa'=>'5','id_zona'=>'3','harga_jasa'=>'180000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'18','id_jasa'=>'6','id_zona'=>'3','harga_jasa'=>'150000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'19','id_jasa'=>'7','id_zona'=>'1','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'20','id_jasa'=>'8','id_zona'=>'1','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'21','id_jasa'=>'9','id_zona'=>'1','harga_jasa'=>'125000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'22','id_jasa'=>'10','id_zona'=>'1','harga_jasa'=>'300000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'23','id_jasa'=>'11','id_zona'=>'1','harga_jasa'=>'500000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'24','id_jasa'=>'12','id_zona'=>'1','harga_jasa'=>'650000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'25','id_jasa'=>'13','id_zona'=>'1','harga_jasa'=>'150000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'26','id_jasa'=>'14','id_zona'=>'1','harga_jasa'=>'250000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'27','id_jasa'=>'15','id_zona'=>'1','harga_jasa'=>'400000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'28','id_jasa'=>'16','id_zona'=>'1','harga_jasa'=>'600000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'29','id_jasa'=>'17','id_zona'=>'1','harga_jasa'=>'275000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'30','id_jasa'=>'18','id_zona'=>'1','harga_jasa'=>'750000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'31','id_jasa'=>'19','id_zona'=>'1','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'32','id_jasa'=>'20','id_zona'=>'1','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'33','id_jasa'=>'21','id_zona'=>'1','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'34','id_jasa'=>'22','id_zona'=>'1','harga_jasa'=>'200000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'35','id_jasa'=>'7','id_zona'=>'2','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'36','id_jasa'=>'8','id_zona'=>'2','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'37','id_jasa'=>'9','id_zona'=>'2','harga_jasa'=>'125000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'38','id_jasa'=>'10','id_zona'=>'2','harga_jasa'=>'300000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'39','id_jasa'=>'11','id_zona'=>'2','harga_jasa'=>'500000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'40','id_jasa'=>'12','id_zona'=>'2','harga_jasa'=>'650000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'41','id_jasa'=>'13','id_zona'=>'2','harga_jasa'=>'150000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'42','id_jasa'=>'14','id_zona'=>'2','harga_jasa'=>'250000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'43','id_jasa'=>'15','id_zona'=>'2','harga_jasa'=>'400000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'44','id_jasa'=>'16','id_zona'=>'2','harga_jasa'=>'600000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'45','id_jasa'=>'17','id_zona'=>'2','harga_jasa'=>'275000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'46','id_jasa'=>'18','id_zona'=>'2','harga_jasa'=>'750000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'47','id_jasa'=>'19','id_zona'=>'2','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'48','id_jasa'=>'20','id_zona'=>'2','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'49','id_jasa'=>'21','id_zona'=>'2','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'50','id_jasa'=>'22','id_zona'=>'2','harga_jasa'=>'200000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'51','id_jasa'=>'7','id_zona'=>'3','harga_jasa'=>'75000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'52','id_jasa'=>'8','id_zona'=>'3','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'53','id_jasa'=>'9','id_zona'=>'3','harga_jasa'=>'125000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'54','id_jasa'=>'10','id_zona'=>'3','harga_jasa'=>'300000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'55','id_jasa'=>'11','id_zona'=>'3','harga_jasa'=>'500000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'56','id_jasa'=>'12','id_zona'=>'3','harga_jasa'=>'650000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'57','id_jasa'=>'13','id_zona'=>'3','harga_jasa'=>'150000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'58','id_jasa'=>'14','id_zona'=>'3','harga_jasa'=>'250000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'59','id_jasa'=>'15','id_zona'=>'3','harga_jasa'=>'400000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'60','id_jasa'=>'16','id_zona'=>'3','harga_jasa'=>'600000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'61','id_jasa'=>'17','id_zona'=>'3','harga_jasa'=>'275000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'62','id_jasa'=>'18','id_zona'=>'3','harga_jasa'=>'750000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'63','id_jasa'=>'19','id_zona'=>'3','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'64','id_jasa'=>'20','id_zona'=>'3','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'65','id_jasa'=>'21','id_zona'=>'3','harga_jasa'=>'100000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'66','id_jasa'=>'22','id_zona'=>'3','harga_jasa'=>'200000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'67','id_jasa'=>'23','id_zona'=>'1','harga_jasa'=>'1000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'68','id_jasa'=>'23','id_zona'=>'2','harga_jasa'=>'1000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'69','id_jasa'=>'23','id_zona'=>'3','harga_jasa'=>'1000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1'],
            ['id_harga_jasa'=>'70','id_jasa'=>'4','id_zona'=>'1','harga_jasa'=>'195000','created_at'=>date('Y-m-d H:i:s'),'status'=>'1']
        ]);
    }
}
