<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    // return $router->app->version();
    return showOutputResponse("rootAccess", false, 'Unauthorized', array(), 401);
});




$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->post('/user', 'UserController@index');
    $router->post('/uploadFile', 'UserController@uploadFile');
    $router->post('/booking', 'BookingController@index');
    $router->post('/deposit', 'DepositController@index');
    $router->post('/ppob', 'PPOBController@index');


    $router->group(['prefix' => 'finance'], function () use ($router) {
        $router->post('/apibca', 'ApiBCA@index');
    });

    $router->group(['prefix' => 'finance-uat'], function () use ($router) {
        $router->post('/apibca', 'ApiBCA@index');
    });
});

$router->get('/post', 'PostController@index');
$router->post('/post', 'PostController@store');
$router->get('/post/{id}', 'PostController@show');
$router->get('/post/{id}', 'PostController@showdata');
$router->post('/post/{id}', 'PostController@update');
$router->delete('/post/{id}', 'PostController@destroy');


$router->post('/userPublic', 'UserPublicController@index');
$router->post('/saveFirebasePublic', 'UserPublicController@saveFirebasePublic');
$router->post('/tesFile', 'UserPublicController@tesFile');
$router->post('/manajemenDeposit', 'DepositController@manajemenDeposit');
$router->post('/updateFotoProfile', 'UserController@updateFotoProfile');
$router->post('/updateFotoBooking', 'BookingController@updateFotoBooking');
$router->post('/sendNotification', 'UserPublicController@sendNotification');

//BCA
$router->group(['prefix' => 'finance'], function () use ($router) {
    $router->post('/inquiry', 'ApiBCA@GetBillVaBCA');
    $router->post('/payment', 'ApiBCA@GetPaymentBillVaBCA');
    $router->post('/oauth/token', 'ApiBCA@GetTokenVABCA');
});

$router->group(['prefix' => 'finance-uat'], function () use ($router) {
    $router->post('/inquiry', 'ApiBCA@GetBillVaBCA');
    $router->post('/payment', 'ApiBCA@GetPaymentBillVaBCA');
    $router->post('/oauth/token', 'ApiBCA@GetTokenVABCA');
});

$router->group(['prefix' => 'notification'], function () use ($router) {
    $router->post('/tolakan', 'ApiBCA@GetNotificationTolakan');
    $router->post('/app', 'ApiBCA@GetNotification');
});

$router->group(['prefix' => 'notification-uat'], function () use ($router) {
    $router->post('/tolakan', 'ApiBCA@GetNotificationTolakan');
    $router->post('/app', 'ApiBCA@GetNotification');
});
$router->post('/createvanumber', 'ApiBCA@GenerateNumberVA');
